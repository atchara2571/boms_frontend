import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider } from 'angular5-social-login';

export function getAuthServiceConfigs() {
    const config = new AuthServiceConfig([{
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('<>')
    }]);

    return config;
}
