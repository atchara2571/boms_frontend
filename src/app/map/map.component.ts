import {Component, OnInit, Inject, ViewChild, ElementRef, NgZone, AfterViewInit} from '@angular/core';
import {MapsAPILoader} from '@agm/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {} from '@types/googlemaps';

// import {google} from '@agm/core/services/google-maps-types';

export interface MapDialogData {
    latitude: string;
    longitude: string;
}

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, AfterViewInit {

    // default location picked.
    latitude: any = 15.870032;
    longitude: any = 100.99254100000007;
    locationChosen = false;
    // google: any;

    @ViewChild('search') public searchElement: ElementRef;

    constructor(public dialog: MatDialog,
                private mapsAPILoader: MapsAPILoader,
                private ngZone: NgZone) {
        this.getLocation(event);

    }

    ngAfterViewInit() {
        this.getLocation(event);
    }

    ngOnInit() {

    }

    // load map.
    getLocation(event) {
        this.mapsAPILoader.load().then(() => {
            const autocomplete = new google.maps.places.Autocomplete(this.searchElement.nativeElement, {
                types: [],
                componentRestrictions: {'country': 'TH'}
            });
            autocomplete.addListener('place_changed', () => {
                this.ngZone.run(() => {
                    const place: google.maps.places.PlaceResult = autocomplete.getPlace();
                    this.latitude = place.geometry.location.lat();
                    this.longitude = place.geometry.location.lng();
                    this.locationChosen = true;
                    this.openDialog(place);

                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }
                });
            });
        });
    }

    // open confirm dialog when already get the location.
    openDialog(event) {
        // this.latitude = event.coords.lat;
        // this.longitude = event.coords.lng;
        localStorage.setItem('lat', this.latitude);
        localStorage.setItem('lng', this.longitude);
        const dialogRef = this.dialog.open(MapDialogComponent, {
            disableClose: true,
            width: '300px',
            data: {lat: this.latitude, lng: this.longitude}
        });
        this.locationChosen = true;

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            //  this.animal = result;
        });
    }
}

// map confirm dialog.
@Component({
    selector: 'app-map-dialog',
    template: '<h1 mat-dialog-title>Picked Location</h1>\n' +
        '<div mat-dialog-content>\n' +
        '  <p> Latitude: {{data.lat}} </p>\n' +
        ' <p>Longitude: {{data.lng}} </p>\n' +
        '</div>\n' +
        '<div mat-dialog-actions>\n' +
        '  <button mat-raised-button button (click)="onNoClick()">No</button>\n' +
        '  <button mat-raised-button button color="primary" (click)="onChoseLocation()" [mat-dialog-close]="data.lat" cdkFocusInitial>Yes</button>\n' +
        '</div>'
})
export class MapDialogComponent {

    latitude: string;
    longitude: string;

    constructor(
        public dialogRef: MatDialogRef<MapDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: MapDialogData,
        private router: Router,
        private location: Location) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    // navigate to previous page when submit confirm location.
    onChoseLocation() {
        // this.router.navigate(['/company/company-create']);
        // this.location.back();
    }

}
