import {Routes} from '@angular/router';
import {CarsCreateComponent} from './cars/cars-create/cars-create.component';
import {CarsListComponent} from './cars/cars-list/cars-list.component';
import {CarsEditComponent} from './cars/cars-edit/cars-edit.component';
import {UsersTypeListComponent} from './users/users-type-list/users-type-list.component';
import {UsersTypeCreateComponent} from './users/users-type-create/users-type-create.component';
import {UsersTypeEditComponent} from './users/users-type-edit/users-type-edit.component';

export const TypesRoutes: Routes = [
    {
        path: 'cars-create',
        component: CarsCreateComponent,
        data: {
            heading: 'Cars'
        }
    },
    {
        path: 'cars-list',
        component: CarsListComponent,
        data: {
            heading: 'Cars List'
        }
    },
    {
        path: 'cars-edit/:carId/:carTitle',
        component: CarsEditComponent,
        data: {
            heading: 'Edit Car Info'
        }
    },
    {
        path: 'users-type-create',
        component: UsersTypeCreateComponent,
        data: {
            heading: 'Create New User Type'
        }
    },
    {
        path: 'users-type-list',
        component: UsersTypeListComponent,
        data: {
            heading: 'User Type List'
        }
    },
    {
        path: 'users-type-edit/:userTypeId/:userTypeTitle',
        component: UsersTypeEditComponent,
        data: {
            heading: 'User Type Edit'
        }
    },
];