import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {AngularMaterialModule} from '../angular-material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SatPopoverModule} from '@ncstate/sat-popover';
import {CarsCreateComponent} from './cars/cars-create/cars-create.component';
import {TypesRoutes} from './types.routing';
import { CarsListComponent } from './cars/cars-list/cars-list.component';
import { CarsEditComponent } from './cars/cars-edit/cars-edit.component';
import { UsersTypeEditComponent } from './users/users-type-edit/users-type-edit.component';
import { UsersTypeCreateComponent } from './users/users-type-create/users-type-create.component';
import { UsersTypeListComponent } from './users/users-type-list/users-type-list.component';

@NgModule({
    imports: [
        RouterModule.forChild(TypesRoutes),
        CommonModule,
        AngularMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        SatPopoverModule
    ],
    declarations: [
        CarsCreateComponent,
        CarsListComponent,
        CarsEditComponent,
        UsersTypeEditComponent,
        UsersTypeCreateComponent,
        UsersTypeListComponent
    ]
})

export class TypesModule {}
