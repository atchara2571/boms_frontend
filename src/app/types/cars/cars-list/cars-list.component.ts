import {Component, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {CarModel} from '../../../models/Car.model';
import {MatSort, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';

@Component({
    selector: 'app-cars-list',
    templateUrl: './cars-list.component.html',
    styleUrls: ['./cars-list.component.scss']
})
export class CarsListComponent implements OnInit {

    dataSource;
    displayedColumns = [];
    cars: CarModel[] = [];
    @ViewChild(MatSort) sort: MatSort;
    private carId: string;
    private status: string;

    constructor(public utilsService: UtilsService,
    private router: Router) {
        this.getCars();
    }

    ngOnInit() {
        this.displayedColumns = ['no', 'title', 'dimension', 'number', 'price', 'status', 'actions'];
    }

    getCars() {
        const _this = this;
        this.utilsService.reqGET('/config/type/cars', null, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
                const data = resp.data;
                _this.cars = data;
                _this.dataSource = new MatTableDataSource(_this.cars);
                _this.dataSource.sort = _this.sort;
            }
        });
    }

    // icon event.
    onSelect(i, event) {
        const carId = this.dataSource.data[i]._id;
        const carTitle = this.dataSource.data[i].title;
        if (event.target.id === 'editCarType') {
            this.router.navigate(['/types/cars-edit', carId, carTitle]);
        }
    }

    changeStatus(i, event) {
        this.carId = this.dataSource.data[i]._id;
        this.status = event.checked;
        this.updateCarType();
    }

    updateCarType() {
        const params = {
            enable: this.status
        };
        this.utilsService.reqPUT('/config/type/cars/' + this.carId, params, resp => {
            if (resp && resp.code === 20000) {
                this.ngOnInit();
            }
        });
    }

}
