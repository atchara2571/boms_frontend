import {Component, OnInit} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {Company} from '../../../models/Company.model';
import {AuthService} from '../../../helper/auth.service';
import {Partner} from '../../../models/Partner.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
    selector: 'app-cars-create',
    templateUrl: './cars-create.component.html',
    styleUrls: ['./cars-create.component.scss']
})
export class CarsCreateComponent implements OnInit {

    companies: Company[] = [];
    partners: Partner[] = [];
    form: FormGroup;
    companyId: string;
    partnerId: string;
    userId: string;

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private fb: FormBuilder,
                private router: Router) {
        this.userId = this.authService.getUserId();
    }

    ngOnInit() {
        this.getCompanyByUserId();
        this.fethCarForm();
    }

    fethCarForm() {
        this.form = this.fb.group({
            title: [''],
            description: [''],
            company: [''],
            partners: [''],
            number: [''],
            price: [''],
            weight: [''],
            long: [''],
            height: [''],
            enable: ['']
        });
    }

    // companies selection drop down list.
    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const comList = resp.data.filter(c => c);
                _this.companies = comList;
            }
        });
    }

    // get partner sort by company.
    getPartnerByCompany() {
        const _this = this;
        this.utilsService.reqGET('/partner/company/' + this.companyId, null, resp => {
            if (resp && resp.code === 20000) {
                const partnerList = resp.data.filter(idCom => idCom.company === _this.companyId);
                _this.partners = partnerList;
            }
        });
    }

    // company index.
    pickedCompany(i) {
        this.companyId = this.companies[i]['_id'];
        this.getPartnerByCompany();
    }

    // back to previous page.
    back() {
        this.utilsService.locationBack();
    }

    createNewCarType() {
        const params = {
            'title': this.form.value.title,
            'description': this.form.value.description,
            'company': this.companyId,
            'support': {
                'partners': this.form.value.partner,
                'distances': {
                    'number': this.form.value.number,
                    'price': this.form.value.price,
                },
                'dimension': {
                    'weight': this.form.value.weight,
                    'long': this.form.value.long,
                    'height': this.form.value.height,
                }
            },
            'enable': this.form.value.enable
        };
        this.utilsService.reqPOST('/config/type/cars', params, resp => {
            if (resp && resp.code === 20000) {
                alert('Create Success!!');
                this.router.navigate(['/types/cars-list']);
            }
        });
    }

}
