import {Component, OnInit} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {ActivatedRoute} from '@angular/router';
import {CarModel} from '../../../models/Car.model';
import {Company} from '../../../models/Company.model';
import {Partner} from '../../../models/Partner.model';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../../helper/auth.service';

@Component({
    selector: 'app-cars-edit',
    templateUrl: './cars-edit.component.html',
    styleUrls: ['./cars-edit.component.scss']
})
export class CarsEditComponent implements OnInit {

    public carId: string;
    public carTitle: string;
    public userId: string;
    public companyId: string;
    public cars: CarModel[] = [];
    public companies: Company[] = [];
    public partners: Partner[] = [];

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private route: ActivatedRoute) {
        this.carId = this.route.snapshot.paramMap.get('carId');
        this.carTitle = this.route.snapshot.paramMap.get('carTitle');
        this.userId = this.authService.getUserId();
    }

    ngOnInit() {
        this.getCarInfo();
        this.getCompanyByUserId();
        this.getPartnerByCompany();
    }

    getCarInfo() {
        const _this = this;
        this.utilsService.reqGET('/config/type/cars/' + this.carId, null, resp => {
            if (resp && resp.code === 20000) {
               const data = resp.data.filter(c => c);
               _this.cars = data;
            }
        });
    }

    // companies selection drop down list.
    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const comList = resp.data.filter(c => c);
                _this.companies = comList;
                _this.companyId = resp.data.filter(cc => cc._id);
            }
        });
    }

    // get partner sort by company.
    getPartnerByCompany() {
        const _this = this;
        this.utilsService.reqGET('/partner/company/' + _this.companyId, null, resp => {
            if (resp && resp.code === 20000) {
                const partnerList = resp.data.filter(r => r);
                _this.partners = partnerList;
            }
        });
    }

    updateCarType(CarForm: NgForm) {
        const params = {
            'title': CarForm.value.title,
            'description': CarForm.value.description,
            'company': CarForm.value.company,
            'support': {
                'partners': CarForm.value.partners,
                'distances': {
                    'number': CarForm.value.number,
                    'price': CarForm.value.price,
                },
                'dimension': {
                    'weight': CarForm.value.weight,
                    'long': CarForm.value.long,
                    'height': CarForm.value.height,
                }
            },
            'enable': CarForm.value.enable
        };
        this.utilsService.reqPUT('/config/type/cars/' + this.carId, params, resp => {
            if (resp && resp.code === 20000) {
               alert('Success!');
               this.back();
            }
        });
    }

    // back to previous page.
    back() {
        this.utilsService.locationBack();
    }

}
