import {Component, OnInit} from '@angular/core';
import {UserTypeModel} from '../../../models/UserType.model';
import {UtilsService} from '../../../helper/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Company} from '../../../models/Company.model';
import {AuthService} from '../../../helper/auth.service';
import {NgForm} from '@angular/forms';

@Component({
    selector: 'app-users-type-edit',
    templateUrl: './users-type-edit.component.html',
    styleUrls: ['./users-type-edit.component.scss']
})
export class UsersTypeEditComponent implements OnInit {

    public userTypes: UserTypeModel[] = [];
    public companies: Company[] = [];
    public userTypeId: string;
    public userTypeTitle: string;
    public userId: string;
    public companyId: string;

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private route: ActivatedRoute,
                private router: Router) {
        this.userId = this.authService.getUserId();
        this.userTypeId = this.route.snapshot.paramMap.get('userTypeId');
        this.userTypeTitle = this.route.snapshot.paramMap.get('userTypeTitle');
    }

    ngOnInit() {
        this.getUserType();
    }

    getUserType() {
        const _this = this;
        this.utilsService.reqGET('/config/type/user', this.userTypeId, resp => {
            if (resp && resp.code === 20000) {
                const data = resp.data;
                _this.userTypes.push(data);
            }
        });
    }

    // // company selection list
    // getCompanyByUserId() {
    //     const _this = this;
    //     this.utilsService.reqGET('/company', _this.userId, resp => {
    //         if (resp && resp.code === 20000) {
    //             console.log(resp);
    //             const companyId = resp.data.map(c => c._id);
    //             _this.companyId = companyId;
    //         }
    //     });
    // }

    updateUserType(userTypeForm: NgForm) {
        const params = {
            title: userTypeForm.value.title,
            description: userTypeForm.value.description,
            key: userTypeForm.value.key,
            status: userTypeForm.value.status
        };
        if (userTypeForm.valid) {
            this.utilsService.reqPUT('/config/type/user/' + this.userTypeId, params, resp => {
                if (resp && resp.code === 20000) {
                    console.log(resp);
                    alert('Success!');
                    this.router.navigate(['/types/users-type-list']);
                }
            });
        }
    }

    // back to previous page.
    back() {
        this.utilsService.locationBack();
    }

}
