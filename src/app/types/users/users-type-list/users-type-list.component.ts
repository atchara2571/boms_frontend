import {Component, OnInit, ViewChild} from '@angular/core';
import {UserTypeModel} from '../../../models/UserType.model';
import {UtilsService} from '../../../helper/utils.service';
import {MatSort, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';

@Component({
    selector: 'app-users-type-list',
    templateUrl: './users-type-list.component.html',
    styleUrls: ['./users-type-list.component.scss']
})
export class UsersTypeListComponent implements OnInit {

    dataSource;
    displayedColumns = [];
    public userTypeId: string;
    public status: string;
    public userTypes: UserTypeModel[] = [];

    @ViewChild(MatSort) sort: MatSort;

    constructor(public utilsService: UtilsService,
                private router: Router) {
        this.getUserType();
    }

    ngOnInit() {
        this.displayedColumns = ['number', 'title', 'key', 'status', 'actions' ];
    }

    getUserType() {
        const _this = this;
        this.utilsService.reqGET('/config/type/users', null, resp => {
            if (resp && resp.code === 20000) {
                const data = resp.data.filter(r => r);
                _this.userTypes = data;
                _this.dataSource = new MatTableDataSource(_this.userTypes);
                _this.dataSource.sort = _this.sort;
            }
        });
    }
    // icon event.
    onSelect(i, event) {
        const userTypeId = this.dataSource.data[i]._id;
        const userTypeTitle = this.dataSource.data[i].title;
        if (event.target.id === 'editUserTypeIcon') {
            this.router.navigate(['/types/users-type-edit', userTypeId, userTypeTitle]);
        }
    }

    changeStatus(i, event) {
        this.userTypeId = this.dataSource.data[i]._id;
        this.status = event.checked;
        this.updateUserType();
    }

    // submit edit company info.
    updateUserType() {
        const params = {
            enable: this.status
        };
        this.utilsService.reqPUT('/config/type/user/' + this.userTypeId, params, resp => {
            if (resp && resp.code === 20000) {

            }
        });
    }

    addUserType() {
        this.router.navigate(['/types/users-type-create']);
    }


}
