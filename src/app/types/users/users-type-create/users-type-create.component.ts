import {Component, OnInit} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {Company} from '../../../models/Company.model';
import {AuthService} from '../../../helper/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
    selector: 'app-users-type-create',
    templateUrl: './users-type-create.component.html',
    styleUrls: ['./users-type-create.component.scss']
})
export class UsersTypeCreateComponent implements OnInit {

    form: FormGroup;
    public userId: string;
    public companyId: string;
    public companies: Company[] = [];

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private fb: FormBuilder,
                private router: Router) {
        this.userId = this.authService.getUserId();
    }

    ngOnInit() {
        this.fetchUserTypeForm();
        this.getCompanyByUserId();
    }

    fetchUserTypeForm() {
        this.form = this.fb.group({
            title: [''],
            description: [''],
            company: [''],
            key: [null],
            enable: ['']
        });
    }

    // companies selection drop down list.
    public getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const comList = resp.data.filter(c => c);
                _this.companies = comList;
            }
        });
    }

    createNewUserType() {
        const params = {
            'title': this.form.value.title,
            'description': this.form.value.description,
            'company': this.form.value.company,
            'key': this.form.value.key,
            'enable': this.form.value.enable
        };
        if (this.form.valid) {
            this.utilsService.reqPOST('/config/type/user', params, resp => {
                if (resp && resp.code === 20000) {
                    this.router.navigate(['/types/users-type-list']);
                }
            });
        }
    }

    back() {
        this.utilsService.locationBack();
    }

}
