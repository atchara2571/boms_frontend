import {NgModule} from '@angular/core';
import {
    MatSidenavModule,
    MatDatepickerModule,
    MatMenuModule,
    MatCheckboxModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatTooltipModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatCardModule,
    MatDialogModule,
    MatSelectModule,
    MatInputModule,
    MatNativeDateModule,
    MatRadioModule,
    MatStepperModule,
    MatExpansionModule,
    MatTableModule,
    MatSnackBarModule,
    MatGridListModule,
    MatSlideToggleModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatFormFieldModule
} from '@angular/material';

@NgModule({
    exports: [
        MatSidenavModule,
        MatDatepickerModule,
        MatMenuModule,
        MatCheckboxModule,
        MatIconModule,
        MatButtonModule,
        MatToolbarModule,
        MatTooltipModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatSelectModule,
        MatProgressBarModule,
        MatPaginatorModule,
        MatCardModule,
        MatDialogModule,
        MatInputModule,
        MatNativeDateModule,
        MatRadioModule,
        MatStepperModule,
        MatExpansionModule,
        MatTableModule,
        MatSnackBarModule,
        MatGridListModule,
        MatSlideToggleModule,
        MatAutocompleteModule,
        MatChipsModule,
        MatFormFieldModule
    ]
})

export class AngularMaterialModule {
}
