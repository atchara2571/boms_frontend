import {Routes} from '@angular/router';
import {TransportationCreateComponent} from './transportation-create/transportation-create.component';
import {TransportationEditComponent} from './transportation-edit/transportation-edit.component';
import {TransportationListComponent} from './transportation-list/transportation-list.component';

export const TransportationRoutes: Routes = [
    {
        path: 'transportation-create',
        component: TransportationCreateComponent,
        data: {
            heading: 'Create Transportation',
            css: 'view-no-padding'
        }
    },
    {
        path: 'transportation-edit',
        component: TransportationEditComponent,
        data: {
            heading: 'Edit Transportation Info',
            css: 'view-no-padding'
        }
    },
    {
        path: 'transportation-list',
        component: TransportationListComponent,
        data: {
            heading: 'Transportation List',
            css: 'view-no-padding'
        }
    },
];