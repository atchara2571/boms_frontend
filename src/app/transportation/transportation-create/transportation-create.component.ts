import {Component, OnInit} from '@angular/core';
import {UtilsService} from '../../helper/utils.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Company} from '../../models/Company.model';
import {Partner} from '../../models/Partner.model';
import {CarModel} from '../../models/Car.model';
import {AuthService} from '../../helper/auth.service';

@Component({
    selector: 'app-transportation-create',
    templateUrl: './transportation-create.component.html',
    styleUrls: ['./transportation-create.component.scss']
})
export class TransportationCreateComponent implements OnInit {

    form: FormGroup;
    public userId: string;
    public companyId: string;
    public partnerId: string;
    public carId: string;
    public companies: Company[] = [];
    public partners: Partner[] = [];
    public cars: CarModel[] = [];

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private fb: FormBuilder) {
        this.userId = this.authService.getUserId();
        this.getCompanyByUserId();
    }

    ngOnInit() {
        this.fetchTForm();
        this.getCar();
    }

    fetchTForm() {
        this.form = this.fb.group({
            company: [''],
            partner: [''],
            car: [''],
            type_name: [''],
            type_key: [''],
            type_description: [''],
            title: [''],
            description: [''],
            contact_name: [''],
            contact_msisdn: [''],
            contact_note: [''],
            lat: [localStorage.getItem('lat')],
            long: [localStorage.getItem('lng')]
        });
    }

    // companies selection drop down list.
    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const comList = resp.data.filter(c => c);
                _this.companies = comList;
            }
        });
    }

    // company index.
    pickedCompany(i) {
        this.companyId = this.companies[i]['_id'];
        this.getPartnerByCompany();
    }

    // get partner sort by company.
    getPartnerByCompany() {
        const _this = this;
        this.utilsService.reqGET('/partner/company/' + this.companyId, null, resp => {
            if (resp && resp.code === 20000) {
                const partnerList = resp.data.filter(idCom => idCom.company === _this.companyId);
                _this.partners = partnerList;
            }
        });
    }

    getCar() {
        const _this = this;
        this.utilsService.reqGET('/config/type/cars', null, resp => {
            if (resp && resp.code === 20000) {
                const data = resp.data.filter(c => c);
                _this.cars = data;
            }
        });
    }

    createNewT() {
        const params = {
            company: this.companyId,
            partner: this.form.value.partner,
            cars: this.form.value.car,
            type: {
                type_name: this.form.value.type_name,
                type_keys: this.form.value.type_keys,
                type_description: this.form.value.type_description
            },
            transportation_position: {
                title: this.form.value.title,
                description: this.form.value.description,
                contact: {
                    contact_name: this.form.value.contact_name,
                    contact_msisdn: this.form.value.contact_msisdn,
                    contact_note: this.form.value.note
                },
                locations: {
                    lat: this.form.value.lat,
                    long: this.form.value.long
                }
            }
        };
        this.utilsService.reqPOST('/transportaions', params, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
            }
        });
    }

    back() {
        this.utilsService.locationBack();
    }

    // clear storage when leave this page.
    ngOnDestroy(): void {
        localStorage.removeItem('lat');
        localStorage.removeItem('lng');
    }

}
