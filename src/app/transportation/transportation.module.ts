import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {AngularMaterialModule} from '../angular-material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SatPopoverModule} from '@ncstate/sat-popover';
import {TransportationCreateComponent} from './transportation-create/transportation-create.component';
import {TransportationEditComponent} from './transportation-edit/transportation-edit.component';
import {TransportationListComponent} from './transportation-list/transportation-list.component';
import {TransportationRoutes} from './transportation.routing';

@NgModule({
    imports: [
        RouterModule.forChild(TransportationRoutes),
        CommonModule,
        AngularMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        SatPopoverModule
    ],
    declarations: [
        TransportationCreateComponent,
        TransportationEditComponent,
        TransportationListComponent
    ]
})

export class TransportationModule {}
