import {Component, OnInit} from '@angular/core';
import {TransportationModel} from '../../models/Transportation.model';
import {UtilsService} from '../../helper/utils.service';

@Component({
    selector: 'app-transportation-list',
    templateUrl: './transportation-list.component.html',
    styleUrls: ['./transportation-list.component.scss']
})
export class TransportationListComponent implements OnInit {

    dataSource;
    displayedColumns = [];
    public transportations: TransportationModel[] = [];

    constructor(public utilsService: UtilsService) {
        this.getTransportation();
    }

    ngOnInit() {
    }

    getTransportation() {
        this.utilsService.reqGET('/transportations', null, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
            }
        });
    }

}
