import {HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
// import {throwError} from 'rxjs/Observable/throw';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private dialog: MatDialog) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        console.log(error.message);
        // if (error.error.message) {
        //   alert(error.error.message);
        // }
        // this.dialog.open(ErrorComponent, {data: {message: errorMessage}});
        return ErrorObservable.create(error);
      })
    );
  }
}
