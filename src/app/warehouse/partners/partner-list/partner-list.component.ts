import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Partner} from '../../../models/Partner.model';
import {Router} from '@angular/router';
import {AuthService} from '../../../helper/auth.service';
import {DialogData} from '../../../models/Dialog.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Company} from '../../../models/Company.model';

@Component({
    selector: 'app-partner-list',
    templateUrl: './partner-list.component.html',
    styleUrls: ['../partner.scss']
})
export class PartnerListComponent implements OnInit {

    public userId: string;
    public companyId: string;
    public companyName: string;
    private partnerId: string;
    public status: any;
    @ViewChild(MatSort) sort: MatSort;
    dataSource;
    displayedColumns = [];
    partners: Partner[] = [];

    // partnerCurrentId: string;
    // current = [
    //     this.partnerCurrentId
    // ];

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private router: Router,
                public dialog: MatDialog) {

        this.userId = this.authService.getUserId();
        this.getCompanyByUserId();

    }

    ngOnInit() {
        this.displayedColumns = ['number', 'partnerName', 'actions'];
    }

    // companies drop down list of current user.
    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const companyId = resp.data.map(c => c._id);
                const companyName = resp.data.map(cname => cname.title);
                _this.companyId = companyId;
                _this.companyName = companyName;
            }
            _this.getPartnerByCompany();
        });
    }

    // partners of current company list.
    getPartnerByCompany() {
        const _this = this;
        this.utilsService.reqGET('/partner/company/' + this.companyId, null, resp => {
            if (resp && resp.code === 20000) {
                const partnerList = resp.data.filter(r => r);
                _this.partners = partnerList;
                _this.dataSource = new MatTableDataSource(_this.partners);
                _this.dataSource.sort = _this.sort;
            }
        });
    }

    // icon event
    onSelect(i: number) {
        const id = this.dataSource.data[i]._id;
        this.partnerId = id;
        this.openDialog();
        // this.router.navigate(['/partner/partner-edit', this.partnerId]);
    }

    changeStatus(i, event) {
        this.partnerId = this.dataSource.data[i]._id;
        this.status = event.checked;
        // this.updateCompany();
    }

    // add partner dialog.
    public openDialog(): void {
        const dialogRef = this.dialog.open(CompanyDialogComponent, {
            disableClose: true,
            width: '250px',
            data: {pnId: this.partnerId, uId: this.userId}
        });
        dialogRef.afterClosed().subscribe(result => {
        });
    }


    // getPartner() {
    //   const _this = this;
    //   this.utilsService.reqGET('/partners', null, resp => {
    //     console.log(resp);
    //     if (resp && resp.code === 20000) {
    //       const partnerList = resp.data
    //         .filter(data => data.create.by === _this.userId);
    //       _this.partners = partnerList;
    //       _this.dataSource = new MatTableDataSource(_this.partners);
    //       _this.dataSource.sort = _this.sort;
    //     }
    //   });
    // }

    // addPn() {
    //   this.current.push(this.partnerId);
    //   console.log(this.current);
    //   this.addPartnerToWarehouse();
    // }

    // addPartnerToWarehouse() {
    //   const params = {
    //     'partners': this.current
    //   };
    //   console.log(params);
    //   this.utilsService.reqPUT('/warehouse/' + this.warehouseId, params, resp => {
    //     console.log(resp);
    //   });
    //   this.router.navigate(['/partner/partner-by-warehouse']);
    // }

}


// add partner dialog.
@Component({
    providers: [PartnerListComponent],
    selector: 'app-company-dialog',
    template: '<h1 mat-dialog-title>Select Company</h1>\n' +
        '<div mat-dialog-content style="height: 150px;">\n' +
        '<p>Please select company.</p>\n' +
        ' <form [formGroup]="form">\n' +
        '    <div class="formGroup">\n' +
        '  <mat-form-field>\n' +
        '        <mat-select placeholder="Select Company" formControlName="company">\n' +
        '          <mat-option *ngFor="let company of companies;" [value]="company.title">\n' +
        '            {{company.title}}\n' +
        '          </mat-option>\n' +
        '        </mat-select>\n' +
        '      </mat-form-field>\n' +
        '</div>\n' +
        '<div mat-dialog-actions>\n' +
        '  <button mat-button (click)="onNoClick()">Cancel</button>\n' +
        '  <button mat-button cdkFocusInitial (click)="addPartner()">Ok</button>\n' +
        '</div>\n' +
        '</form>\n' +
        '</div>'
})

export class CompanyDialogComponent implements OnInit {

    form: FormGroup;
    public companyName: string;
    public companies: Company[];
    private userId: string;

    constructor(
        public dialogRef: MatDialogRef<CompanyDialogComponent>,
        private router: Router,
        public utilsService: UtilsService,
        public authService: AuthService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private fb: FormBuilder) {

        this.userId = this.authService.getUserId();
    }

    ngOnInit() {
        this.fetchCompanyDialogForm();
        this.getCompanyByUserId();
    }

    fetchCompanyDialogForm() {
        this.form = this.fb.group({
            company: ['']
        });
    }

    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const companyList = resp.data.filter(r => r);
                _this.companies = companyList;
            }
        });
    }

    addPartner() {
        this.router.navigate(['/partner/partner-edit', this.data.pnId, this.form.value.company]);
        this.onNoClick();
    }

    // leave dialog.
    onNoClick(): void {
        this.dialogRef.close();
    }

}