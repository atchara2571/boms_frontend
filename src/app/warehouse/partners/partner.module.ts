import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatInputModule} from '@angular/material';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {AngularMaterialModule} from '../../angular-material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PartnerRouting} from './partner.routing';
import {PartnerByCompanyComponent} from './partner-by-company/partner-by-company.component';
import {PartnerCreateComponent} from './partner-create/partner-create.component';
import {PartnerEditComponent} from './partner-edit/partner-edit.component';
import {CompanyDialogComponent, PartnerListComponent} from './partner-list/partner-list.component';
import {MapComponent} from '../../map/map.component';
import {AgmCoreModule} from '@agm/core';

@NgModule({
    imports: [
        CommonModule,
        PartnerRouting,
        MatInputModule,
        NgxDatatableModule,
        AngularMaterialModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
        AgmCoreModule
    ],
    declarations: [
        PartnerByCompanyComponent,
        PartnerCreateComponent,
        PartnerEditComponent,
        PartnerListComponent,
        CompanyDialogComponent,
        MapComponent
    ],
    entryComponents: [
        CompanyDialogComponent,
    ]
})
export class PartnerModule {
}
