import {Component, OnInit} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {UtilsService} from '../../../helper/utils.service';
import {AuthService} from '../../../helper/auth.service';
import {HttpClient} from '@angular/common/http';
import {Company} from '../../../models/Company.model';
import {ActivatedRoute, Router} from '@angular/router';
import {Partner} from '../../../models/Partner.model';


@Component({
    selector: 'app-partner-edit',
    templateUrl: './partner-edit.component.html',
    styleUrls: ['./partner-edit.component.scss']
})
export class PartnerEditComponent implements OnInit {

    public partnerId: string;
    public userId: string;
    public companyId: string;
    partners: Partner[] = [];
    companies: Company[] = [];
    lat = localStorage.getItem('lat');
    lng = localStorage.getItem('lng');

    constructor(private fb: FormBuilder,
                public utilsService: UtilsService,
                public authService: AuthService,
                private http: HttpClient,
                private router: Router,
                private route: ActivatedRoute) {

        this.userId = this.authService.getUserId();
        this.partnerId = this.route.snapshot.paramMap.get('partnerId');
        this.getPartnerInfo();
    }

    ngOnInit() {
        this.getCompanyByUser();
    }


    getPartnerInfo() {
        this.utilsService.reqGET('/partner/' + this.partnerId, null, resp => {
            if (resp && resp.code === 20000) {
                const data = resp.data;
                if (this.lat && this.lng) {
                    data.contact.lat = this.lat;
                    data.contact.long = this.lng;
                } else {
                    data.contact.lat;
                    data.contact.long;
                }
                this.partners.push(data);

            }
        });

    }

    // get company by user.
    getCompanyByUser() {
        const _this = this;
        this.utilsService.reqGET('/company/' + _this.userId, null, (resp) => {
            if (resp && resp.code === 20000) {
                const dataList = resp.data;
                _this.companies = dataList;
            }
        });
    }

    // submit editing partner info.
    updatePartner(partnerForm: NgForm) {
        const params = {
            'title': partnerForm.value.title,
            'company': partnerForm.value.company,
            'contact': {
                'name': partnerForm.value.name,
                'msisdn': partnerForm.value.msisdn,
                'email': partnerForm.value.email,
                'address': partnerForm.value.address,
                'lat': partnerForm.value.lat,
                'long': partnerForm.value.long
            },
            'users': {
                'by': this.userId
            }
        };
        this.utilsService.reqPUT('/partner/' + this.partnerId, params, resp => {
            if (resp && resp['code'] === 20000) {
                console.log(resp);
                this.router.navigate(['/partner/partner-list']);
            }
        });
    }

    ngOnDestroy(): any {
        localStorage.removeItem('lat');
        localStorage.removeItem('lng');
    }

}
