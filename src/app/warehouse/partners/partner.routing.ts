import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PartnerListComponent} from './partner-list/partner-list.component';
import {PartnerCreateComponent} from './partner-create/partner-create.component';
import {PartnerEditComponent} from './partner-edit/partner-edit.component';
import {PartnerByCompanyComponent} from './partner-by-company/partner-by-company.component';

const routes: Routes = [
  {
    path: '',
    component: PartnerListComponent,
    data: {
      heading: 'Partner List'
    }
  },
  {
    path: 'partner-list',
    component: PartnerListComponent,
    data: {
      heading: 'Partner List'
    }
  },
  {
    path: 'partner-by-company/:companyId/:companyName',
    component: PartnerByCompanyComponent,
    data: {
      heading: 'Partner List by Company'
    }
  },
  {
    path: 'partner-create',
    component: PartnerCreateComponent,
    data: {
      heading: 'Create Partner'
    }
  },
  {
    path: 'partner-edit/:partnerId/:companyName',
    component: PartnerEditComponent,
    data: {
      heading: 'Edit Partner Info'
    }
  },
]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PartnerRouting {}
