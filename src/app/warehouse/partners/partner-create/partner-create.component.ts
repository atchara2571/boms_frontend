import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Company} from '../../../models/Company.model';
import {UtilsService} from '../../../helper/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../helper/auth.service';
import {MapComponent} from '../../../map/map.component';

@Component({
    selector: 'app-partner-create',
    templateUrl: './partner-create.component.html',
    styleUrls: ['./partner-create.component.scss'],
    providers: [MapComponent]
})
export class PartnerCreateComponent implements OnInit {
    form: FormGroup;
    companies: Company[] = [];
    public companyId: string;
    public userId: string;
    public companyName: string;
    @ViewChild('search') public searchElement: ElementRef;

    lat: string;

    constructor(private fb: FormBuilder,
                public utilsService: UtilsService,
                public authService: AuthService,
                private router: Router,
                private route: ActivatedRoute,
                private map: MapComponent) {

        this.userId = this.authService.getUserId();
        this.companyName = this.route.snapshot.paramMap.get('companyName');
        this.getCompanyByUserId();
    }

    ngOnInit() {
        this.fetchPartnerForm();
        this.lat = this.map.latitude;
        console.log(this.lat);
    }

    // fetch partner form.
    fetchPartnerForm() {
        this.form = this.fb.group({
            title: [''],
            company: [''],
            name: [''],
            msisdn: [''],
            email: [''],
            address: [''],
            lat: [localStorage.getItem('lat')],
            long: [localStorage.getItem('lng')]
        });
    }

    // companies selection drop down list.
    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const comList = resp.data.filter(c => c);
                _this.companies = comList;
            }
        });
    }

    // submit create new partner.
    createPartner() {
        const params = {
            'title': this.form.value.title,
            'company': this.form.value.company,
            'contact': {
                'name': this.form.value.name,
                'msisdn': this.form.value.msisdn,
                'email': this.form.value.email,
                'address': this.form.value.address,
                'lat': this.form.value.lat,
                'long': this.form.value.long
            },
            'users': {
                'by': this.userId
            }
        };
        if (this.form.valid) {
            this.utilsService.reqPOST('/partner', params, resp => {
                if (resp && resp.code === 20000) {
                    // this.utilsService.locationBack();
                    this.router.navigate(['/partner/partner-list']);
                }
            });
        }
    }

    ngOnDestroy(): any {
        localStorage.removeItem('lat');
        localStorage.removeItem('lng');
    }

    // back to previous page.
    back() {
        this.utilsService.locationBack();
    }

}


