import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {UtilsService} from '../../../helper/utils.service';
import {ActivatedRoute, Router} from '@angular/router';

export interface Section {
  title: string;
  icon: string;
}

@Component({
  selector: 'app-partner-by-company',
  templateUrl: './partner-by-company.component.html',
  styleUrls: ['./partner-by-company.component.scss']
})
export class PartnerByCompanyComponent implements OnInit {

  public partnerId: string;
  public companyId: string;
  public companyName: string;
  dataSource;
  displayedColumns = [];
  partners: Section[] = [];
  @ViewChild(MatSort) sort: MatSort;

  constructor(public utilsService: UtilsService,
              public router: Router,
              private route: ActivatedRoute) {

      // get company id & name.
      this.companyId = this.route.snapshot.paramMap.get('companyId');
      this.companyName = this.route.snapshot.paramMap.get('companyName');
      this.getPartnerByCompany();

  }

  ngOnInit() {
    // column's title.
    this.displayedColumns = ['number', 'partnerName', 'actions'];
  }

  // partners of current company list.
  getPartnerByCompany() {
    const _this = this;
    this.utilsService.reqGET('/partner/company/' + _this.companyId, null, resp => {
      if (resp && resp.code === 20000) {
        console.log(resp);
        const data = resp.data.filter(idCom => idCom.company === _this.companyId);
        _this.partners = data;
        _this.dataSource = new MatTableDataSource(_this.partners);
        _this.dataSource.sort = _this.sort;
      }
    });
  }

  // icon event.
  onSelect(i: number, event) {
    const partnerId = this.dataSource.data[i]._id;
    const partnerName = this.dataSource.data[i].title;
    if (event.target.id === 'editPartnerIcon') {
      this.router.navigate(['/partner/partner-edit', partnerId, this.companyName]);
    } else if (event.target.id === 'seeProductIcon') {
      this.router.navigate(['/store-product/store-product-by-partner', partnerId, partnerName, this.companyId]);
    } else if (event.target.id === 'seeWarehouseIcon') {
      this.router.navigate(['/warehouse/warehouse-by-partner', partnerId, partnerName]);
    } else if (event.target.id === 'addWarehouseIcon') {
      this.router.navigate(['/warehouse/warehouse-create']);
    }
  }

}
