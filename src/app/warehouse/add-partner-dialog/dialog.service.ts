import {Injectable} from '@angular/core';
import {AddPartnerDialogComponent} from './add-partner-dialog.component';
import {MatDialog} from '@angular/material';

@Injectable()
export class DialogService {

    constructor(private dialog: MatDialog) {}

    public openDialog(): void {
        const dialogRef = this.dialog.open(AddPartnerDialogComponent, {
            disableClose: true,
            width: '250px',
            // data: {cId: this.companyId}
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

}