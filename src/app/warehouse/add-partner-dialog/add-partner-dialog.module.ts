import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AngularMaterialModule} from '../../angular-material.module';
import {WarehouseModule} from '../warehouses/warehouse.module';
import {AddPartnerDialogComponent} from './add-partner-dialog.component';

@NgModule({
    imports: [
        CommonModule,
        AngularMaterialModule,
        WarehouseModule
    ],
    declarations: [
        AddPartnerDialogComponent
    ],
    entryComponents: [
    ]
})

export class AddPartnerDialogModule {
}