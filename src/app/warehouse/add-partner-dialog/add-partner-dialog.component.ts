import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogData} from '../../models/Dialog.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UtilsService} from '../../helper/utils.service';
import {AuthService} from '../../helper/auth.service';
import {Router} from '@angular/router';
import {Company} from '../../models/Company.model';
import {Partner} from '../../models/Partner.model';
import {Product} from '../../models/Product.model';

@Component({
  selector: 'app-add-partner-dialog',
  templateUrl: './add-partner-dialog.component.html',
  styleUrls: ['./add-partner-dialog.component.scss']
})
export class AddPartnerDialogComponent implements OnInit {

    form: FormGroup;
    partners: Partner[] = [];
    products: Product[] = [];
    companies: Company[] = [];
    private companyId: string;
    private partnerId: string;
    private userId: string;

  constructor(public dialogRef: MatDialogRef<AddPartnerDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private fb: FormBuilder,
              public utilsService: UtilsService,
              public authService: AuthService,
              private router: Router) { }

  ngOnInit() {
      this.userId = this.authService.getUserId();
      this.form = this.fb.group({
          company: [''],
          partner: [''],
          import_value: [''],
          export_value: [''],
          daily_freight: ['']
      });
      this.getCompanyByUserId();
  }

    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const companyList = resp.data.filter(r => r);
                _this.companies = companyList;
            }
        });
    }

    pickedCompany(i) {
        this.companyId = this.companies[i]['_id'];
        this.getPartnerByCompany();
    }

    getPartnerByCompany() {
        const _this = this;
        this.utilsService.reqGET('/partner/company/' + _this.companyId, null, resp => {
            if (resp && resp.code === 20000) {
                // const partnerList = resp.data.filter(idCom => idCom);
                const partnerList = resp.data.filter(p => p._id !== _this.data.pnId._id);
                _this.partners = partnerList;
                console.log(_this.partners);
                if (_this.partners.length === 0) {
                    return false;
                }
            }
        });
    }

    addSupport() {
        const params = {
            'support': [{
                'partners': this.form.value.partner,
                'charged_product': {
                    'import_value': this.form.value.import_value,
                    'export_value': this.form.value.export_value,
                    'daily_freight': this.form.value.daily_freight
                }
            }]
        };
        this.utilsService.reqPUT('/management/product/serial/' + this.data.pId + '/' + 'support', params, resp => {
            if (resp && resp.code === 20000) {
                this.router.navigate(['/product/product-by-partner', this.data.pId]);
                this.onNoClick();
            }
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    //
    // // addWarehouseByCompany() {
    // //     const warehouseId = localStorage.getItem('warehouseId');
    // //     const companyId = localStorage.getItem('companyId');
    // //     const params = {
    // //         'support': [{
    // //             'company': companyId,
    // //             'charged_warehouse': {
    // //                 'import_value': this.form.value.import_value,
    // //                 'daily_freight': this.form.value.daily_freight,
    // //                 'export_value': this.form.value.export_value
    // //             }
    // //         }]
    // //     };
    // //     this.utilsService.reqPUT('/warehouse/' + warehouseId + '/' + 'support/company', params, resp => {
    // //         if (resp && resp.code === 20000) {
    // //             console.log(resp);
    // //             this.router.navigate(['/warehouse/warehouse-by-company']);
    // //         }
    // //     });
    // // }
    //


}
