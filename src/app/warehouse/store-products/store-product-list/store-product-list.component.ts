import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {StoreProduct} from '../../../models/StoreProduct.model';
import {UtilsService} from '../../../helper/utils.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

const backend_url = environment.apiUrl;

@Component({
    selector: 'app-store-product-list',
    templateUrl: './store-product-list.component.html',
    styleUrls: ['./store-product-list.component.scss']
})
export class StoreProductListComponent implements OnInit {

    displayedColumns = [];
    storeProducts: StoreProduct[] = [];
    dataSource;
    companyId: string;
    warehouseId: string;
    // storeProductId: string;
    // exportId: string;
    // tagCode: string;
    @ViewChild(MatSort) sort: MatSort;

    constructor(public utilsService: UtilsService,
                private http: HttpClient) {

        this.getStoreProduct();
        this.displayedColumns = ['number', 'company', 'warehouse', 'serial_product', 'partner'];

    }

    ngOnInit() {

    }

    // get all product in all warehouse.
    getStoreProduct() {
        const _this = this;
        this.http.get(backend_url + '/management/product/stores').subscribe(resp => {
            if (resp && resp['code'] === 20000) {
                const storeProductList = resp['data'].filter(r => {
                    return r.serial_product;
                });
                _this.storeProducts = storeProductList;
                _this.dataSource = new MatTableDataSource(_this.storeProducts);
                _this.dataSource.sort = _this.sort;
            }
        });
    }

    // pickedStoreProduct(i: number) {
    //  this.storeProductId = this.dataSource.data[i]._id;
    //  this.getStoreProductById();
    //  this.updateStoreProduct();
    // }

    // getStoreProductById() {
    //   const _this = this;
    //   this.utilsService.reqGET('/management/product/store/' + _this.storeProductId, null, resp => {
    //    if (resp) {
    //      _this.companyId = resp.data.company._id;
    //      _this.warehouseId = resp.data.warehouse._id;
    //      _this.exportId = resp.data.partner.import._id;
    //      _this.tagCode = resp.data.tag_code;
    //    }
    //   });
    //   return false;
    // }

    // updateStoreProduct() {
    //   const _this = this;
    //   const params = {
    //     'company': this.companyId,
    //     'warehouse': this.warehouseId,
    //     'partner': {
    //       'export': this.exportId
    //     },
    //     'tag_code': this.tagCode
    //   }
    //   this.utilsService.reqPUT('/management/product/store/export/' + _this.storeProductId, params, resp => {
    //     console.log(resp);
    //   });
    // }

}
