import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSort, MatTableDataSource} from '@angular/material';
import {StoreProduct} from '../../../models/StoreProduct.model';
import {ActivatedRoute, Router} from '@angular/router';
import {ExportDialogComponent} from '../store-product-by-partner/store-product-by-partner.component';
import {DialogData} from '../../../models/Dialog.model';
import {Partner} from '../../../models/Partner.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Angular5Csv} from 'angular5-csv/Angular5-csv';

@Component({
    selector: 'app-store-product-by-company-warehouse',
    templateUrl: './store-product-by-company-warehouse.component.html',
    styleUrls: ['./store-product-by-company-warehouse.component.scss']
})
export class StoreProductByCompanyWarehouseComponent implements OnInit {
    public companyId: string;
    public warehouseId: string;
    public productName: string;
    public companyName: string;
    public partnerName: string;
    public partnerId: string;
    private productId: string;
    warehouseName: string;
    public exportArray = [];
    dataSource;
    displayedColumns = [];
    storeProducts: StoreProduct[] = [];
    @ViewChild(MatSort) sort: MatSort;

    constructor(public utilsService: UtilsService,
                private route: ActivatedRoute,
                public dialog: MatDialog) {

        this.warehouseId = this.route.snapshot.paramMap.get('warehouseId');
        this.companyId = this.route.snapshot.paramMap.get('companyId');
        this.getStoreProductByCompanyWarehouse();

    }

    ngOnInit() {
        this.displayedColumns = ['no', 'serial_product', 'product_title', 'product_volume', 'out', 'remain', 'actions'];
    }

    getStoreProductByCompanyWarehouse() {
        const _this = this;
        this.utilsService.reqGET('/management/product/count/' + _this.companyId + '/' + _this.warehouseId, null, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
                const data = resp.data.filter(r => r);
                const warehouseName = resp.data[0].warehouse.title;
                _this.warehouseName = warehouseName;
                _this.storeProducts = data;
                _this.dataSource = new MatTableDataSource(_this.storeProducts);
                _this.dataSource.sort = _this.sort;
                // _this.dataSource = _this.exportArray;
                for (let i = 0; i < _this.dataSource.data.length; i++) {
                    _this.exportArray.push({
                        'No.': i + 1,
                        'Serial_product': resp.data[i].serial_product.serial_product,
                        'Product Name': resp.data[i].serial_product.title,
                        'Volume': resp.data[i].total,
                        'Out': resp.data[i].remain
                    });
                }
            }
        });
    }

    onSelect(i: number) {
        const productName = this.dataSource.data[i].serial_product.title;
        const productId = this.dataSource.data[i].serial_product._id;
        const companyName = this.dataSource.data[i].company.title;
        const companyId = this.dataSource.data[i].company._id;
        this.productName = productName;
        this.productId = productId;
        this.companyName = companyName;
        this.companyId = companyId;
        this.openExportDialog();
    }

    exportCsv() {
        const options = {
            // fieldSeparator: ',',
            // quoteStrings: '"',
            // decimalseparator: '.',
            // showLabels: true,
            // showTitle: true,
            // useBom: true,
            // noDownload: false,
            headers: ['No.', 'Serial_product', 'Product Name', 'Volume', 'Out', 'Remain']
        };
        const header = ['No.', 'Serial_product', 'Product Name', 'Volume', 'Out', 'Remain'];
        new Angular5Csv(this.exportArray, 'Product of ' + '' + this.warehouseName,
            // {headers: (header)}
            options
        );
    }

    openExportDialog(): void {
        const dialogRef = this.dialog.open(ExportDialogComponent, {
            disableClose: true,
            width: '250px',
            data: {pdName: this.productName, cName: this.companyName, pnName: this.partnerName, pnId: this.partnerId, cId: this.companyId}
        });

        dialogRef.afterClosed().subscribe(result => {
            // console.log('The dialog was closed');
        });
    }

    openPartnerDialog(): void {
        const dialogRef = this.dialog.open(PartnerDialogComponent, {
            disableClose: true,
            width: '250px',
            data: {cId: this.companyId, wId: this.warehouseId}
        });
        dialogRef.afterClosed().subscribe(result => {
            // console.log('The dialog was closed');
        });
    }

}

@Component({
    selector: 'app-partner-dialog',
    template: '<h1 mat-dialog-title>Select Partner</h1>\n' +
        '<div mat-dialog-content style="height: 150px;">\n' +
        '<p>Please select partner.</p>\n' +
        ' <form [formGroup]="form">\n' +
        '    <div class="formGroup">\n' +
        '  <mat-form-field>\n' +
        '        <mat-select placeholder="Select Partner" formControlName="partner">\n' +
        '          <mat-option *ngFor="let partner of partners;" [value]="partner._id">\n' +
        '            {{partner.title}}\n' +
        '          </mat-option>\n' +
        '        </mat-select>\n' +
        '      </mat-form-field>\n' +
        '</div>\n' +
        '<div mat-dialog-actions>\n' +
        '  <button mat-button (click)="onNoClick()">Cancel</button>\n' +
        '  <button mat-button cdkFocusInitial (click)="importProduct()">Ok</button>\n' +
        '</div>\n' +
        '</form>\n' +
        '</div>',
    styleUrls: ['./store-product-by-company-warehouse.component.scss']
})
export class PartnerDialogComponent implements OnInit {

    partners: Partner[] = [];
    partnerId: string;
    form: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<PartnerDialogComponent>,
        private router: Router,
        public utilsService: UtilsService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private fb: FormBuilder) {
    }

    ngOnInit() {
        this.getPartnerByCompany();
        this.form = this.fb.group({
            partner: ''
        });
    }

    getPartnerByCompany() {
        const _this = this;
        this.utilsService.reqGET('/partner/company/' + _this.data.cId, null, resp => {
            if (resp && resp.code === 20000) {
                const partnerList = resp.data.filter(idCom => idCom.company === _this.data.cId);
                _this.partners = partnerList;
            }
        });
    }

    importProduct() {
        this.router.navigate(['/store-product/store-product-create', this.data.cId, this.form.value.partner, this.data.wId]);
        this.dialogRef.close();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}

