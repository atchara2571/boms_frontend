import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {UtilsService} from '../../../helper/utils.service';
import {StoreProduct} from '../../../models/StoreProduct.model';

@Component({
  selector: 'app-store-product-by-company',
  templateUrl: './store-product-by-company.component.html',
  styleUrls: ['./store-product-by-company.component.scss']
})
export class StoreProductByCompanyComponent implements OnInit {

  dataSource;
  displayedColumns = [];
  storeProducts: StoreProduct[] = [];
  @ViewChild(MatSort) sort: MatSort;

  constructor(public utilsService: UtilsService) {

      this.getStoreProductByCompany();

  }

  ngOnInit() {
    this.displayedColumns = ['number', 'storeProductName', 'actions'];
  }

  getStoreProductByCompany() {
    const _this = this;
    const companyId = localStorage.getItem('companyId');
    this.utilsService.reqGET('/management/product/company/' + companyId, null, resp => {
      if (resp && resp.code === 20000) {
        const data = resp.data.filter(idCom => idCom.company === companyId);
        _this.storeProducts = data;
        _this.dataSource = new MatTableDataSource(_this.storeProducts);
        _this.dataSource.sort = _this.sort;
      }
    });
  }

}
