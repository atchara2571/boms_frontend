import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StoreProductListComponent} from './store-product-list/store-product-list.component';
import {StoreProductCreateComponent} from './store-product-create/store-product-create.component';
import {StoreProductByCompanyComponent} from './store-product-by-company/store-product-by-company.component';
import {StoreProductByPartnerComponent} from './store-product-by-partner/store-product-by-partner.component';
import {StoreProductByWarehouseComponent} from './store-product-by-warehouse/store-product-by-warehouse.component';
import {StoreProductByCompanyWarehouseComponent} from './store-product-by-company-warehouse/store-product-by-company-warehouse.component';
import {StoreProductByCompanyWarehouseAllComponent} from './store-product-by-company-warehouse-all/store-product-by-company-warehouse-all.component';
import {StoreProductByCompanyWarehousePartnerComponent} from './store-product-by-company-warehouse-partner/store-product-by-company-warehouse-partner.component';
import {ImportFileComponent} from './import-file/import-file.component';
import {ExportFileComponent} from './export-file/export-file.component';
import {StoreProductCreateTableComponent} from './store-product-create-table/store-product-create-table.component';

const routes: Routes = [
  // {
  //   path: '',
  //   component: StoreProductListComponent,
  //   data: {
  //     heading: 'Product Store'
  //   }
  // },
  {
    path: 'store-product-list',
    component: StoreProductListComponent,
    data: {
      heading: 'Product Store'
    }
  },
  {
    path: 'store-product-create/:companyId/:partnerId/:warehouseId',
    component: StoreProductCreateComponent,
    data: {
      heading: 'Create Product Store'
    }
  },
  {
    path: 'store-product-by-company',
    component: StoreProductByCompanyComponent,
    data: {
      heading: 'Store Product by Company'
    }
  },
  {
    path: 'store-product-by-partner/:partnerId/:partnerName/:companyId',
    component: StoreProductByPartnerComponent,
    data: {
      heading: 'Store Product by Partner'
    }
  },
  {
    path: 'store-product-by-warehouse/:id',
    component: StoreProductByWarehouseComponent,
    data: {
      heading: 'Store Product by Warehouse'
    }
  },
  {
    path: 'store-product-by-company-warehouse/:companyId/:warehouseId',
    component: StoreProductByCompanyWarehouseComponent,
    data: {
      heading: 'Store Product by Company and Warehouse'
    }
  },
  {
    path: 'store-product-by-company-warehouse-all/:companyId',
    component: StoreProductByCompanyWarehouseAllComponent,
    data: {
      heading: 'Store Product by Company and Warehouse All'
    }
  },
  {
    path: 'store-product-by-company-warehouse-partner/:companyId/:warehouseId/:partnerId/:partnerName',
    component: StoreProductByCompanyWarehousePartnerComponent,
    data: {
      heading: 'Store Product by Company Warehouse and Partner'
    }
  },
  {
    path: 'import-file',
    component: ImportFileComponent,
    data: {
      heading: 'Import File'
    }
  },
  {
    path: 'export-file',
    component: ExportFileComponent,
    data: {
      heading: 'Export File'
    }
  },
  {
    path: 'store-product-create-table/:companyId/:partnerId',
    component: StoreProductCreateTableComponent,
    data: {
      heading: 'store product create by table'
    }
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class StoreProductRouting {}
