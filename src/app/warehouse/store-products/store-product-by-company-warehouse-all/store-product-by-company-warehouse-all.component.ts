import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {UtilsService} from '../../../helper/utils.service';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-store-product-by-company-warehouse-all',
    templateUrl: './store-product-by-company-warehouse-all.component.html',
    styleUrls: ['./store-product-by-company-warehouse-all.component.scss']
})
export class StoreProductByCompanyWarehouseAllComponent implements OnInit {

    public companyId: string;
    public companyName: string;
    public warehouseId: string;
    dataSource;
    displayedColumns = [];
    whList = [];
    @ViewChild(MatSort) sort: MatSort;

    constructor(public utilsService: UtilsService,
                private route: ActivatedRoute,
                public dialog: MatDialog) {

        this.warehouseId = this.route.snapshot.paramMap.get('warehouseId');
        this.companyId = this.route.snapshot.paramMap.get('companyId');
        this.getStoreProductByCompany();

    }

    ngOnInit() {
        this.displayedColumns = ['no', 'serial_product', 'product_title', 'product_type', 'product_volume', 'remain', 'actions'];
    }

    getStoreProductByCompany() {
        const _this = this;
        this.utilsService.reqGET('/management/product/stores/' + _this.companyId, null, resp => {
            if (resp) {
                const data = resp.data.filter(r => r);
                const companyName = resp.data[0].company.title;
                _this.companyName = companyName;
                _this.loopGetWareHouse([], data, 0, function (resp2) {
                    _this.whList = resp2;
                });
            }
        });
    }

    loopGetWareHouse(respList, whList, index, callback) {
        const _this = this;
        if (whList.length > 0 && !whList[index]) {
            if (typeof callback === 'function') {
                callback(respList);
            }
        } else {
            this.utilsService.reqGET('/management/product/count/' + _this.companyId + '/' + whList[index].warehouse._id, null, resp => {
                if (resp) {
                    const data = resp.data.filter(r => r);
                    const storeProducts = data;
                    const dataSource = new MatTableDataSource(storeProducts);
                    dataSource.sort = _this.sort;
                    respList.push(dataSource);
                }
                index++;
                _this.loopGetWareHouse(respList, whList, index, callback);
            });
        }
    }

    // openImportDialog(): void {
    //     const dialogRef = this.dialog.open(ImportDialogComponent, {
    //         width: '250px',
    //         data: {wId: this.warehouseId}
    //     });
    //
    //     dialogRef.afterClosed().subscribe(result => {
    //         console.log('The dialog was closed');
    //     });
    // }

}
