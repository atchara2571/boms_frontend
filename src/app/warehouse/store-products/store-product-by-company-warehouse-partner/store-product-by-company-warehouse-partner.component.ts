import {Component, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {ExportDialogComponent} from '../store-product-by-partner/store-product-by-partner.component';
import {Angular5Csv} from 'angular5-csv/Angular5-csv';

@Component({
    selector: 'app-store-product-by-company-warehouse-partner',
    templateUrl: './store-product-by-company-warehouse-partner.component.html',
    styleUrls: ['./store-product-by-company-warehouse-partner.component.scss']
})
export class StoreProductByCompanyWarehousePartnerComponent implements OnInit {

    public warehouseId: string;
    public partnerId: string;
    public partnerName: string;
    public warehouseName: string;
    public productName: string;
    public companyName: string;
    public companyId: string;
    private productId: string;
    public remain2 = [];
    public exportArray = [];
    dataSource;
    displayedColumns = [];
    // storeProducts: StoreProduct[] = [];
    storeProducts = [];

    // @ViewChild(StoreProductByPartnerComponent) child;
    @ViewChild(MatSort) sort: MatSort;

    constructor(public utilsService: UtilsService,
                private route: ActivatedRoute,
                private router: Router,
                public dialog: MatDialog) {

        this.companyId = this.route.snapshot.paramMap.get('companyId');
        this.warehouseId = this.route.snapshot.paramMap.get('warehouseId');
        this.partnerId = this.route.snapshot.paramMap.get('partnerId');
        this.partnerName = this.route.snapshot.paramMap.get('partnerName');
        this.getProductInWarehouseByCWP();

    }

    ngOnInit() {
        this.displayedColumns = ['no', 'serial_product', 'product_title', 'product_volume', 'out', 'remain', 'actions'];
    }

    getProductInWarehouseByCWP() {
        const _this = this;
        this.utilsService.reqGET('/management/product/count', _this.companyId + '/' + _this.warehouseId + '/' + _this.partnerId,
            resp => {
                if (resp && resp.code === 20000) {
                    console.log(resp);
                    const dataList = resp.data.filter(r => r);
                    const warehouseName = resp.data[0].warehouse.title;
                    const remain = resp.data.filter(r => r.total - r.remain);
                    _this.remain2 = remain;
                    console.log(remain);
                    _this.warehouseName = warehouseName;
                    _this.storeProducts = dataList;
                    _this.dataSource = new MatTableDataSource(_this.storeProducts);
                    _this.dataSource.sort = _this.sort;

                    for (let i = 0; i < _this.dataSource.data.length; i++) {
                        _this.exportArray.push({
                            'No.': i + 1,
                            'Serial_product': resp.data[i].serial_product.serial_product,
                            'Product Name': resp.data[i].serial_product.title,
                            'Volume': resp.data[i].total,
                            'Out': resp.data[i].remain
                        });
                    }
                }
            });
    }

    onSelect(i: number) {
        const productName = this.dataSource.data[i].serial_product.title;
        const productId = this.dataSource.data[i].serial_product._id;
        const companyName = this.dataSource.data[i].company.title;
        const companyId = this.dataSource.data[i].company._id;
        this.productName = productName;
        this.productId = productId;
        this.companyName = companyName;
        this.companyId = companyId;
        console.log(productId);
        // this.getStoreProductByPartner();
        this.openDialog();
    }

    addProduct() {
        this.router.navigate(['/store-product/store-product-create', this.companyId, this.partnerId]);
    }

    exportCsv() {
        const options = {
            headers: ['No.', 'Serial_product', 'Product Name', 'Volume', 'Out', 'Remain']
        };
        const header = ['No.', 'Serial_product', 'Product Name', 'Volume', 'Out', 'Remain'];
        new Angular5Csv(this.exportArray, 'Product of' + ' ' + this.warehouseName,
            // {headers: (header)}
            options
        );
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(ExportDialogComponent, {
            disableClose: true,
            width: '250px',
            data: {pdName: this.productName, cName: this.companyName, pnName: this.partnerName, pnId: this.partnerId, cId: this.companyId}
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }


}
