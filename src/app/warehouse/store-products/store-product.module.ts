import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatInputModule} from '@angular/material';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {AngularMaterialModule} from '../../angular-material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StoreProductListComponent} from './store-product-list/store-product-list.component';
import {ConfirmDialogComponent, StoreProductCreateComponent} from './store-product-create/store-product-create.component';
import {StoreProductRouting} from './store-product.routing';
import {StoreProductByCompanyComponent} from './store-product-by-company/store-product-by-company.component';
import {ExportDialogComponent, StoreProductByPartnerComponent} from './store-product-by-partner/store-product-by-partner.component';
import {StoreProductByWarehouseComponent} from './store-product-by-warehouse/store-product-by-warehouse.component';
import {
    PartnerDialogComponent,
    StoreProductByCompanyWarehouseComponent
} from './store-product-by-company-warehouse/store-product-by-company-warehouse.component';
import {StoreProductByCompanyWarehouseAllComponent} from './store-product-by-company-warehouse-all/store-product-by-company-warehouse-all.component';
import {StoreProductByCompanyWarehousePartnerComponent} from './store-product-by-company-warehouse-partner/store-product-by-company-warehouse-partner.component';
import {ImportFileComponent} from './import-file/import-file.component';
import {ExportFileComponent} from './export-file/export-file.component';
import {StoreProductCreateTableComponent} from './store-product-create-table/store-product-create-table.component';

// import { StoreProductByCWPDialogComponent } from './export-modal.component';

@NgModule({
    imports: [
        CommonModule,
        StoreProductRouting,
        MatInputModule,
        NgxDatatableModule,
        AngularMaterialModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        StoreProductListComponent,
        StoreProductCreateComponent,
        StoreProductByCompanyComponent,
        StoreProductByPartnerComponent,
        ExportDialogComponent,
        ConfirmDialogComponent,
        PartnerDialogComponent,
        StoreProductByWarehouseComponent,
        StoreProductByCompanyWarehouseComponent,
        StoreProductByCompanyWarehouseAllComponent,
        StoreProductByCompanyWarehousePartnerComponent,
        ImportFileComponent,
        ExportFileComponent,
        StoreProductCreateTableComponent
    ],
    entryComponents: [
        ExportDialogComponent,
        ConfirmDialogComponent,
        PartnerDialogComponent
    ]
})

export class StoreProductModule {
}
