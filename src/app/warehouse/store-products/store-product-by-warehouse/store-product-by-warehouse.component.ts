import {Component, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {StoreProduct} from '../../../models/StoreProduct.model';
import {MatSort, MatTableDataSource} from '@angular/material';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-store-product-by-warehouse',
    templateUrl: './store-product-by-warehouse.component.html',
    styleUrls: ['./store-product-by-warehouse.component.scss']
})
export class StoreProductByWarehouseComponent implements OnInit {

    public warehouseId: string;
    public companyId: string;
    dataSource;
    storeProducts: StoreProduct[] = [];
    displayedColumns = [];
    @ViewChild(MatSort) sort: MatSort;

    constructor(public utilsService: UtilsService,
                private route: ActivatedRoute) {

        this.warehouseId = this.route.snapshot.paramMap.get('warehouseId');
        this.companyId = this.route.snapshot.paramMap.get('companyId');
        this.getStoreProductByCompanyWarehouse();

    }

    ngOnInit() {
        this.displayedColumns = ['no', 'serial_product', 'product_title', 'product_type', 'product_volume', 'remain', 'actions'];
    }

    // get product in warehouse sort by matching company and warehouse.
    getStoreProductByCompanyWarehouse() {
        const _this = this;
        this.utilsService.reqGET('/management/product/count/' + _this.companyId + '/' + _this.warehouseId, null, resp => {
            if (resp) {
                const data = resp.data.filter(r => r);
                _this.storeProducts = data;
                _this.dataSource = new MatTableDataSource(_this.storeProducts);
                _this.dataSource.sort = _this.sort;
            }
        });
    }

}
