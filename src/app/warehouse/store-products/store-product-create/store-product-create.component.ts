import {Component, OnInit, Inject} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Company} from '../../../models/Company.model';
import {Partner} from '../../../models/Partner.model';
import {Warehouse} from '../../../models/Warehouse.model';
import {UtilsService} from '../../../helper/utils.service';
import {Product} from '../../../models/Product.model';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material';
import {Location} from '@angular/common';
import {AuthService} from '../../../helper/auth.service';

@Component({
    selector: 'app-store-product-create',
    templateUrl: './store-product-create.component.html',
    styleUrls: ['./store-product-create.component.scss']
})
export class StoreProductCreateComponent implements OnInit {

    file: File;
    form: FormGroup;
    companies: Company[] = [];
    partners: Partner[] = [];
    warehouses: Warehouse[] = [];
    products: Product[] = [];
    warehouseId: string;
    productId: string;
    companyId: string;
    companyName: string;
    partnerId: string;
    partnerName: string;
    private userId: string;

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private fb: FormBuilder,
                private router: Router,
                public dialog: MatDialog,
                private location: Location,
                private route: ActivatedRoute) {

        this.companyId = this.route.snapshot.paramMap.get('companyId');
        this.partnerId = this.route.snapshot.paramMap.get('partnerId');
        this.warehouseId = this.route.snapshot.paramMap.get('warehouseId');
        this.userId = this.authService.getUserId();

    }

    ngOnInit() {
        this.fetchStoreProductForm();
        this.getCompanyByUserId();
        this.getCPWById();
        this.getWarehouseByCompany();
        this.getProductByPartner();
    }

    fetchStoreProductForm() {
        this.form = this.fb.group({
            company: [''],
            partner: [''],
            warehouse: [''],
            // product: [''],
            serial_product: [''],
            // file: ['']
        });
    }

    // get matching company and partner sort by company id.
    getCPWById() {
        // const _this = this;
        this.utilsService.reqGET('/companys/' + this.companyId, null, resp => {
            if (resp && resp.code === 20000) {
                const companyList = resp.data.filter(c => c);
                this.companies = companyList;

                this.utilsService.reqGET('/partner/' + this.partnerId, null, respP => {
                    if (respP && respP.code === 20000) {
                        const partnerList = respP.data;
                        this.partners.push(partnerList);

                        this.utilsService.reqGET('/warehouse/' + this.warehouseId, null, respW => {
                            if (respW && respW.code === 20000) {
                               const warehouseList = respW.data;
                               this.warehouses.push(warehouseList);

                                this.form.setValue({
                                    company: resp.data[0]._id,
                                    partner: partnerList._id,
                                    warehouse: warehouseList._id,
                                    // product: [''],
                                    serial_product: [''],
                                    // file: ['']
                                });

                            }
                        });
                    }
                });

            }
        });
    }

    // get company sort by user id.
    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const comList = resp.data.filter(c => c);
                _this.companies = comList;
            }
        });
    }

    // get warehouse sort by company.
    getWarehouseByCompany() {
        const _this = this;
        this.utilsService.reqGET('/warehouse/company/' + _this.companyId, null, resp => {
            if (resp && resp.code === 20000) {
                const data = resp.data.filter(warehouse => {
                    return warehouse;
                });
                _this.warehouses = data;
            }
        });
    }

    // get product sort by partner.
    getProductByPartner() {
        const _this = this;
        this.utilsService.reqGET('/management/product/serials/' + _this.partnerId, null, resp => {
            if (resp && resp.code === 20000) {
                if (resp.data.length === 0) {
                    _this.openDialog();
                }
                const productList = resp.data.filter(response => response);
                _this.products = productList;
            }
        });
    }

    // confirm partner selection dialog.
    openDialog(): void {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            disableClose: true,
            width: '250px'
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    // submit import new product to warehouse.
    importNewProduct() {
        const params = {
            'company': this.form.value.company,
            'warehouse': this.form.value.warehouse,
            'partner': {
                'import': this.form.value.partner
            },
            'serial_product': this.form.value.serial_product
        };
        if (this.form.value.warehouse && this.form.value.serial_product) {
            this.utilsService.reqPOST('/management/product/store/import', params, resp => {
                if (resp && resp.code === 20000) {
                    alert('Success!');
                    this.location.back();
                }
            });
        } else {
            alert('Please select warehouse and product');
        }
    }

    // go to product import file page.
    fileImport() {
        this.router.navigate(['/store-product/import-file']);
    }

    // go to multiple product import page.
    multipleImport() {
        this.router.navigate(['/store-product/store-product-create-table', this.companyId, this.partnerId]);
    }

}

// no product found dialog.
@Component({
    selector: 'app-confirm-dialog',
    template: '<h1 mat-dialog-title>No product found!</h1>\n' +
        '<div mat-dialog-content>\n' +
        '  <p>Click create product button to create new product</p>\n' +
        '</div>\n' +
        '<div mat-dialog-actions>\n' +
        '  <button mat-button (click)="onNoClick()">Cancel</button>\n' +
        '  <button mat-button cdkFocusInitial (click)="createNewProduct()">Create</button>\n' +
        '</div>',
})
export class ConfirmDialogComponent {

    constructor(
        public dialogRef: MatDialogRef<ConfirmDialogComponent>, private router: Router) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    // go to create new product page.
    createNewProduct() {
        this.router.navigate(['/product/product-create']);
        this.dialogRef.close();
    }
}
