import {Component, Inject, OnInit, ViewChild, Output, EventEmitter} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSort, MatTableDataSource} from '@angular/material';
import {StoreProduct} from '../../../models/StoreProduct.model';
import {UtilsService} from '../../../helper/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Partner} from '../../../models/Partner.model';
import {Warehouse} from '../../../models/Warehouse.model';
import {Angular5Csv} from 'angular5-csv/Angular5-csv';
import {DialogData} from '../../../models/Dialog.model';

@Component({
    selector: 'app-store-product-by-partner',
    templateUrl: './store-product-by-partner.component.html',
    styleUrls: ['./store-product-by-partner.component.scss']
})
export class StoreProductByPartnerComponent implements OnInit {
    dataSource;
    displayedColumns = [];
    storeProducts: StoreProduct[] = [];
    public exportArray = [];
    public products = [];
    private productId: string;
    public partnerName: string;
    public partnerId: string;
    public companyId: string;
    public companyRoute: string;
    public productName: string;
    public companyName: string;
    public warehouseId: string;

    @ViewChild(MatSort) sort: MatSort;

    constructor(public utilsService: UtilsService,
                private router: Router,
                private route: ActivatedRoute,
                public dialog: MatDialog) {

        this.partnerId = this.route.snapshot.paramMap.get('partnerId');
        this.partnerName = this.route.snapshot.paramMap.get('partnerName');
        this.warehouseId = this.route.snapshot.paramMap.get('warehouseId');
        this.companyRoute = this.route.snapshot.paramMap.get('companyId');
        this.getStoreProductByPartnerCount();

    }

    ngOnInit() {
        this.displayedColumns = ['no', 'serial_product', 'product_title', 'product_volume', 'out', 'remain', 'actions'];
    }

    // get product in warehouse sort by partner.
    getStoreProductByPartnerCount() {
        const _this = this;
        this.utilsService.reqGET('/management/product/count', this.partnerId, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
                const companyId = resp.data.map(c => c.company);
                _this.companyId = companyId;
                const data = resp.data.filter(r => r);
                _this.storeProducts = data;
                _this.dataSource = new MatTableDataSource(_this.storeProducts);
                _this.dataSource.sort = _this.sort;
                for (let i = 0; i < _this.dataSource.data.length; i++) {
                    _this.exportArray.push({
                        'No.': i + 1,
                        'Serial_product': resp.data[i].serial_product.serial_product,
                        'Product Name': resp.data[i].serial_product.title,
                        'Volume': resp.data[i].total,
                        'Out': resp.data[i].remain
                    });
                }
            }
        });
    }

    // icon event.
    onSelect(i: number) {
        const productName = this.dataSource.data[i].serial_product.title;
        const productId = this.dataSource.data[i].serial_product._id;
        const companyName = this.dataSource.data[i].company.title;
        const companyId = this.dataSource.data[i].company._id;
        this.productName = productName;
        this.productId = productId;
        this.companyName = companyName;
        this.companyId = companyId;
        // this.getStoreProductByPartner();
        this.openDialog();
    }

    // pickedStoreProduct(i: number) {
    //   this.storeProductId = this.dataSource.data[i]._id;
    //   this.companyId = this.dataSource.data[i].company._id;
    //   this.warehouseId = this.dataSource.data[i].warehouse._id;
    //   this.exportId = this.dataSource.data[i].partner.import._id;
    //   this.tagCode = this.dataSource.data[i].tag_code;
    //   //   this.updateStoreProduct();
    //   this.openDialog();
    // }

    // import new product to warehouse(store).
    public addPNormal() {
        this.router.navigate(['/store-product/store-product-create', this.companyRoute, this.partnerId]);
    }

    // export product dialog.
    openDialog(): void {
        const dialogRef = this.dialog.open(ExportDialogComponent, {
            disableClose: true,
            width: '250px',
            data: {pdName: this.productName, cName: this.companyName, pnName: this.partnerName, pnId: this.partnerId, cId: this.companyId}
        });

        dialogRef.afterClosed().subscribe(result => {
        });
    }

    // download as csv. file.
    exportCsv() {
        const options = {
            headers: ['No.', 'Serial_product', 'Product Name', 'Volume', 'Out', 'Remain']
        };
        new Angular5Csv(this.exportArray, 'Product of' + ' ' + this.partnerName, options);
    }

}


// export product dialog component.
@Component({
    // providers: [StoreProductByPartnerComponent],
    selector: 'app-export-dialog',
    templateUrl: '../export-modal.html'
})

export class ExportDialogComponent implements OnInit {

    form: FormGroup;
    warehouses: Warehouse[] = [];
    partners: Partner[] = [];
    tagCodes = [];
    private tagCodeId: string;
    private storeProductId: string;
    public warehouseId: string;
    public partnerId: string;
    public companyId: string;

    constructor(
        public dialogRef: MatDialogRef<ExportDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private fb: FormBuilder,
        public utilsService: UtilsService,
        private router: Router,
        private route: ActivatedRoute) {

        this.companyId = this.route.snapshot.paramMap.get('companyId');
        this.partnerId = this.route.snapshot.paramMap.get('partnerId');
        this.getWarehouseByPartner();
    }

    ngOnInit() {
        this.fetchExportProductForm();
        // this.getPartner();
    }

    ngAfterViewInit() {
    }

    // fetch export dialog form.
    fetchExportProductForm() {
        this.form = this.fb.group({
            warehouse: [''],
            tagCode: ['']
        });
    }

    // get warehouse sort by partner.
    getWarehouseByPartner() {
        const _this = this;
        this.utilsService.reqGET('/warehouses', this.data.pnId, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
                const whList = resp.data.filter(w => w);
                _this.warehouses = whList;
            }
        });
    }

    // picked warehouse id.
    pickedWarehouse(i) {
        this.warehouseId = this.warehouses[i]._id;
        this.getStoreProductByPartner();
    }

    // picked tag code.
    pickedTagCode(i) {
        this.tagCodeId = this.tagCodes[i].tag_code;
        this.storeProductId = this.tagCodes[i]._id;
    }

    // getStoreProductByCompanyWarehouse() {
    //   const _this = this;
    //   this.utilsService.reqGET('/management/product/stores/' + _this.data.cId + '/' + _this.warehouseId, null, resp => {
    //     if (resp && resp.code === 20000) {
    //       const tagCodeList = resp.data.filter(w => w.warehouse._id === _this.warehouseId);
    //       _this.tagCodes = tagCodeList;
    //     }
    //   });
    // }
    //
    // getWarehouseByPartner() {
    //   const _this = this;
    //   this.utilsService.reqGET('/warehouses', this.data.pnId, resp => {
    //     if (resp && resp.code === 20000) {
    //       console.log(resp);
    //       const whList = resp.data.filter(w => w);
    //       const s = resp.data.filter(uId => uId.support.find(l => l.length !== 0) && whList);
    //       _this.warehouses = s;
    //     }
    //   });
    // }

    // get product in warehouse sort by partner.
    getStoreProductByPartner() {
        const _this = this;
        this.utilsService.reqGET('/management/product/partner/import', _this.data.pnId, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
                const list = resp.data.filter(r => r);
                // const tagCodeList = resp.data.filter(w => w.warehouse._id === _this.warehouseId);
                // console.log(tagCodeList);
                _this.tagCodes = list;
            }
        });
    }

    // submit export product.
    exportProduct() {
        const _this = this;
        const params = {
            'company': this.data.cId,
            'warehouse': this.warehouseId,
            'partner': {
                'export': this.data.pnId
            },
            'tag_code': this.tagCodeId
        };
        this.utilsService.reqPUT('/management/product/store/export/' + _this.storeProductId, params, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
            }
        });
        this.onNoClick();
    }

    // getPartner() {
    //   const _this = this;
    //   this.utilsService.reqGET('/partners', null, resp => {
    //     if (resp) {
    //       const partnerList = resp.data.filter(r => r._id !== _this.partnerId);
    //       console.log(partnerList);
    //       // _this.partners = partnerList;
    //     }
    //   });
    // }

    // updateStoreProduct() {
    //   const _this = this;
    //   const params = {
    //     'company': this.companyId,
    //     'warehouse': this.warehouseId,
    //     'partner': {
    //       'export': this.exportId
    //     },
    //     'tag_code': this.tagCode
    //   };
    //   this.utilsService.reqPUT('/management/product/store/export/' + _this.storeProductId, params, resp => {
    //     console.log(resp);
    //   });
    //   this.dialogRef.close();
    // }

    // close dialog.
    onNoClick(): void {
        this.dialogRef.close();
    }

}
