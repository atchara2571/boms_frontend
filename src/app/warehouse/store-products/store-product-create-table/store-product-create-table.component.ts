import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {UtilsService} from '../../../helper/utils.service';
import {Product} from '../../../models/Product.model';
import {Warehouse} from '../../../models/Warehouse.model';
import {Location} from '@angular/common';
import index from '@angular/cli/lib/cli';

export interface Element {
    number: number;
    warehouse: string;
    product: string;
}

const ELEMENT_DATA: Element[] = [
    {
        number: 1, warehouse: 'warehouse 1', product: 'product 1'
    }
];

@Component({
    selector: 'app-store-product-create-table',
    templateUrl: './store-product-create-table.component.html',
    styleUrls: ['./store-product-create-table.component.scss']
})
export class StoreProductCreateTableComponent implements OnInit {

    public warehouseId: string;
    public productId: string;
    public companyId: string;
    public partnerId: string;
    isClicked = false;
    selectedWarehouse: string;
    selectedProduct: string;

    dataSource = new MatTableDataSource(ELEMENT_DATA);
    warehouses: Warehouse[] = [];
    products: Product[] = [];
    displayedColumns = [];
    array = [];

    constructor(private route: ActivatedRoute, public utilsService: UtilsService, public location: Location) {

        this.companyId = this.route.snapshot.paramMap.get('companyId');
        this.partnerId = this.route.snapshot.paramMap.get('partnerId');

    }

    ngOnInit() {
        this.displayedColumns = ['number', 'warehouse', 'product', 'actions'];
        this.getWarehouseByPartner();
        this.getProductByPartner();
    }

    // picked warehouse id.
    pickedWarehouse(i) {
        this.warehouseId = this.warehouses[i]['_id'];
        this.selectedWarehouse = this.warehouses[i]['title'];
    }

    // picked product id.
    pickedProduct(i) {
        this.productId = this.products[i]['serial_product'];
        this.selectedProduct = this.products[i]['title'];
        // if (this.array[i].serial_product !== this.productId) {
        this.array.push({
            'company': this.companyId,
            'warehouse': this.warehouseId,
            'partner': {
                'import': this.partnerId
            },
            'serial_product': this.productId
        });
        // } else {
        //   alert('duplicated!');
        // }

    }

    // add selection row.
    addRow() {
        if (this.array.length > 0) {
            this.isClicked = false;
            ELEMENT_DATA.push({number: null, warehouse: null, product: null});
            this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        } else {
            alert('Please select warehouse and product.');
        }
    }


    removeRow(i: number) {
        if (ELEMENT_DATA.splice(i, 1)) {
            this.array.splice(i, 1);
            this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        }
        console.log(this.array);
    }

    getWarehouseByPartner() {
        const _this = this;
        this.utilsService.reqGET('/warehouses', _this.partnerId, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
                const whList = resp.data.filter(w => w);
                _this.warehouses = whList;
            }
        });
    }

    getProductByPartner() {
        const _this = this;
        console.log(this.array.map(p => p.serial_product));
        this.utilsService.reqGET('/management/product/serials/' + _this.partnerId, null, resp => {
            if (resp && resp.code === 20000) {
                const productList = resp.data.filter(response => response);
                _this.products = productList;
            }
        });
    }

    importAll() {
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        const dataList = this.array;
        for (let i = 0; i < dataList.length; i++) {
            this.utilsService.reqPOST('/management/product/store/import', dataList[i], resp => {
                if (resp && resp.code === 20000) {
                    console.log(resp);
                    // alert('import success!');
                    // this.location.back();
                    // window.location.href = 'store-product/store-product-create-table';
                }
            });
        }
    }

}
