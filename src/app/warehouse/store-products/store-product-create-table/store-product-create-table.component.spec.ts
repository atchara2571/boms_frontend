import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreProductCreateTableComponent } from './store-product-create-table.component';

describe('StoreProductCreateTableComponent', () => {
  let component: StoreProductCreateTableComponent;
  let fixture: ComponentFixture<StoreProductCreateTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreProductCreateTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreProductCreateTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
