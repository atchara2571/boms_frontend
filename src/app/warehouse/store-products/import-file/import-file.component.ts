import {Component, OnInit} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {Partner} from '../../../models/Partner.model';
import {Warehouse} from '../../../models/Warehouse.model';
import {Product} from '../../../models/Product.model';
import {Company} from '../../../models/Company.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import * as XLSX from 'ts-xlsx';

@Component({
    selector: 'app-import-file',
    templateUrl: './import-file.component.html',
    styleUrls: ['./import-file.component.scss']
})

export class ImportFileComponent implements OnInit {

    companies: Company[] = [];
    partners: Partner[] = [];
    warehouses: Warehouse[] = [];
    products: Product[] = [];
    form: FormGroup;
    file: File;
    arrayBuffer: any;

    constructor(public utilsService: UtilsService, private fb: FormBuilder) {
    }

    ngOnInit() {
       this.fetchImportForm();
        this.getAllCompany();
        this.getAllWarehouse();
        this.getAllPartner();
        this.getAllProduct();
    }

    // fetch import form.
    fetchImportForm() {
        this.form = this.fb.group({
            file: ''
        });
    }

    // get company.
    getAllCompany() {
        const _this = this;
        this.utilsService.reqGET('/companys', null, resp => {
            _this.companies = resp.data;
        });
    }

    // get warehouse.
    getAllWarehouse() {
        const _this = this;
        this.utilsService.reqGET('/warehouses', null, resp => {
            _this.warehouses = resp.data;
        });
    }

    // get partner.
    getAllPartner() {
        const _this = this;
        this.utilsService.reqGET('/partners', null, resp => {
            _this.partners = resp.data;
        });
    }

    // get product.
    getAllProduct() {
        const _this = this;
        this.utilsService.reqGET('/management/product/serials', null, resp => {
            _this.products = resp.data;
        });
    }

    // get file import.
    incomingfile(event) {
        this.file = event.target.files[0];
        console.log(this.file);
    }

    // upload product import file.
    uploadFile() {
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
            this.arrayBuffer = fileReader.result;
            const data = new Uint8Array(this.arrayBuffer);
            const arr = new Array();
            for (let i = 0; i !== data.length; ++i) {
                arr[i] = String.fromCharCode(data[i]);
            }
            const bstr = arr.join('');
            const workbook = XLSX.read(bstr, {type: 'binary'});
            const first_sheet_name = workbook.SheetNames[0];
            const worksheet = workbook.Sheets[first_sheet_name];
            const f = XLSX.utils.sheet_to_json(worksheet, {raw: true});
            console.log(f);
            for (let i = 0; i < f.length; i++) {
                const companyName = this.companies.filter(t => t.title === f[i]['company']);
                if (companyName.length > 0) {
                    f[i]['company'] = companyName[0]._id;
                } else {
                    alert('No Product Found!');
                }
                const warehouseName = this.warehouses.filter(w => w.title === f[i]['warehouse']);
                if (warehouseName.length > 0) {
                    f[i]['warehouse'] = warehouseName[0]._id;
                }
                const partnerName = this.partners.filter(p => p.title === f[i]['partner']);
                if (partnerName.length > 0) {
                    f[i]['partner'] = {
                        import: partnerName[0]._id
                    };
                } else {
                    alert('No Product Found!');
                }
                const productName = this.products.filter(pd => pd.title === f[i]['product_name']);
                if (productName.length > 0) {
                    f[i]['serial_product'] = productName[0].serial_product;
                } else {
                    alert('No Product Found!');
                }
                this.utilsService.reqPOST('/management/product/store/import', f[i], resp => {
                    if (resp && resp.code === 20000) {
                    }
                });
            }
            // alert('Import Success!');
            // this.location.back();
        };
        fileReader.readAsArrayBuffer(this.file);
    }

}
