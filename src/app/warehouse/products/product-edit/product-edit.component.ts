import {Component, Injectable, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UtilsService} from '../../../helper/utils.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Product} from '../../../models/Product.model';
import {ActivatedRoute, Router} from '@angular/router';
import {Company} from '../../../models/Company.model';
import {Partner} from '../../../models/Partner.model';
import {AuthService} from '../../../helper/auth.service';
import {ProductType} from '../../../models/ProductType.model';

const backend_url = environment.apiUrl;

@Injectable()

@Component({
    selector: 'app-product-edit',
    templateUrl: './product-edit.component.html',
    styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {

    form: FormGroup;
    enable: boolean;
    public productId: string;
    public userId: string;
    public image: any;
    companyId: string;
    partnerId: string;
    types: ProductType[] = [];
    companies: Company[] = [];
    partners: Partner[] = [];
    // backend_url = 'http://127.0.0.1:8080/images/products/';
    defaultImage = 'assets/images/product_default.jpg';

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private fb: FormBuilder,
                private http: HttpClient,
                private router: Router,
                private route: ActivatedRoute) {

        this.productId = this.route.snapshot.paramMap.get('productId');
        this.userId = this.authService.getUserId();

    }

    ngOnInit() {
        this.fetchProductForm();
        this.getProductInfo();
        this.getCompanyByUserId();
        this.getProductType();
    }

    // fetch product form.
    fetchProductForm() {
        this.form = this.fb.group({
            serial_product: [''],
            company: [''],
            partner: [''],
            title: [''],
            description: [''],
            type: [''],
            image: [''],
            enable: ['']
        });
    }

    // get product info.
    getProductInfo() {
        this.utilsService.reqGET('/management/product/serial', this.productId, resp => {
            if (resp && resp['code'] === 20000) {
                this.image = resp['data'].image;
                this.companyId = resp['data'].company;
                this.form.setValue({
                    serial_product: resp['data'].serial_product,
                    company: resp['data'].company,
                    partner: resp['data'].support[0].partners._id,
                    title: resp['data'].title,
                    description: resp['data'].description,
                    type: resp['data'].type._id,
                    image: this.image,
                    enable: resp['data'].enable
                });
                this.getPartnerByCompany();
                if (this.enable = true) {
                    return this.form.value.enable = '1';
                } else {
                    return false;
                }
            }
        });
    }

    // get product type.
    getProductType() {
        const _this = this;
        this.utilsService.reqGET('/management/product/types', null, function (resp) {
            const type = resp.data.filter(t => t);
            _this.types = type;
        });
    }

    // get company sort by user.
    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const comList = resp.data.filter(c => c);
                _this.companies = comList;
            }
        });
    }

    // get partner sort by company.
    getPartnerByCompany() {
        const _this = this;
        this.utilsService.reqGET('/partner/company/' + _this.companyId, null, resp => {
            if (resp && resp.code === 20000) {
                // const partnerList = resp.data.filter(idCom => idCom.company === _this.companyId);
                const data = resp.data.filter(p => p);
                _this.partners = data;
            }
        });
    }

    pickedCompany() {
        this.companyId = this.form.value.company;
        this.getPartnerByCompany();
    }

    // get image file.
    onImagePicked(event: Event) {
        const file = (event.target as HTMLInputElement).files[0];
        this.form.patchValue({image: file});
        this.form.get('image').updateValueAndValidity();
        const reader = new FileReader();
        reader.onload = () => {
            this.image = reader.result;
        };
        reader.readAsDataURL(file);
    }

    // update product info.
    updateProduct(image: File) {
        image = this.form.value.image;
        const productData = new FormData();
        productData.append('company', this.form.value.company),
            productData.append('title', this.form.value.title),
            productData.append('description', this.form.value.description),
            productData.append('serial_product', this.form.value.serial_product),
            productData.append('partners', this.form.value.partner),
            productData.append('import_value', this.form.value.import_value),
            productData.append('daily_freight', this.form.value.daily_freight),
            productData.append('export_value', this.form.value.export_value),
            productData.append('enable', this.form.value.enable),
            productData.append('type', this.form.value.type),
            productData.append('image', image, image.name);
        this.utilsService.reqPUT('/management/product/serial/' + this.productId, productData, resp => {
            if (resp && resp.code === 20000) {
                this.router.navigate(['product/product-list']);
            }
        });
    }

}
