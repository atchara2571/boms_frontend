import {Component, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {ActivatedRoute} from '@angular/router';
import {Product} from '../../../models/Product.model';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {Partner} from '../../../models/Partner.model';
import {AddPartnerDialogComponent} from '../../add-partner-dialog/add-partner-dialog.component';

@Component({
    selector: 'app-product-by-partner',
    templateUrl: './product-by-partner.component.html',
    styleUrls: ['./product-by-partner.component.scss']
})
export class ProductByPartnerComponent implements OnInit {

    products: Product[] = [];
    partners: Partner[] = [];
    displayedColumns = [];
    partnerId: string;
    productId: string;
    dataSource;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private route: ActivatedRoute, public utilsService: UtilsService, private dialog: MatDialog) {

        this.partnerId = this.route.snapshot.paramMap.get('partnerId');
        this.getProductByPartner();

    }

    ngOnInit() {
        this.displayedColumns = ['number', 'partner_name', 'daily_freight', 'import_value', 'export_value'];
    }

    // get product list sort by partner.
    getProductByPartner() {
        const _this = this;
        this.utilsService.reqGET('/management/product/serials/' + _this.partnerId, null, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
                _this.productId = resp.data[0]._id;
                const partnerList = resp.data[0].support.filter(p => p.partners.title);
                _this.partners = partnerList;
                _this.dataSource = new MatTableDataSource(_this.partners);
                _this.dataSource.sort = _this.sort;
            }
        });
    }

    // add partner dialog.
    openDialog(): void {
        const dialogRef = this.dialog.open(AddPartnerDialogComponent, {
            disableClose: true,
            width: '250px',
            data: {pId: this.productId}
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

}
