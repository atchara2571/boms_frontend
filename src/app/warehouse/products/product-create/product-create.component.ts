import {Component, OnInit} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Company} from '../../../models/Company.model';
import {Partner} from '../../../models/Partner.model';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {AuthService} from '../../../helper/auth.service';
import {HttpClient} from '@angular/common/http';
import {Product} from '../../../models/Product.model';
import {environment} from '../../../../environments/environment';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ProductType} from '../../../models/ProductType.model';

export interface Type {
    title: string;
}

const backend_url = environment.apiUrl;

@Component({
    selector: 'app-product-create',
    templateUrl: './product-create.component.html',
    styleUrls: ['./product-create.component.scss']
})
export class ProductCreateComponent implements OnInit {

    form: FormGroup;
    types: Type[] = [];
    companies: Company[] = [];
    partners: Partner[] = [];
    companyId: string;
    productTypeId: string;
    partnerId: string;
    imagePreview: string;
    private userId: string;

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private fb: FormBuilder,
                private router: Router,
                private location: Location,
                private http: HttpClient) {

        this.userId = this.authService.getUserId();

    }

    ngOnInit() {
        this.fetchProductForm();
        this.getCompanyByUserId();
        this.getPartnerByCompany();
        this.getProductType();
    }

    // fetch product form.
    fetchProductForm() {
        this.form = this.fb.group({
            company: [''],
            title: [''],
            description: [''],
            serial_product: [''],
            partner: [''],
            import_value: [''],
            daily_freight: [''],
            export_value: [''],
            enable: [null],
            type: [''],
            image: [''],
        });
    }

    // companies selection drop down list.
    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const comList = resp.data.filter(c => c);
                _this.companies = comList;
            }
        });
    }


    // get partner sort by company.
    getPartnerByCompany() {
        const _this = this;
        this.utilsService.reqGET('/partner/company/' + this.companyId, null, resp => {
            if (resp && resp.code === 20000) {
                const partnerList = resp.data.filter(idCom => idCom.company === _this.companyId);
                _this.partners = partnerList;
            }
        });
    }

    // get product type.
    getProductType() {
        const _this = this;
        this.utilsService.reqGET('/management/product/types', null, (resp) => {
            if (resp && resp.code === 20000) {
                const dataList = resp.data;
                const user = dataList.filter(r => {
                    return r.create.by === _this.userId;
                });
                _this.types = user;
            }
        });
    }

    pickedCompany(i) {
        this.companyId = this.companies[i]['_id'];
        this.getPartnerByCompany();
    }

    // get image file.
    onImagePicked(event: Event) {
        const file = (event.target as HTMLInputElement).files[0];
        this.form.patchValue({image: file});
        this.form.get('image').updateValueAndValidity();
        const reader = new FileReader();
        reader.onload = () => {
            this.imagePreview = reader.result;
        };
        console.log(file);
        reader.readAsDataURL(file);
    }

    // create new product.
    createProduct(image: File) {
        image = this.form.value.image;
        const productData = new FormData();
        productData.append('company', this.companyId),
            productData.append('title', this.form.value.title),
            productData.append('description', this.form.value.description),
            productData.append('serial_product', this.form.value.serial_product),
            productData.append('partners', this.form.value.partner),
            productData.append('import_value', this.form.value.import_value),
            productData.append('daily_freight', this.form.value.daily_freight),
            productData.append('export_value', this.form.value.export_value),
            productData.append('enable', this.form.value.enable),
            productData.append('type', this.form.value.type),
            productData.append('image', image, image.name);
        if (this.form.valid) {
            this.utilsService.reqPOST('/management/product/serial', productData, (resp) => {
                if (resp && resp.code === 20000) {
                    if (resp && resp.code === 20000) {
                        this.router.navigate(['product/product-list']);
                        // this.utilsService.locationBack();
                    }
                }
            });
        }
    }

    // back to previous page.
    back() {
        this.utilsService.locationBack();
    }

}
