import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {UtilsService} from '../../../../helper/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../../helper/auth.service';
import {Company} from '../../../../models/Company.model';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

// parent

@Component({
    selector: 'app-product-type-edit',
    templateUrl: './product-type-edit.component.html',
    styleUrls: ['./product-type-edit.component.scss']
})

export class ProductTypeEditComponent implements OnInit {
    form: FormGroup;
    companies: Company[] = [];
    private userId: string;
    public companyId: string;
    public companyName: string;
    public productTypeId: string;
    public enable: boolean;
    public testText = new BehaviorSubject('text from parent');

    public testId = new BehaviorSubject<any>(this.test());

    // public testId = '1234';

    constructor(private fb: FormBuilder,
                private http: HttpClient,
                public utilsService: UtilsService,
                public authService: AuthService,
                private router: Router,
                private route: ActivatedRoute) {

        this.userId = this.authService.getUserId();
        this.productTypeId = this.route.snapshot.paramMap.get('productTypeId');

    }

    public test() {
        this.testText.subscribe({
            next: (v) =>
                console.log(v)
        });
        console.log('set' + this.testText);
        this.testText.next('next text');
    }

    ngOnInit() {
        this.fetchProductTypeForm();
        this.getProductTypeInfo();
    }

    fetchProductTypeForm() {
        this.form = this.fb.group({
            title: [''],
            // company: [''],
            description: [''],
            enable: [null]
        });
    }

    getProductTypeInfo() {
        // const _this = this;
        this.utilsService.reqGET('/management/product/types', this.productTypeId, resp => {
            if (resp && resp.code === 20000) {
                const companyId = resp.data.company;
                this.companyId = companyId;
                this.form.setValue({
                    title: resp.data.title,
                    // company: '',
                    description: resp.data.description,
                    enable: resp.data.enable
                });
            }
        });
    }

    // update product type.
    updateProductType() {
        const _this = this;
        const params = {
            title: this.form.value.title,
            company: this.companyId,
            description: this.form.value.description,
            enable: this.form.value.enable
        };
        this.utilsService.reqPUT('/management/product/types/' + _this.productTypeId, params, resp => {
            if (resp && resp.code === 20000) {
                return;
            }
        });
        this.router.navigate(['/product/product-type-list']);
    }

}
