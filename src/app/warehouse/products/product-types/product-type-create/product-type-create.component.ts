import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UtilsService} from '../../../../helper/utils.service';
import {Router} from '@angular/router';
import {AuthService} from '../../../../helper/auth.service';
import {Company} from '../../../../models/Company.model';
import {ProductTypeEditComponent} from '../product-type-edit/product-type-edit.component';
import {ProductCreateComponent} from '../../product-create/product-create.component';

// child

@Component({
    selector: 'app-product-type',
    templateUrl: './product-type-create.component.html',
    styleUrls: ['./product-type-create.component.scss'],
    providers: [
        ProductTypeEditComponent,
        ProductCreateComponent
    ]
})
export class ProductTypeCreateComponent implements OnInit, AfterViewInit {

    form: FormGroup;
    companies: Company[] = [];
    companyId: string;
    userId = this.authService.getUserId();

    constructor(private fb: FormBuilder,
                public utilsService: UtilsService,
                public authService: AuthService,
                private router: Router,
                private comp2: ProductTypeEditComponent,
                public pc: ProductCreateComponent) {
    }

    testText: any;
    message: string;
    // companies: any;

    ngOnInit() {
        this.fetchProductTypeForm();
        this.getCompanyByUserId();

        // this.pc.getCompanyByUserId();
        // this.companies = this.pc.companies.value;
        // this.pc.companies.value.push(this.companies);

        // console.log(this.companies);

        this.testText = this.comp2.testText;

        this.utilsService.test.subscribe(message => this.message = message);
        console.log(this.message);


    }

    ngAfterViewInit() {

    }

    public call(): void {
        this.comp2.test();
    }

    // fetch product type form.
    fetchProductTypeForm() {
        this.form = this.fb.group({
            title: [''],
            company: [''],
            description: [''],
            enable: [null]
        });
    }

    getCompanyByUserId() {
        this.utilsService.reqGET('/company', this.userId, resp => {
            if (resp && resp['code'] === 20000) {
                const comList = resp['data'].filter(c => c);
                this.companies = comList;
            }
        });
    }


    // create new product type.
    createProductType() {
        const params = {
            'title': this.form.value.title,
            'company': this.form.value.company,
            'description': this.form.value.description,
            'enable': this.form.value.enable
        };
        if (this.form.valid) {
            this.utilsService.reqPOST('/management/product/types', params, (resp) => {

                if (resp && resp.code === 20000) {
                    this.router.navigate(['/product/product-type-list']);
                }
            });
        }

    }

    // back to previous page.
    back() {
        this.utilsService.locationBack();
    }

}


