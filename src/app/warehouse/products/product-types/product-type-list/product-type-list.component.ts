import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {UtilsService} from '../../../../helper/utils.service';
import {Router} from '@angular/router';
import {AuthService} from '../../../../helper/auth.service';
import {ProductTypeEditComponent} from '../product-type-edit/product-type-edit.component';

export interface ProductType {
    title: string;
    description: string;
    enable: string;
}

@Component({
    selector: 'app-product-type-list',
    templateUrl: './product-type-list.component.html',
    styleUrls: ['./product-type-list.component.scss'],
    providers: [ProductTypeEditComponent]
})
export class ProductTypeListComponent implements OnInit {

    dataSource;
    displayedColumns = [];
    productTypes: ProductType[] = [];
    @ViewChild(MatSort) sort: MatSort;
    public userId: string;
    private productTypeId: string;
    private status: string;

    testId: any;
    testText: any;

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private router: Router,
                public p: ProductTypeEditComponent) {

        this.userId = this.authService.getUserId();
        this.getProductType();

        // this.testId = this.utilsService.testId;
        // this.testId = this.p.testId;
        this.testText = this.p.testText;
        // this.p.test();
        console.log(this.testText);

    }

    ngOnInit() {
        this.getProductType();
        this.displayedColumns = ['number', 'productTypeName', 'status', 'actions'];
    }

    // get product type.
    getProductType() {
        // const _this = this;
        this.utilsService.reqGET('/management/product/types', null, (resp) => {
            if (resp && resp.code === 20000) {
                const productTypeList = resp.data
                    .filter(data => data.create.by === this.userId);
                this.productTypes = productTypeList;
                this.dataSource = new MatTableDataSource(this.productTypes);
                this.dataSource.sort = this.sort;
            }
        });
    }

    changeStatus(i, event) {
        this.productTypeId = this.dataSource.data[i]._id;
        this.status = event.checked;
        this.updateProductType();
    }

    // update product type.
    editProductType(i: number) {
        const productTypeId = this.dataSource.data[i]._id;
        this.router.navigate(['/product/product-type-edit', productTypeId]);
    }

    // update product type.
    updateProductType() {
        const params = {
            enable: this.status
        };
        this.utilsService.reqPUT('/management/product/types/' + this.productTypeId, params, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
                this.ngOnInit();
            }
        });
    }

}
