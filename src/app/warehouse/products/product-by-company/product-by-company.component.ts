import {Component, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {MatSort, MatTableDataSource} from '@angular/material';
import {Product} from '../../../models/Product.model';

@Component({
  selector: 'app-product-by-company',
  templateUrl: './product-by-company.component.html',
  styleUrls: ['./product-by-company.component.scss']
})
export class ProductByCompanyComponent implements OnInit {

  dataSource;
  displayedColumns = [];
  products: Product[] = [];
  @ViewChild(MatSort) sort: MatSort;

  constructor(public utilsService: UtilsService) {

      this.getProductByCompany();

  }

  ngOnInit() {
    this.displayedColumns = ['number', 'productName', 'actions'];
  }


  // get product list sort by company.
  getProductByCompany() {
    const _this = this;
    const companyId = localStorage.getItem('companyId');
    this.utilsService.reqGET('/management/product/serials', null, resp => {
      if (resp && resp.code === 20000) {
        const productList = resp.data.filter(id => id.company === companyId);
        _this.products = productList;
        _this.dataSource = new MatTableDataSource(_this.products);
        _this.dataSource.sort = _this.sort;
      }
    });
  }

}
