import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatInputModule} from '@angular/material';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {ProductRouting} from './product.routing';
import {ProductListComponent} from './product-list/product-list.component';
import {AngularMaterialModule} from '../../angular-material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProductCreateComponent} from './product-create/product-create.component';
import {ProductTypeCreateComponent} from './product-types/product-type-create/product-type-create.component';
import {ProductEditComponent} from './product-edit/product-edit.component';
import {ProductTypeListComponent} from './product-types/product-type-list/product-type-list.component';
import {ProductTypeEditComponent} from './product-types/product-type-edit/product-type-edit.component';
import {ProductByCompanyComponent} from './product-by-company/product-by-company.component';
import {AddPartnerDialogComponent} from '../add-partner-dialog/add-partner-dialog.component';
import { ProductByPartnerComponent } from './product-by-partner/product-by-partner.component';
import { TestComponent } from './test/test.component';

@NgModule({
    imports: [
        CommonModule,
        ProductRouting,
        MatInputModule,
        NgxDatatableModule,
        AngularMaterialModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        ProductListComponent,
        ProductCreateComponent,
        ProductTypeCreateComponent,
        ProductEditComponent,
        ProductTypeListComponent,
        ProductTypeEditComponent,
        ProductByCompanyComponent,
        AddPartnerDialogComponent,
        ProductByPartnerComponent,
        TestComponent
    ],
    entryComponents: [
        AddPartnerDialogComponent
    ]
})
export class ProductModule {
}
