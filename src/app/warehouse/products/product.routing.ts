import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductCreateComponent} from './product-create/product-create.component';
import {ProductEditComponent} from './product-edit/product-edit.component';
import {ProductTypeCreateComponent} from './product-types/product-type-create/product-type-create.component';
import {ProductTypeListComponent} from './product-types/product-type-list/product-type-list.component';
import {ProductTypeEditComponent} from './product-types/product-type-edit/product-type-edit.component';
import {ProductByCompanyComponent} from './product-by-company/product-by-company.component';
import {ProductByPartnerComponent} from './product-by-partner/product-by-partner.component';
import {TestComponent} from './test/test.component';

const routes: Routes = [
    {
        path: '',
        component: ProductListComponent,
        data: {
            heading: 'Product'
        }
    },
    {
        path: 'product-list',
        component: ProductListComponent,
        data: {
            heading: 'Product'
        }
    },
    {
        path: 'product-create',
        component: ProductCreateComponent,
        data: {
            heading: 'Create New Product'
        }
    },
    {
        path: 'product-edit/:productId',
        component: ProductEditComponent,
        data: {
            heading: 'Edit Product'
        }
    },
    {
        path: 'product-type-create',
        component: ProductTypeCreateComponent,
        data: {
            heading: 'Create Product Type'
        }
    },
    {
        path: 'product-type-list',
        component: ProductTypeListComponent,
        data: {
            heading: 'Product Type List'
        }
    },
    {
        path: 'product-type-edit/:productTypeId',
        component: ProductTypeEditComponent,
        data: {
            heading: 'Edit Product Type'
        }
    },
    {
        path: 'product-by-company',
        component: ProductByCompanyComponent,
        data: {
            heading: 'Product by Company'
        }
    },
    {
        path: 'product-by-partner/:partnerId',
        component: ProductByPartnerComponent,
        data: {
            heading: 'Product by Partner'
        }
    },
    {
        path: 'test',
        component: TestComponent,
        data: {
            heading: 'test'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ProductRouting {
}
