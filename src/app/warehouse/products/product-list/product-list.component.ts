import {Component, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {AuthService} from '../../../helper/auth.service';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';
import {Product} from '../../../models/Product.model';
import {AddPartnerDialogComponent} from '../../add-partner-dialog/add-partner-dialog.component';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

const backend_url = environment.apiUrl;

@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

    backend_url = 'http://127.0.0.1:8080/images/products/';
    defaultImage = 'assets/images/product_default.jpg';
    nullImage = 'http://127.0.0.1:8080/images/products/';
    dataSource;
    displayedColumns = [];
    products: Product[] = [];
    @ViewChild(MatSort) sort: MatSort;
    public userId: string;
    public productId: string;
    public partnerId: string;
    public image: string;
    public status: string;

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private router: Router,
                private dialog: MatDialog,
                private http: HttpClient) {

        this.userId = this.authService.getUserId();
        this.getProduct();

    }

    ngOnInit() {
        this.getProduct();
        this.displayedColumns = ['number', 'image', 'serial_number', 'productName', 'status', 'actions'];
    }

    // get all product list.
    getProduct() {
        const _this = this;
        this.utilsService.reqGET('/management/product/serials', null, (resp) => {
            if (resp && resp.code === 20000) {
                const productList = resp.data.filter(data => data.create.by === _this.userId);
                _this.image = resp.data.filter(data => data.image);
                if (_this.image !== '') {
                    _this.image = resp.data.filter(data => data.image = _this.backend_url + data.image);
                }
                _this.products = productList;
                _this.dataSource = new MatTableDataSource(_this.products);
                _this.dataSource.sort = _this.sort;
                console.log(_this.dataSource);
            }
        });
    }

    // icon event.
    onSelect(i: number, event) {
        this.productId = this.dataSource.data[i]._id;
        this.partnerId = this.dataSource.data[i].support[0]._id;
        if (event.target.id === 'editProduct') {
            this.router.navigate(['/product/product-edit', this.productId]);
        } else if (event.target.id === 'addPartner') {
            this.openDialog();
        }
    }

    // add partner dialog.
    public openDialog(): void {
        const dialogRef = this.dialog.open(AddPartnerDialogComponent, {
            disableClose: true,
            width: '250px',
            data: {pId: this.productId, pnId: this.partnerId}
        });
        dialogRef.afterClosed().subscribe(result => {
        });
    }

    changeStatus(i, event) {
        this.productId = this.dataSource.data[i]._id;
        this.status = event.checked;
        this.updateProduct();
    }

    // create new product.
   updateProduct() {
        const productData = new FormData();
        productData.append('enable', this.status);
        this.http.put<Product>(backend_url + '/management/product/serial/' + this.productId, productData)
            .subscribe(resp => {
                if (resp && resp['code'] === 20000) {
                    console.log(resp);
                  this.ngOnInit();
                }
            });
    }


    // deleteWarehouseByCompany(i: number) {
    //  const _this = this;
    //   const params = {
    //     'body': {
    //       'support': {
    //         '_id': {
    //           '$in': [this.supportId]
    //         }
    //       }
    //     }
    //   };
    //   this.utilsService.reqDELETE('/management/product/serial/' + _this.productId + '/' + 'support', params, resp => {
    //     if (resp && resp.code === 20000) {
    //       console.log(resp);
    //       window.location.href = 'warehouse/warehouse-by-company';
    //     }
    //   });
    // }

}
