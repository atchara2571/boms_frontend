import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {MatTableDataSource, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../helper/auth.service';
import {Partner} from '../../../models/Partner.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {DialogData} from '../../../models/Dialog.model';
import {Warehouse} from '../../../models/Warehouse.model';

@Component({
    selector: 'app-warehouse-list',
    templateUrl: './warehouse-list.component.html',
    styleUrls: ['../warehouse.scss']
})
export class WarehouseListComponent implements OnInit {

    renderedData: any;
    name: string;
    dataSource;
    displayedColumns = [];
    warehouses: Warehouse[] = [];
    @ViewChild(MatSort) sort: MatSort;
    private userId: string;
    private warehouseId: string;
    private companyId: string;
    public status: any;
    public supportId: string;

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private route: ActivatedRoute,
                private router: Router,
                public dialog: MatDialog) {

        this.userId = this.authService.getUserId();
        this.getWarehouse();

    }

    ngOnInit() {
        this.displayedColumns = ['number', 'warehouseName', 'status', 'actions'];
        this.userId = this.authService.getUserId();
        this.getWarehouse();
    }


    // get all warehouse.
    getWarehouse() {
        const _this = this;
        this.utilsService.reqGET('/warehouses', null, resp => {
            if (resp && resp.code === 20000) {
                const data = resp.data.filter(uId => uId.create.by === _this.userId);
                const s = resp.data.filter(uId => uId.support.find(l => l.length !== 0) && data);
                _this.warehouses = s;
                _this.dataSource = new MatTableDataSource(_this.warehouses);
                _this.dataSource.connect().subscribe(d => _this.renderedData = d);
                _this.dataSource.sort = _this.sort;
            }
        });
    }

    changeStatus(i, event) {
        this.warehouseId = this.dataSource.data[i]._id;
        this.status = event.checked;
        this.updateWarehouse();
    }

    updateWarehouse() {
        const params = {
            enable: this.status
        };
        this.utilsService.reqPUT('/warehouse/' + this.warehouseId, params, resp => {
            if (resp && resp.code === 20000) {
                this.ngOnInit();
            }
        });
    }

    // icon event.
    onSelect(i: number, event) {
        this.companyId = this.dataSource.data[i].support[0].company;
        this.warehouseId = this.dataSource.data[i]._id;
        const warehouseName = this.dataSource.data[i].title;
        this.supportId = this.dataSource.data[i].support[0]._id;
        this.status = this.dataSource.data[i].enable;
        if (event.target.id === 'editWarehouseInfo') {
            if (this.status === true) {
                this.router.navigate(['/warehouse/warehouse-edit', this.warehouseId, warehouseName]);
            } else {
                alert('This warehouse is not available');
            }
        } else if (event.target.id === 'importProduct') {
            this.openDialog();
        } else if (event.target.id === 'eachStore') {
            if (this.status === true) {
                this.router.navigate(['/store-product/store-product-by-company-warehouse', this.companyId, this.warehouseId]);
            } else {
                alert('This warehouse is not available');
            }
        } else if (event.target.id === 'deleteWarehouseIcon') {
            this.openConfirmDialog();
        }

        // else if (event.target.id === 'allStore') {
        //     this.router.navigate(['/store-product/store-product-by-company-warehouse-all', companyId]);
        // }
    }

    // go to create new warehouse page.
    addWarehouse() {
        this.router.navigate(['/warehouse/warehouse-create']);
    }

    openConfirmDialog(): void {
        const dialogRef = this.dialog.open(ConfirmDialog2Component, {
            disableClose: true,
            width: '250px',
            data: {supportId: this.supportId, wId: this.warehouseId}
        });
        dialogRef.afterClosed().subscribe(result => {
            this.ngOnInit();
        });
    }

    // add partner dialog.
    public openDialog(): void {
        const dialogRef = this.dialog.open(PartnerDialog2Component, {
            disableClose: true,
            width: '250px',
            data: {cId: this.companyId, status: this.status, wId: this.warehouseId}
        });
        dialogRef.afterClosed().subscribe(result => {
        });
    }

}


// add partner dialog.
@Component({
    selector: 'app-partner-dialog2',
    template: '<h1 mat-dialog-title>Select Partner</h1>\n' +
        '<div mat-dialog-content style="height: 150px;">\n' +
        '<p>Please select partner.</p>\n' +
        ' <form [formGroup]="form">\n' +
        '    <div class="formGroup">\n' +
        '  <mat-form-field>\n' +
        '        <mat-select placeholder="Select Partner" formControlName="partner">\n' +
        '          <mat-option *ngFor="let partner of partners;" [value]="partner._id">\n' +
        '            {{partner.title}}\n' +
        '          </mat-option>\n' +
        '        </mat-select>\n' +
        '      </mat-form-field>\n' +
        '</div>\n' +
        '<div mat-dialog-actions>\n' +
        '  <button mat-button (click)="onNoClick()">Cancel</button>\n' +
        '  <button mat-button cdkFocusInitial (click)="importProduct()">Ok</button>\n' +
        '</div>\n' +
        '</form>\n' +
        '</div>'
})
export class PartnerDialog2Component implements OnInit {

    partners: Partner[] = [];
    form: FormGroup;
    partnerId: string;

    constructor(
        public dialogRef: MatDialogRef<PartnerDialog2Component>,
        private router: Router,
        public utilsService: UtilsService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private fb: FormBuilder) {
    }

    ngOnInit() {
        this.getPartnerByCompany();
        this.fetchPartnerDialogForm();
    }

    // fetch dialog form.
    fetchPartnerDialogForm() {
        this.form = this.fb.group({
            partner: ''
        });
    }

    // get partner sort by company.
    getPartnerByCompany() {
        const _this = this;
        this.utilsService.reqGET('/partner/company/' + _this.data.cId, null, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
                // const partnerList = resp.data.filter(idCom => idCom.company === _this.data.cId);
                const partnerList = resp.data.filter(p => p);
                _this.partners = partnerList;
            }
        });
    }

    // go to product import page.
    importProduct() {
        if (this.data.status === true) {
            this.router.navigate(['/store-product/store-product-create', this.data.cId, this.form.value.partner, this.data.wId]);
            this.dialogRef.close();
        } else {
            alert('This warehouse is not available');
            this.dialogRef.close();
        }
    }

    // leave dialog.
    onNoClick(): void {
        this.dialogRef.close();
    }
}


@Component({
    providers: [WarehouseListComponent],
    selector: 'app-confirm-dialog2',
    template: '<h1 mat-dialog-title>Are you sure?</h1>\n' +
        '<div mat-dialog-actions>\n' +
        '  <button mat-button (click)="onNoClick()">No</button>\n' +
        '  <button mat-button cdkFocusInitial (click)="deleteWarehouseByCompany()">Yes</button>\n' +
        '</div>',
})
export class ConfirmDialog2Component implements AfterViewInit {


    constructor(
        public dialogRef: MatDialogRef<ConfirmDialog2Component>,
        private router: Router,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        public utilsService: UtilsService,
        private  warehouseList: WarehouseListComponent) {
    }

    ngAfterViewInit() {
        console.log('Values on ngAfterViewInit():');
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    // delete selected warehouse.
    deleteWarehouseByCompany() {
        const params = {
            'body': {
                'support': {
                    '_id': {
                        '$in': [this.data.supportId]
                    }
                }
            }
        };
        this.utilsService.reqDELETE('/warehouse/' + this.data.wId + '/' + 'support/company', params, resp => {
            if (resp && resp.code === 20000) {
                // window.location.href = 'warehouse/warehouse-list';
                // this.warehouseList.ngOnInit();
                this.onNoClick();
                alert('Delete Success!');
            }
        });
    }
}
