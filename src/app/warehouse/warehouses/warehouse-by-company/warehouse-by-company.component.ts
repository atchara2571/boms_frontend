import {Component, OnInit, ViewChild, Pipe, PipeTransform} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {UtilsService} from '../../../helper/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Company} from '../../../models/Company.model';
import {AuthService} from '../../../helper/auth.service';
import {Warehouse} from '../../../models/Warehouse.model';

@Component({
    selector: 'app-warehouse-by-company',
    templateUrl: './warehouse-by-company.component.html',
    styleUrls: ['../warehouse.scss']
})
export class WarehouseByCompanyComponent implements OnInit {

    whList = [];
    displayedColumns = [];
    companies: Company[] = [];
    warehouses: Warehouse[] = [];
    dataSource;
    public companyId: string;
    public companyName: string;
    public userId: string;
    public productId: string;
    public warehouseId: string;
    public partnerId: string;
    @ViewChild(MatSort) sort: MatSort;


    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private router: Router,
                private route: ActivatedRoute) {

        this.userId = this.authService.getUserId();
        this.companyId = this.route.snapshot.paramMap.get('companyId');
        // this.getCompanyByUserId();
        this.getWarehouseByCompany();

    }

    ngOnInit() {
        this.displayedColumns = ['no', 'serial_product', 'product_title', 'product_type', 'product_volume', 'remain', 'actions'];

    }

    // // get company sort by user id.
    // getCompanyByUserId() {
    //     const _this = this;
    //     this.utilsService.reqGET('/company', _this.userId, resp => {
    //         if (resp && resp.code === 20000) {
    //             const companyId = resp.data.map(c => c._id);
    //             _this.companyId = companyId;
    //         }
    //         _this.getWarehouseByCompany();
    //         // _this.getWarehouseByCompany();
    //         // _this.getStoreProductByCompany();
    //     });
    // }

    // get warehouse sort by company.
    getWarehouseByCompany() {
        const _this = this;
        this.utilsService.reqGET('/warehouse/company/' + _this.companyId, null, resp => {
            const data = resp.data.filter(r => r);
            _this.loopGetWareHouse([], data, 0, function (resp2) {
                _this.whList = resp2;
                console.log(_this.whList);
            });
        });
    }

    // get all warehouse sort by company, show each table per warehouse.
    loopGetWareHouse(respList, whList, index, callback) {
        const _this = this;
        if (whList.length > 0 && !whList[index]) {
            if (typeof callback === 'function') {
                callback(respList);
            }
        } else {
            this.utilsService.reqGET('/management/product/count/' + _this.companyId + '/' + whList[index]._id, null, resp => {
                if (resp) {
                    const data = resp.data.filter(r => r);
                    const storeProducts = data;
                    const dataSource = new MatTableDataSource(storeProducts);
                    dataSource.sort = _this.sort;
                    respList.push(dataSource);
                    index++;
                    _this.loopGetWareHouse(respList, whList, index, callback);
                } else {
                    return false;
                }
            });
        }
    }

    // icon event
    onSelect(i: number) {
        const productId = this.whList[i].data[i].serial_product._id;
        const warehouseId = this.whList[i].data[i].warehouse._id;
        const companyId = this.whList[i].data[i].company._id;
        this.productId = productId;
        this.warehouseId = warehouseId;
        this.companyId = companyId;
        // this.getProductById();
    }

    exportProduct() {
        const _this = this;
        // const params = {
        //   'company': this.companyId,
        //   'warehouse': this.warehouseId,
        //   'partner': {
        //     'export': this.partnerId
        //   },
        //   'tag_code': this.tag_code
        // }
        this.utilsService.reqPUT('/management/product/store/export/' + _this.warehouseId, null, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
            }
        });
    }

    // getWarehouseByCompany() {
    //   const _this = this;
    //   this.utilsService.reqGET('/warehouse/company/' + _this.companyId, null, resp => {
    //     if (resp && resp.code === 20000) {
    //       console.log(resp);
    //       const data = resp.data.filter(warehouse => {
    //         return warehouse;
    //       });
    //       _this.warehouses = data;
    //       _this.dataSource = new MatTableDataSource(_this.warehouses);
    //       _this.dataSource.sort = _this.sort;
    //     }
    //   });
    // }

    // getStoreProductByCompany() {
    //   const _this = this;
    //   this.utilsService.reqGET('/management/product/stores/' + _this.companyId, null, resp => {
    //     if (resp) {
    //       console.log(resp);
    //       const data = resp.data.filter(r => r);
    //       _this.loopGetWareHouse([], data, 0, function (resp2) {
    //         console.log(resp2);
    //         _this.whList = resp2;
    //       });
    //     }
    //   });
    // }

    // getProductById() {
    //   const _this = this;
    //   this.utilsService.reqGET('/management/product/serial', _this.productId, resp => {
    //     if (resp && resp.code === 20000) {
    //       console.log(resp);
    //       const partnerId = resp.data.support.map(pn => pn.partners._id);
    //       _this.partnerId = partnerId;
    //     }
    //     _this.getProductInWarehouseByCWP();
    //   });
    // }

    // getProductInWarehouseByCWP() {
    //   const _this = this;
    //   this.utilsService.reqGET('/management/product/count', _this.companyId + '/' + _this.warehouseId + '/' + _this.partnerId,
    //     resp => {
    //       if (resp) {
    //         console.log(resp);
    //       }
    //     });
    // }

    // getWarehouseById(i: number) {
    //   const _this = this;
    //   const companyId = localStorage.getItem('companyId');
    //   const id = this.dataSource.data[i]._id;
    //   this.warehouseId = id;
    //   localStorage.setItem('warehouseId', id);
    //   this.utilsService.reqGET('/warehouse/' + id, null, function (resp) {
    //     const supportData = resp.data.support.find(idCom => idCom.company === companyId);
    //     const spId = supportData._id;
    //     _this.supportId = spId;
    //     _this.deleteWarehouseByCompany(i);
    //   });
    //   // this.router.navigate(['/warehouse-list']);
    // }


    // warehouseIndex(i: number) {
    //   console.log(this.dataSource);
    //   const warehouseId = this.dataSource.data[i]._id;
    //   const warehouseName = this.dataSource.data[i].title;
    //   localStorage.setItem('warehouseId', warehouseId);
    //   localStorage.setItem('warehouseName', warehouseName);
    //   this.router.navigate(['/partner/partner-by-warehouse']);
    // }

    // deleteWarehouseByCompany(i: number) {
    // this.getWarehouseById(i);
    //   const params = {
    //     'body': {
    //       'support': {
    //         '_id': {
    //           '$in': [this.supportId]
    //         }
    //       }
    //     }
    //   };
    //   this.utilsService.reqDELETE('/warehouse/' + this.warehouseId + '/' + 'support/company', params, resp => {
    //     if (resp && resp.code === 20000) {
    //       console.log(resp);
    //       window.location.href = 'warehouse/warehouse-by-company';
    //     }
    //   });
    // }

}
