import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../../angular-material.module';
import {ConfirmDialog2Component, PartnerDialog2Component, WarehouseListComponent} from './warehouse-list/warehouse-list.component';
import {WarehouseRoutingModule} from './warehouse.routing';
import {WarehouseCreateComponent} from './warehouse-create/warehouse-create.component';
import {WarehouseEditComponent} from './warehouse-edit/warehouse-edit.component';
import {WarehouseByCompanyComponent} from './warehouse-by-company/warehouse-by-company.component';
import {WarehouseByPartnerComponent} from './warehouse-by-partner/warehouse-by-partner.component';
import {MapComponent, MapDialogComponent} from '../../map/map.component';
import {AgmCoreModule} from '@agm/core';

@NgModule({
    imports: [
        CommonModule,
        WarehouseRoutingModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
        AngularMaterialModule,
        AgmCoreModule
    ],
    declarations: [
        WarehouseListComponent,
        WarehouseByCompanyComponent,
        WarehouseCreateComponent,
        WarehouseEditComponent,
        WarehouseByPartnerComponent,
        PartnerDialog2Component,
        ConfirmDialog2Component,
        MapDialogComponent,
        MapComponent
    ],
    entryComponents: [PartnerDialog2Component, ConfirmDialog2Component, MapDialogComponent]
})

export class WarehouseModule {
}
