import {Component, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../../helper/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Warehouse} from '../../../models/Warehouse.model';
import {MatSort, MatTableDataSource} from '@angular/material';

@Component({
    selector: 'app-warehouse-by-partner',
    templateUrl: './warehouse-by-partner.component.html',
    styleUrls: ['./warehouse-by-partner.component.scss']
})
export class WarehouseByPartnerComponent implements OnInit {

    public partnerId: string;
    public partnerName: string;
    dataSource;
    warehouses: Warehouse[] = [];
    displayedColumns = [];
    @ViewChild(MatSort) sort: MatSort;

    constructor(public utilsService: UtilsService,
                private route: ActivatedRoute,
                private router: Router) {

        this.partnerId = this.route.snapshot.paramMap.get('partnerId');
        this.partnerName = this.route.snapshot.paramMap.get('partnerName');
        this.getWarehouseByPartner();

    }

    ngOnInit() {
        this.displayedColumns = ['number', 'warehouseName', 'actions'];
    }

    // get warehouse sort by partner.
    getWarehouseByPartner() {
        const _this = this;
        this.utilsService.reqGET('/warehouses', _this.partnerId, resp => {
            if (resp) {
                const dataList = resp.data.filter(warehouse => warehouse);
                const s = resp.data.filter(uId => uId.support.find(l => l.length !== 0) && dataList);
                _this.warehouses = s;
                _this.dataSource = new MatTableDataSource(_this.warehouses);
                _this.dataSource.sort = _this.sort;
            }
        });
    }

    // icon event.
    onSelect(i, event) {
        const warehouseId = this.dataSource.data[i]._id;
        const warehouseName = this.dataSource.data[i].title;
        const companyId = this.dataSource.data[i].support[0].company;
        if (event.target.id === 'editWarehouseIcon') {
            this.router.navigate(['/warehouse/warehouse-edit', warehouseId, warehouseName]);
        } else if (event.target.id === 'seeProductIcon') {
            this.router.navigate(['../../store-product/store-product-by-company-warehouse-partner', companyId, warehouseId, this.partnerId, this.partnerName]);
        } else if (event.target.id === 'addProductIcon') {
            this.router.navigate(['../../store-product/store-product-create', companyId, this.partnerId]);
        }
    }

    // go to create new warehouse page.
    addWarehouse() {
        this.router.navigate(['/warehouse/warehouse-create']);
    }

}
