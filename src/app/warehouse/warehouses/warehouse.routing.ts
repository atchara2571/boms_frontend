import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WarehouseCreateComponent} from './warehouse-create/warehouse-create.component';
import {WarehouseListComponent} from './warehouse-list/warehouse-list.component';
import {WarehouseEditComponent} from './warehouse-edit/warehouse-edit.component';
import {WarehouseByCompanyComponent} from './warehouse-by-company/warehouse-by-company.component';
import {WarehouseByPartnerComponent} from './warehouse-by-partner/warehouse-by-partner.component';

export const routes: Routes = [
  {
    path: '',
    component: WarehouseListComponent,
    data: {
      heading: 'Warehouse List'
    }
  },
  {
    path: 'warehouse-list',
    component: WarehouseListComponent,
    data: {
      heading: 'Warehouse List'
    }
  },
  {
    path: 'warehouse-by-company/:companyId',
    component: WarehouseByCompanyComponent,
    data: {
      heading: 'Warehouse By Company'
    }
  },
  {
    path: 'warehouse-create',
    component: WarehouseCreateComponent,
    data: {
      heading: 'Add New Warehouse'
    }
  },
  {
    path: 'warehouse-edit/:warehouseId/:warehouseName',
    component: WarehouseEditComponent,
    data: {
      heading: 'Edit Warehouse'
    }
  },
  {
    path: 'warehouse-by-partner/:partnerId/:partnerName',
    component: WarehouseByPartnerComponent,
    data: {
      heading: 'Warehouse by Partner'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class WarehouseRoutingModule { }
