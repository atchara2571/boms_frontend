import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {Warehouse} from '../../../models/Warehouse.model';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {UtilsService} from '../../../helper/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {error} from 'util';

const backend_url = environment.apiUrl;

@Component({
    selector: 'app-warehouse-edit',
    templateUrl: './warehouse-edit.component.html',
    styleUrls: ['./warehouse-edit.component.scss']
})
export class WarehouseEditComponent implements OnInit {

    form: FormGroup;
    enable: any;
    public warehouseId: string;
    public warehouseName: string;

    constructor(private fb: FormBuilder,
                private http: HttpClient,
                public utilsService: UtilsService,
                private router: Router,
                private route: ActivatedRoute) {

        this.warehouseId = this.route.snapshot.paramMap.get('warehouseId');
        this.warehouseName = this.route.snapshot.paramMap.get('warehouseName');

    }

    ngOnInit() {
        this.fetchWarehouseForm();
        this.getWarehouseInfo();
    }

    // fetch warehouse form.
    fetchWarehouseForm() {
        this.form = this.fb.group({
            title: [''],
            description: [''],
            import_value: [''],
            export_value: [''],
            daily_freight: [''],
            enable: ['']
        });
    }

    // get warehouse info to update.
    getWarehouseInfo() {
        this.utilsService.reqGET('/warehouse', this.warehouseId, resp => {
            if (resp && resp['code'] === 20000) {
                this.form.setValue({
                    title: resp['data'].title,
                    description: resp['data'].description,
                    import_value: resp['data']['support'].map(r => {
                        return r.charged_warehouse.import_value;
                    }),
                    export_value: resp['data']['support'].map(r => {
                        return r.charged_warehouse.export_value;
                    }),
                    daily_freight: resp['data']['support'].map(r => {
                        return r.charged_warehouse.daily_freight;
                    }),
                    enable: resp['data'].enable
                });
            } else if (error) {
                alert('This Warehouse is not available');
                this.utilsService.locationBack();
            }
        });
    }

    // submit update warehouse info.
    updateWarehouse() {
        const params = {
            title: this.form.value.title,
            description: this.form.value.description,
            import_value: this.form.value.import_value,
            export_value: this.form.value.export_value,
            daily_freight: this.form.value.daily_freight,
            enable: this.form.value.enable
        };
        this.utilsService.reqPUT('/warehouse/' + this.warehouseId, params, resp => {
        });
        this.router.navigate(['/warehouse/warehouse-list']);
    }

}
