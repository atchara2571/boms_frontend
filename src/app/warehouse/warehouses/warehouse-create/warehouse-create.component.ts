import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UtilsService} from '../../../helper/utils.service';
import {Router} from '@angular/router';
import {CompanyCreateComponent} from '../../../company/company-create/company-create.component';
import {Partner} from '../../../models/Partner.model';
import {AuthService} from '../../../helper/auth.service';
import {Company} from '../../../models/Company.model';

const ELEMENT_DATA = [
    
]


@Component({
    providers: [CompanyCreateComponent],
    selector: 'app-warehouse-create',
    templateUrl: './warehouse-create.component.html',
    styleUrls: ['./warehouse-create.component.scss']
})
export class WarehouseCreateComponent implements OnInit {

    // errors = errorMessages;
    form: FormGroup;
    companies: Company[] = [];
    partners: Partner[] = [];
    companyId: string;
    partnerId: string;
    private userId: string;
    displayedColumn = [];
    dataSource = ELEMENT_DATA;
    provinces = [
        {title: 'เชียงราย'},
        {title: 'เชียงใหม่'},
        {title: 'พะเยา'}
    ];

    constructor(
        private fb: FormBuilder,
        public utilsService: UtilsService,
        public authService: AuthService,
        private router: Router) {
    }

    ngOnInit() {
        this.displayedColumn = ['province', 'description', 'action']
        this.userId = this.authService.getUserId();
        this.fetchWarehouseForm();
        this.getCompanyByUserId();
    }

    // fetch warehouse form.
    fetchWarehouseForm() {
        this.form = this.fb.group({
            title: [''],
            address: [''],
            district: [''],
            sub_district: ['']
        });
    }

    // get company sort by user id.
    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
                const comList = resp.data.filter(c => c);
                _this.companies = comList;
            }
        });
    }

    // picked company id.
    pickedCompany(i) {
        this.companyId = this.companies[i]['_id'];
        this.getPartnerByCompany();
    }


    // get partner sort by company.
    getPartnerByCompany() {
        const _this = this;
        this.utilsService.reqGET('/partner/company/' + this.companyId, null, resp => {
            if (resp && resp.code === 20000) {
                const partnerList = resp.data.filter(idCom => idCom.company === _this.companyId);
                _this.partners = partnerList;
            }
        });
    }

    // create new warehouse
    createWarehouse() {
        if (this.form.invalid) {
            return;
        }
        const params = {
            'title': this.form.value.title,
            'description': this.form.value.description,
            'partners': this.form.value.partner,
            'support': [{
                'company': this.companyId,
                'charged_warehouse': {
                    'import_value': this.form.value.import_value,
                    'daily_freight': this.form.value.daily_freight,
                    'export_value': this.form.value.export_value,
                }
            }],
            'enable': this.form.value.enable,
        };
        const _this = this;
        if (this.form.valid) {
            this.utilsService.reqPOST('/warehouse', params, function (resp) {
                if (resp && resp.code === 20000) {
                    alert('Create Successful!');
                    _this.router.navigate(['/warehouse/warehouse-list']);
                } else {
                    return false;
                }
            });
        }
    }

    // back to previous page.
    back() {
        this.utilsService.locationBack();
    }

}


