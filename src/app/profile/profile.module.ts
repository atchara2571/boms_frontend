import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FlexLayoutModule } from '@angular/flex-layout';

import { DynamicFormsCoreModule } from '@ng-dynamic-forms/core';
import { DynamicFormsMaterialUIModule } from '@ng-dynamic-forms/ui-material';

import { TreeModule } from 'angular-tree-component';

import { ProfileRoutes } from './profile.routing';
import { ProfileComponent } from './profile.component';

import {AngularMaterialModule} from '../angular-material.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProfileRoutes),
    FlexLayoutModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    DynamicFormsCoreModule.forRoot(),
    DynamicFormsMaterialUIModule,
    TreeModule,
    AngularMaterialModule
  ],
  declarations: [
    ProfileComponent
  ],
})

export class ProfileModule {}
