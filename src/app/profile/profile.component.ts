import {Component, OnInit} from '@angular/core';
import {AuthService} from '../helper/auth.service';
import {UtilsService} from '../helper/utils.service';
import {FormGroup, FormControl} from '@angular/forms';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    public defaultImage = 'assets/images/avatar.jpg';
    form: FormGroup;
    isViewable: boolean;
    disabled = true;
    imagePreview: string;
    update: boolean;
    image: string;

    // isLoading = false;

    constructor(public authService: AuthService,
                public utilsService: UtilsService) {
    }

    ngOnInit() {
        this.isViewable = true;
        this.fetchProfileForm();
        this.getProfileInfo();
    }

    // get current user's profile info.
    getProfileInfo() {
        this.utilsService.reqGET('/profile', null, resp => {
            if (resp && resp['code'] === 20000) {
                this.form.setValue({
                    firstname: resp['data'].userinfo.firstname,
                    lastname: resp['data'].userinfo.lastname,
                    birthday: resp['data'].userinfo.birthday,
                    email: resp['data'].userinfo.email,
                    address: resp['data'].userinfo.address.address,
                    zipcode: resp['data'].userinfo.address.zipcode,
                    card_id: resp['data'].userinfo.card_id,
                    lineid: resp['data'].userinfo.lineid,
                    facebook_id: resp['data'].userinfo.facebook_id,
                    image: resp['data'].userinfo.image_profile
                });
            }
        });
    }

    // fetch profile form.
    fetchProfileForm() {
        this.form = new FormGroup({
            firstname: new FormControl({value: '', disabled: true}),
            lastname: new FormControl({value: '', disabled: true}),
            birthday: new FormControl({value: '', disabled: true}),
            email: new FormControl({value: '', disabled: true}),
            address: new FormControl({value: '', disabled: true}),
            zipcode: new FormControl({value: '', disabled: true}),
            card_id: new FormControl({value: '', disabled: true}),
            lineid: new FormControl({value: '', disabled: true}),
            facebook_id: new FormControl({value: '', disabled: true}),
            image: new FormControl({value: '', disabled: true})
        });
    }

    // enable form field.
    onEditProfile() {
        this.form.enable();
        this.isViewable = false;
        this.disabled = !this.disabled;
    }

    // get image file.
    onImagePicked(event: Event) {
        const file = (event.target as HTMLInputElement).files[0];
        this.form.patchValue({image: file});
        this.form.get('image').updateValueAndValidity();
        const reader = new FileReader();
        reader.onload = () => {
            this.imagePreview = reader.result;
        };
        reader.readAsDataURL(file);
    }

    // submit edit profile info.
    updateProfile(image: File) {
        image = this.form.value.image;
        // const _this = this;
        const profileData = new FormData();
        profileData.append('firstname', this.form.value.firstname),
            profileData.append('lastname', this.form.value.lastname),
            profileData.append('birthday', this.form.value.birthday),
            profileData.append('email', this.form.value.email),
            profileData.append('address', this.form.value.address),
            profileData.append('zipcode', this.form.value.zipcode),
            profileData.append('card_id', this.form.value.card_id),
            profileData.append('lineid', this.form.value.lineid),
            profileData.append('facebook_id', this.form.value.facebook_id),
            profileData.append('image', image, image.name);
        this.utilsService.reqPUT('/change/profile', profileData, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
                alert('update successful');
                this.isViewable = true;
                // window.location.href = '/profile';
                this.ngOnInit();
            }
        });
    }

    // removeImageProfile() {
    //     this.form.value.image = this.defaultImage;
    // }

}
