import {Component, OnInit} from '@angular/core';
import {PromotionModel} from '../../models/Promotion.model';

@Component({
    selector: 'app-promotions-list',
    templateUrl: './promotions-list.component.html',
    styleUrls: ['./promotions-list.component.scss']
})
export class PromotionsListComponent implements OnInit {

    dataSource;
    displayedColumn = [];
    public promotions: PromotionModel[] = [];

    constructor() {
    }

    ngOnInit() {
        this.displayedColumn = ['no', 'title', 'description', 'promote_start', 'promote_end', 'usage_start',
            'usage_end', 'discount_minimum_piece', 'discount_minimum_total', 'discount_price', 'discount_rate', 'items', 'status'];
    }


}
