import { Routes } from '@angular/router';
import {PromotionsCreateComponent} from './promotions-create/promotions-create.component';
import {PromotionsListComponent} from './promotions-list/promotions-list.component';

export const PromotionRoutes: Routes = [
    {
        path: 'promotions-create',
        component: PromotionsCreateComponent,
        data: {
            heading: 'Create Promotion'
        }
    },
    {
        path: 'promotions-list',
        component: PromotionsListComponent,
        data: {
            heading: 'Promotion List'
        }
    },
];
