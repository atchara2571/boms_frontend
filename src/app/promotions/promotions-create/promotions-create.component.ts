import {Component, OnInit} from '@angular/core';
import {UtilsService} from '../../helper/utils.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Company} from '../../models/Company.model';
import {MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {DatePipe} from '@angular/common';

@Component({
    selector: 'app-promotions-create',
    templateUrl: './promotions-create.component.html',
    styleUrls: ['./promotions-create.component.scss'],
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'th-TH'}
    ]
})
export class PromotionsCreateComponent implements OnInit {

    form: FormGroup;
    public userId: string;
    public companyId: string;
    public companies: Company[] = [];

    constructor(public utilsService: UtilsService,
                private fb: FormBuilder) {
    }

    ngOnInit() {
        this.fetchPromotionForm();
    }

    fetchPromotionForm() {
        this.form = this.fb.group({
            title: [''],
            description: [''],
            company: [''],
            promotion_start: [''],
            promotion_end: [''],
            usage_start: [''],
            usage_end: [''],
            minimum_piece: [''],
            minimum_total: [''],
            price: [''],
            rate: [''],
            items: [''],
            enable: ['']
        });
    }

    // companies selection drop down list.
    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const comList = resp.data.filter(c => c);
                _this.companies = comList;
            }
        });
    }


    createNewPromotion() {
        const params = {
            title: this.form.value.title,
            description: this.form.value.description,
            company: this.form.value.company,
            promote: {
                start: this.form.value.promotion_start,
                end: this.form.value.promotion_end,
            },
            usage: {
                start: this.form.value.usage_start,
                end: this.form.value.usage_end
            },
            discount: {
                minimum_piece: this.form.value.minimum_piece,
                minimum_total: this.form.value.minimum_total,
                price: this.form.value.price,
                rate: this.form.value.rate
            },
            items: this.form.value.items,
            enable: this.form.value.enable
        };
        this.utilsService.reqPOST('/promotions/create', params, resp => {
            if (resp && resp.code === 20000) {
                return;
            }
        });
    }

    back() {
        this.utilsService.locationBack();
    }

}
