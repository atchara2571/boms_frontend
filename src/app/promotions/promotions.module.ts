import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {AngularMaterialModule} from '../angular-material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SatPopoverModule} from '@ncstate/sat-popover';
import {PromotionsCreateComponent} from './promotions-create/promotions-create.component';
import {PromotionRoutes} from './promotions.routing';
import {PromotionsListComponent} from './promotions-list/promotions-list.component';

@NgModule({
    imports: [
        RouterModule.forChild(PromotionRoutes),
        CommonModule,
        AngularMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        SatPopoverModule
    ],
    declarations: [
        PromotionsCreateComponent,
        PromotionsListComponent
    ]
})

export class PromotionsModule {}