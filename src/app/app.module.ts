import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HttpClient, HTTP_INTERCEPTORS} from '@angular/common/http';
// Angular Flexlayout
import {FlexLayoutModule} from '@angular/flex-layout';
// ngx-translate
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
// ngx-loading-bar
import {LoadingBarRouterModule} from '@ngx-loading-bar/router';
import {LoadingBarModule} from '@ngx-loading-bar/core';
// ngx-perfect-scrollbar
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {PERFECT_SCROLLBAR_CONFIG} from 'ngx-perfect-scrollbar';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
// google maps
import {AgmCoreModule} from '@agm/core';
// Template core Authentication
import {
    MenuComponent,
    HeaderComponent,
    SidebarComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    OptionsComponent,
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective
} from './core/index';

import {AppRoutes} from './app.routing';
import {AppComponent} from './app.component';
import {UtilsService} from './helper/utils.service';

import {AuthService} from './helper/auth.service';
import {AngularMaterialModule} from './angular-material.module';
import {HeaderInterceptor} from './helper/header-interceptor';
import {ErrorInterceptor} from './error/error-interceptor';
import {ErrorComponent} from './error/error.component';
import {ExportService} from './warehouse/store-products/export-file/export.service';
import {SocialLoginModule} from 'angular5-social-login';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    wheelSpeed: 2,
    wheelPropagation: true
};

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        SidebarComponent,
        MenuComponent,
        AdminLayoutComponent,
        AuthLayoutComponent,
        OptionsComponent,
        AccordionAnchorDirective,
        AccordionLinkDirective,
        AccordionDirective,
        ErrorComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutes,
        FormsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        FlexLayoutModule,
        LoadingBarRouterModule,
        LoadingBarModule.forRoot(),
        PerfectScrollbarModule,
        AngularMaterialModule,
        ReactiveFormsModule,
        NoopAnimationsModule,
        SocialLoginModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAPaM5cz4R31PqPTM4WrPNyPWxotTR-XRA',
            libraries: ['places']
        })
    ],
    providers: [
        UtilsService,
        AuthService,
        ExportService,
        // {
        //   provide: HTTP_INTERCEPTORS,
        //   useClass: AuthInterceptor,
        //   multi: true
        // },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HeaderInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorInterceptor,
            multi: true
        },
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
