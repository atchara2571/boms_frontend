import {Routes} from '@angular/router';
import {JobSheetsListComponent} from './job-sheets-list/job-sheets-list.component';
import {JobSheetsCreateComponent} from './job-sheets-create/job-sheets-create.component';
import {JobSheetsEditComponent} from './job-sheets-edit/job-sheets-edit.component';

export const JobSheetRoutes: Routes = [
    {
        path: 'job-sheets-create',
        component: JobSheetsCreateComponent,
        data: {
            heading: 'Create Job Sheet',
            css: 'view-no-padding'
        }
    },
    {
        path: 'job-sheets-edit',
        component: JobSheetsEditComponent,
        data: {
            heading: 'Edit Job Sheet Info',
            css: 'view-no-padding'
        }
    },
    {
        path: 'job-sheets-list',
        component: JobSheetsListComponent,
        data: {
            heading: 'Job Sheet List',
            css: 'view-no-padding'
        }
    },
]