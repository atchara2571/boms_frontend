import {Component, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../helper/utils.service';
import {JobSheetModel} from '../../models/JobSheet.model';
import {MatSort, MatTableDataSource} from '@angular/material';

@Component({
    selector: 'app-job-sheets-list',
    templateUrl: './job-sheets-list.component.html',
    styleUrls: ['./job-sheets-list.component.scss']
})
export class JobSheetsListComponent implements OnInit {

    jobSheets: JobSheetModel[] = [];
    displayedColumns = [];
    dataSource;
    workers = [];
    @ViewChild(MatSort) sort: MatSort;

    constructor(public utilsService: UtilsService) {
    }

    ngOnInit() {
        this.displayedColumns = [
            'no', 'company', 'car', 'type_name', 'type_key',
            'type_description', 'start', 'benefit_total', 'benefit_company', 'benefit_expense',
            'benefit_system', 'worker_type', 'worker_allowance', 'worker_by', 'worker_date',
            'jp_lat', 'jp_long', 'jp_note', 'jp_signature', 'jp_image', 'jp_start',
            'jp_end', 'jp_location_lat', 'jp_location_long', 'jp_location_address', 'jp_contact_note',
            'jp_contact_msisdn', 'jp_contact_name'
        ];
        this.getJobSheet();
    }

    getJobSheet() {
        const _this = this;
        this.utilsService.reqGET('/jobsheets', null, resp => {
            if (resp && resp.code === 20000) {
                const jsList = resp.data;
                _this.jobSheets.push(jsList);
                _this.dataSource = new MatTableDataSource(_this.jobSheets);
                _this.dataSource.sort = _this.sort;
            }
        });
    }

}
