import {Component, OnInit} from '@angular/core';
import {JobSheetModel} from '../../models/JobSheet.model';
import {UtilsService} from '../../helper/utils.service';
import {Company} from '../../models/Company.model';
import {AuthService} from '../../helper/auth.service';
import {CarModel} from '../../models/Car.model';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';

@Component({
    selector: 'app-job-sheets-edit',
    templateUrl: './job-sheets-edit.component.html',
    styleUrls: ['./job-sheets-edit.component.scss']
})
export class JobSheetsEditComponent implements OnInit {

    form: FormGroup;
    public userId: string;
    public companyId: string;
    public carId: string;
    public jobSheetId: string;
    public jobSheets: JobSheetModel[] = [];
    public companies: Company[] = [];
    public cars: CarModel[] = [];

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private fb: FormBuilder) {

        this.userId = this.authService.getUserId();

    }

    ngOnInit() {
        this.fetchJobSheetForm();
        this.getJobSheetById();
        this.getCompanyByUserId();
        this.getCar();
    }

    fetchJobSheetForm() {
        this.form = this.fb.group({
            company: [''],
            cars: [''],
            type_name: [''],
            type_key: [''],
            type_description: [''],
            start: [''],
            benefit_total: [''],
            benefit_company: [''],
            benefit_expense: [''],
            system: [''],
            worker_type: [''],
            worker_allowance: [''],
            worker_by: [''],
            worker_date: ['']
        });
    }

    getJobSheetById() {
        this.utilsService.reqGET('/jobsheets', this.jobSheetId, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
            }
        });
    }

    // companies selection drop down list.
    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const comList = resp.data.filter(c => c);
                _this.companies = comList;
            }
        });
    }


    getCar() {
        const _this = this;
        this.utilsService.reqGET('/config/type/cars', null, resp => {
            if (resp && resp.code === 20000) {
                const data = resp.data.filter(c => c);
                _this.cars = data;
            }
        });
    }

    back() {
        this.utilsService.locationBack();
    }

    updateJobSheet(jobSheetForm: NgForm) {
    }

}
