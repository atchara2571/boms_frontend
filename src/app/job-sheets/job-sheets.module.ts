import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {AngularMaterialModule} from '../angular-material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SatPopoverModule} from '@ncstate/sat-popover';
import {JobSheetsCreateComponent} from './job-sheets-create/job-sheets-create.component';
import {JobSheetsEditComponent} from './job-sheets-edit/job-sheets-edit.component';
import {JobSheetsListComponent} from './job-sheets-list/job-sheets-list.component';
import {JobSheetRoutes} from './job-sheets.routing';

@NgModule({
    imports: [
        RouterModule.forChild(JobSheetRoutes),
        CommonModule,
        AngularMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        SatPopoverModule
    ],
    declarations: [
        JobSheetsCreateComponent,
        JobSheetsEditComponent,
        JobSheetsListComponent
    ]
})

export class JobSheetsModule {}
