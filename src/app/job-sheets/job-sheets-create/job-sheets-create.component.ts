import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../helper/utils.service';
import {Company} from '../../models/Company.model';
import {CarModel} from '../../models/Car.model';
import {AuthService} from '../../helper/auth.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-job-sheets-create',
    templateUrl: './job-sheets-create.component.html',
    styleUrls: ['./job-sheets-create.component.scss']
})
export class JobSheetsCreateComponent implements OnInit {

    form: FormGroup;
    public userId: string;
    public companyId: string;
    public carId: string;
    public companies: Company[] = [];
    public cars: CarModel[] = [];
    @ViewChild('ref') elementRef: ElementRef;

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private fb: FormBuilder) {
        this.userId = this.authService.getUserId();
    }

    ngOnInit() {
        this.getCompanyByUserId();
        this.getCar();
        this.fetchForm();
    }


    fetchForm() {
        this.form = this.fb.group({
            company: [''],
            cars: [''],
            type_name: [''],
            type_key: [''],
            type_description: [''],
            start: [''],
            benefit_total: [''],
            benefit_company: [''],
            benefit_expense: [''],
            benefit_system: [''],
            worker_type: [''],
            worker_allowance: [''],
            worker_by: [''],
            worker_date: [''],
            lat: [''],
            long: [''],
            products: [''],
            jp_description: [''],
            jp_title: [''],
            jp_lat: [''],
            jp_long: [''],
            jp_note: [''],
            jp_signature: [''],
            jp_image: [''],
            jp_start: [''],
            jp_end: [''],
            jp_location_lat: [''],
            jp_location_long: [''],
            jp_location_address: [''],
            jp_contact_note: [''],
            jp_contact_msisdn: [''],
            jp_contact_name: ['']
        });
    }

    // companies selection drop down list.
    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                const comList = resp.data.filter(c => c);
                _this.companies = comList;
            }
        });
    }

    getCar() {
        const _this = this;
        this.utilsService.reqGET('/config/type/cars', null, resp => {
            if (resp && resp.code === 20000) {
                const data = resp.data.filter(c => c);
                _this.cars = data;
            }
        });
    }

    back() {
        this.utilsService.locationBack();
    }

    createNewJobSheet() {
        const params = {
            company: this.form.value.company,
            cars: this.form.value.cars,
            type_name: this.form.value.type_name,
            type_key: this.form.value.type_key,
            type_description: this.form.value.type_description,
            start: this.form.value.start,
            benefit_total: this.form.value.benefit_total,
            benefit_company: this.form.value.benefit_company,
            benefit_expense: this.form.value.benefit_expense,
            benefit_system: this.form.value.benefit_system,
            worker_type: this.form.value.worker_type,
            worker_allowance: this.form.value.worker_allowance,
            worker_by: this.form.value.worker_by,
            worker_date: this.form.value.worker_date,
            lat: this.form.value.lat,
            long: this.form.value.long,
            jp_start: this.form.value.jp_start,
            jp_end: this.form.value.jp_end,
            jp_location_lat: this.form.value.jp_location_lat,
            jp_location_long: this.form.value.jp_location_long,
            jp_location_address: this.form.value.jp_location_address,
            jp_contact_note: this.form.value.jp_contact_note,
            jp_contact_msisdn: this.form.value.jp_contact_msisdn,
            jp_contact_name: this.form.value.jp_contact_name
        };
        this.utilsService.reqPOST('/jobsheets', params, resp => {
            if (resp && resp.code === 20000) {
                return;
            }
        });
    }
}
