import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UtilsService} from '../helper/utils.service';
import {ActivatedRoute} from '@angular/router';

export interface DialogData {
  edit: string;
  cId: string;
  index: number;
}

@Component({
  selector: 'app-inline-edit',
  styleUrls: ['inline-edit.component.scss'],
  template: `
    <h1 mat-dialog-title>Edit</h1>
    <div mat-dialog-content>
      <form [formGroup]="form">
        <div class="formGroup">
          <mat-form-field>
            <input matInput formControlName="email">
          </mat-form-field>
          <div mat-dialog-actions>
            <button mat-button (click)="onNoClick()">Cancel</button>
            <button mat-button cdkFocusInitial (click)="updateCompanyInfo()">Ok</button>
          </div>
        </div>
      </form>
    </div>
  `
})

export class InlineEditComponent implements OnInit {

  form: FormGroup;
  public companyId: string;

  constructor(
    public dialogRef: MatDialogRef<InlineEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private fb: FormBuilder,
    public utilsService: UtilsService,
    private route: ActivatedRoute) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.form = this.fb.group({
      email: [this.data.edit]
    });
    console.log(this.data.index);
  }

  updateCompanyInfo() {
    const params = {

    };
    this.utilsService.reqPUT('/companys/' + this.data.cId, params[this.data.index], resp => {
      console.log(resp);
    });
  }

}
