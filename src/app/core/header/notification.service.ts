import { Injectable } from '@angular/core';

export interface Notice {
  text: string;
  subtext?: string;
  date?: number;
  icon: string;
  color?: string;
}

@Injectable()
export class NotificationService {
  getAll(): Notice[] {
    return [];
  }
}
