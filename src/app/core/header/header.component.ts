import {Component, EventEmitter, OnInit, OnDestroy, Output} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material';
import {Subscription} from 'rxjs/Subscription';

import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {AuthService} from '../../helper/auth.service';
import {UtilsService} from '../../helper/utils.service';
import {error} from 'util';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    providers: []
})
export class HeaderComponent implements OnInit, OnDestroy {
    userIsAuthenticated = false;
    defaultImage = 'assets/images/avatar.jpg';
    private authListenerSubs: Subscription;

    @Output() toggleSidenav = new EventEmitter<void>();

    public config: PerfectScrollbarConfigInterface = {};
    image: string;

    constructor(
        private authService: AuthService,
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer,
        private utilsService: UtilsService) {
        iconRegistry.addSvgIcon(
            'search-icon',
            sanitizer.bypassSecurityTrustResourceUrl('assets/images/search.svg'));
    }

    ngOnInit() {
        this.userIsAuthenticated = this.authService.getIsAuth();
        this.authListenerSubs = this.authService.getAuthStatusListener().subscribe(
            (isAuthenticated) => {
                this.userIsAuthenticated = isAuthenticated;
            }
        );
        this.utilsService.reqGET('/profile', null, resp => {
            if (resp && resp['code'] === 20000) {
                const image = resp['data'].userinfo.image_profile;
                this.image = image;
            }
        });
    }

    onLogout() {
        this.authService.signout();
    }

    ngOnDestroy() {
        this.authListenerSubs.unsubscribe();
    }
}
