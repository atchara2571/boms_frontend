import {
  Component, ViewEncapsulation
} from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: 'menu.component.html',
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})
export class MenuComponent {
  panelOpenState = false;
  displayMode = 'default';
  multi = false;
  hideToggle = false;
  disabled = false;
  showPanel3 = true;
  expandedHeight: string;
  collapsedHeight: string;
}
