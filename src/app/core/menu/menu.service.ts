import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface Menu {
  state?: string;
  name?: string;
  type: string;
  icon?: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  // {
  //   type: 'title',
  //   name: 'Company View'
  // },
  // {
  //   state: 'company',
  //   name: 'Company',
  //   type: 'sub',
  //   icon: 'store',
  //   children: [
  //     {
  //       state: 'company-list',
  //       name: 'Company List',
  //       type: 'sub'
  //     },
  // {
  //   state: 'company-user',
  //   name: 'Company User',
  //   type: 'sub'
  // }
  //  ]
  // },
  // {
  //   type: 'divider'
  // },
  {
    type: 'title',
    name: 'View by account'
  },
  // {
  //   type: 'title',
  //   name: 'Partner'
  // },
  {
    state: 'company',
    name: 'Company',
    type: 'sub',
    icon: 'store',
    children: [
      {
        state: 'company-list',
        name: 'All Company',
        children: [
          {
            state: 'company-list',
            name: 'All Company',
          }
        ]
      },
      // {
      //   state: 'company-user',
      //   name: 'Company User',
      //   type: 'sub'
      // },
      // {
      //   state: 'warehouse-by-company',
      //   name: 'Warehouse by Company',
      //   type: 'sub'
      // },
      {
        state: 'company-create',
        name: 'Create Company',
        type: 'sub'
      }
    ]
  },
  {
    state: 'partner',
    name: 'Partner',
    type: 'sub',
    icon: 'people',
    children: [
      {
        state: 'partner-create',
        name: 'Create Partner',
        type: 'sub',
      },
      {
        state: 'partner-list',
        name: 'All Partner',
        type: 'sub',
        children: [
          {
            state: 'partner-create',
            name: 'Create Partner',
            type: 'sub',
          }
        ]
      }
    ]
  },
  // {
  //   type: 'title',
  //   name: 'Product'
  // },
  {
    state: 'product',
    name: 'Product',
    type: 'sub',
    icon: 'free_breakfast',
    children: [
      {
        state: 'product-type-list',
        name: 'All Product Type',
        type: 'sub'
      },
      {
        state: 'product-list',
        name: 'All Product',
        type: 'sub'
      },
      {
        state: 'product-type-create',
        name: 'Create Product Type',
        type: 'sub'
      },
      {
        state: 'product-create',
        name: 'Create Product',
        type: 'sub'
      }
    ]
  },
  // {
  //   type: 'title',
  //   name: 'Store Product'
  // },
  // {
  //   state: 'store-product',
  //   name: 'Store Product',
  //   type: 'sub',
  //   icon: 'shopping_basket',
  //   children: [
  //     {
  //       state: 'store-product-by-company-warehouse-all',
  //       name: 'Warehouse by Company',
  //       type: 'sub'
  //     },
  // {
  //   state: 'store-product-create',
  //   name: 'Create Store Product',
  //   type: 'sub'
  // },
  //     {
  //       state: 'store-product-list',
  //       name: 'Store Product List',
  //       type: 'sub'
  //     }
  //   ]
  // },
  // {
  //   type: 'title',
  //   name: 'Warehouse'
  // },
  {
    state: 'warehouse',
    name: 'Warehouse',
    type: 'sub',
    icon: 'store',
    children: [
      {
        state: 'warehouse-list',
        name: 'All Warehouse',
      },
      {
        state: 'warehouse-by-company',
        name: 'Warehouse by Company',
      },
      {
        state: 'warehouse-create',
        name: 'Create Warehouse'
      },
    ]
  },
  {
    type: 'divider'
  }
];

@Injectable()
export class MenuService {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  add(menu: Menu) {
    MENUITEMS.push(menu);
  }
}
