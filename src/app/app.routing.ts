import {RouterModule, Routes} from '@angular/router';
import {AdminLayoutComponent, AuthLayoutComponent} from './core/index';
import {NgModule} from '@angular/core';
import {AuthGuard} from './authen/auth.guards';

export const routes: Routes = [
    {
        path: '',
        component: AdminLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './dashboard/dashboard.module#DashboardModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule',
                canActivate: [AuthGuard]
            },
            {
                path: 'warehouse',
                loadChildren: './warehouse/warehouses/warehouse.module#WarehouseModule',

            },
            {
                path: 'profile',
                loadChildren: './profile/profile.module#ProfileModule',
                canActivate: [AuthGuard]

            },
            {
                path: 'company',
                loadChildren: './company/company.module#CompanyModule',
                canActivate: [AuthGuard]

            },
            {
                path: 'product',
                loadChildren: './warehouse/products/product.module#ProductModule',
                canActivate: [AuthGuard]

            },
            {
                path: 'partner',
                loadChildren: './warehouse/partners/partner.module#PartnerModule',
                canActivate: [AuthGuard]

            },
            {
                path: 'store-product',
                loadChildren: './warehouse/store-products/store-product.module#StoreProductModule',
                canActivate: [AuthGuard]

            },
            // {
            //   path: 'warehouse-by-company',
            //   loadChildren: './warehouse/warehouse-by-company/warehouse-by-company.module#WarehouseByCompanyModule',
            //   canActivate: [AuthGuard]
            //
            // },
            // {
            //     path: 'map',
            //     loadChildren: './map/map.module#MapModule',
            //     canActivate: [AuthGuard]
            //
            // },
            {
                path: 'types',
                loadChildren: './types/types.module#TypesModule',
                canActivate: [AuthGuard]

            },
            {
                path: 'transportation',
                loadChildren: './transportation/transportation.module#TransportationModule',
                canActivate: [AuthGuard]

            },
            {
                path: 'job-sheets',
                loadChildren: './job-sheets/job-sheets.module#JobSheetsModule',
                canActivate: [AuthGuard]

            },
            {
                path: 'promotions',
                loadChildren: './promotions/promotions.module#PromotionsModule',
                canActivate: [AuthGuard]

            }
        ]
    },
    {
        path: '',
        component: AuthLayoutComponent,
        children: [{
            path: 'authen',
            loadChildren: './authen/auth.module#AuthModule'
        }]
    },
    // {
    //   path: 'map',
    //   loadChildren: './map/map.module#MapModule',
    //   canActivate: [AuthGuard]
    //
    // },
    {
        path: '**',
        redirectTo: 'error/404'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [AuthGuard]
})

export class AppRoutes {
}
