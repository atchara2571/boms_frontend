import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SignupComponent} from '../register/signup/signup.component';
import {SigninComponent} from './signin/signin.component';
import {AngularMaterialModule} from '../angular-material.module';
import {AuthRoutingModule} from './auth-routing.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {VerifyUserComponent} from '../register/verify-user/verify-user.component';

@NgModule({
  declarations: [
    SignupComponent,
    SigninComponent,
    VerifyUserComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    AuthRoutingModule,
    FlexLayoutModule
  ]
})
export class AuthModule {}
