import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SigninComponent} from './signin/signin.component';
import {SignupComponent} from '../register/signup/signup.component';
import {VerifyUserComponent} from '../register/verify-user/verify-user.component';

const routes: Routes = [
  {
    path: 'signin',
    component: SigninComponent

  },
  {
    path: 'signup',
    component: SignupComponent,
  },
  {
    path: 'verify-user',
    component: VerifyUserComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AuthRoutingModule {}
