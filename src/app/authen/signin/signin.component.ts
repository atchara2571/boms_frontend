import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import { NgForm } from '@angular/forms';
import {AuthService} from '../../helper/auth.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit, OnDestroy {

  isLoading = false;
  public form: FormGroup;
  private authStatusSub: Subscription;

  constructor(public authService: AuthService) { }

  ngOnInit() {
    // login subscribtion
    this.authStatusSub = this.authService.getAuthStatusListener().subscribe(
      authStatus => {
        this.isLoading = false;
      });
  }

  // signin function
  signIn(form: NgForm) {
    if (form.invalid) {
      return;
    }
    // this.isLoading = true;
    this.authService.signin(form.value.username, form.value.password);
  }

  // unsubscribe when user not logged in
  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }

}
