import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import {AccountData} from '../models/Account.model';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Location} from '@angular/common';


const backend_url = environment.apiUrl;

@Injectable()
export class AuthService {

    users: AccountData[];

    private transaction: string;
    private isAuthenticated = false;
    private x_access_token: string;
    private tokenTimer: any;
    private userId: string;
    private email: string;
    private authStatusListener = new Subject<boolean>();
    // private userName: string;


    constructor(private http: HttpClient, private router: Router, public location: Location) {
        // this.autoAuthUser();
    }

    // get user's token.
    getToken() {
        return this.x_access_token;
    }

    // get user authen status.
    getAuthStatusListener() {
        return this.authStatusListener.asObservable();
    }

    // when user log in successful.
    getIsAuth() {
        return this.isAuthenticated;
    }

    // get user id.
    getUserId() {
        return this.userId;
    }

    // get user's current email.
    getEmail() {
        return this.email;
    }

    // check user authen status when starting web app.
    autoAuthUser() {
        const authInformation = this.getAuthData();
        if (!authInformation) {
            return;
        } else {
            this.x_access_token = authInformation.x_access_token;
            this.isAuthenticated = true;
            this.userId = authInformation.userId;
            this.authStatusListener.next(true);
        }
    }

    // check username.
    checkUsername(username: string) {
        this.http.get<{ code: number }>
        (backend_url + '/register/check/username/' + username)
            .subscribe(resp => {
                if (resp.code === 20000) {
                    return;
                }
            }, err => {
                if (err.error.code === 40301) {
                    alert('Duplicate Username');
                }
            });
    }

    // check phone number.
    checkMsisdn(msisdn: number) {
        this.http.get<{ code: number, msisdn: string }>
        (backend_url + '/register/check/msisdn/' + msisdn)
            .subscribe(resp => {
                if (resp.code === 20000) {
                    return;
                }
            }, err => {
                if (err.error.code === 40301) {
                    alert('Duplicate phone number');
                }
                console.log(err);
            });
    }

    // check email.
    checkEmail(email: string) {
        this.http.get<{ code: number, email: string }>
        (backend_url + '/register/check/email/' + email)
            .subscribe(resp => {
                if (resp.code === 20000) {
                    return;
                }
            }, err => {
                if (err.error.code === 40301) {
                    alert('Duplicate email');
                }
            });
    }

    // check facebook id
    checkFacebook(facebook_id: string) {
        this.http.get<{ code: number, facebook_id: string }>
        (backend_url + '/register/check/facebook/' + facebook_id)
            .subscribe(resp => {
                if (resp.code === 20000) {
                    return;
                }
            }, err => {
                if (err.error.code === 40301) {
                    alert('Duplicate facebook id');
                }
            });
    }

    // check facebook id.
    checkGoogle(google_id: string) {
        this.http.get<{ code: number, google_id: string }>
        (backend_url + '/register/check/google/' + google_id)
            .subscribe(resp => {
                if (resp.code === 20000) {
                    return;
                }
            }, err => {
                if (err.error.code === 40301) {
                    alert('Duplicate google id');
                }
            });
    }

    // register.
    createUser(
        username: string,
        password: string,
        firstname: string,
        lastname: string,
        card_id: string,
        birthday: string,
        msisdn: string,
        email: string,
        lineid: string,
        facebook_id: string,
        google_id: string,
        image: File) {
        const userData = new FormData();
        userData.append('username', username);
        userData.append('password', password);
        userData.append('firstname', firstname);
        userData.append('lastname', lastname);
        userData.append('card_id', card_id);
        userData.append('birthday', birthday);
        userData.append('msisdn', msisdn);
        userData.append('email', email);
        userData.append('lineid', lineid);
        userData.append('facebook_id', facebook_id);
        userData.append('google_id', google_id);
        userData.append('image', image, '');
        this.http
            .post<AccountData>(backend_url + '/register', userData)
            .subscribe(response => {
                    alert('Register Success! Please check verify your email address.');
                    this.saveRegisterData(username, email);
                    this.sendVerify(email);
                    this.router.navigate(['/authen/verify-user']);
                }, err => {
                    this.authStatusListener.next(false);
                }
            );
    }

    // signin.
    signin(username: string, password: string) {
        const authData = {username: username, password: password};
        this.http.post<AccountData>(backend_url + '/login/by/username', authData)
            .subscribe(response => {
                const dataRes = response.data;
                const userinfoRes = response.data.userinfo;
                const x_access_token = dataRes.x_access_token;
                const code = response.code;
                const status = dataRes.status;
                const userId = dataRes._id;
                const email = userinfoRes.email;
                if (status === 0) {
                    alert('Please verify your email');
                    this.saveAuthData(x_access_token, email, username, userId);
                    this.router.navigate(['/verify-user']);
                } else if (x_access_token !== null || code === 2000) {
                    this.isAuthenticated = true;
                    this.userId = userId;
                    this.authStatusListener.next(true);
                    this.saveAuthData(x_access_token, email, username, userId);
                    window.location.href = '/dashboard';

                }
            }, error => {
                this.authStatusListener.next(false);
            });
    }

    // send verification code.
    sendVerify(email) {
        // const email = localStorage.getItem('email');
        this.http
            .get<{
                data: {
                    transaction: string,
                    username: string, email: string
                }
            }>(
                backend_url + '/request/code/email/' + email).subscribe(response => {
            const transaction = response.data.transaction;
            this.transaction = transaction;
        }, error => {
            this.authStatusListener.next(false);
        });
    }

    // send otp code.
    sendCodeConfirm(otp: string) {
        const username = localStorage.getItem('username');
        // const transaction = localStorage.getItem('transaction');
        const sendCodeData = {username: username, otp: otp, transaction: this.transaction};
        this.http.post(backend_url + '/confirm/code', sendCodeData).subscribe(response => {
            this.clearAuthData();
            this.router.navigate(['/dashboard']);
        }, error => {
            this.authStatusListener.next(false);
        });
    }

    // signout.
    signout() {
        this.x_access_token = null;
        this.isAuthenticated = false;
        this.authStatusListener.next(false);
        this.userId = null;
        clearTimeout(this.tokenTimer);
        this.clearAuthData();
        this.router.navigate(['/authen/signin']);
    }

  // check user id and token when starting web app
  private getAuthData() {
    const x_access_token = localStorage.getItem('x_access_token');
    const userId = localStorage.getItem('userId');
    if (!x_access_token) {
      return;
    }
    return {
      x_access_token: x_access_token,
      userId: userId
    };
  }

    // save verify data to local storage
    private saveSendVerifyData(transaction: string) {
        localStorage.setItem('transaction', transaction);
    }

    // save user's neccessary data to local storage
    private saveAuthData(x_access_token, email, username, userId) {
        localStorage.setItem('x_access_token', x_access_token);
        localStorage.setItem('email', email);
        localStorage.setItem('username', username);
        localStorage.setItem('userId', userId);

    }

    // save user's email and username for send verify
    private saveRegisterData(username, email) {
        localStorage.setItem('email', email);
        localStorage.setItem('username', username);
    }

    // clear user data from local storage
    private clearAuthData() {
        localStorage.removeItem('x_access_token');
        localStorage.removeItem('userId');
        localStorage.removeItem('email');
        localStorage.removeItem('username');
        localStorage.removeItem('transaction');
    }

}
