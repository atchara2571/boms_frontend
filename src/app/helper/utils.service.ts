import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {environment} from '../../environments/environment';
import {Location} from '@angular/common';
import {AuthService} from './auth.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

const backend_url = environment.apiUrl;

@Injectable()
export class UtilsService {

    public testId = new BehaviorSubject<string>('1234');

    constructor(private http: HttpClient, private location: Location, public authService: AuthService) {

        this.testId.next('4567');

    }

    private companyModel = new BehaviorSubject('ddd');
    test = this.companyModel.asObservable();


    reqGET(url, params, callback) {
        this.http
            .get(backend_url + url + ((typeof params === 'string') ? '/' + params : ''))
            .subscribe(response => {
                if (typeof callback === 'function') {
                    callback(response);
                } else {
                    console.log(response);
                }
            }, error => {
                console.log(error);
            });
    }

    reqPOST(url, params, callback) {
        this.http
            .post(backend_url + url, params)
            .subscribe(response => {
                if (typeof callback === 'function') {
                    callback(response);
                } else {
                    console.log(response);
                }
            }, error => {
                console.log(error);
            });
    }

    reqPUT(url, params, callback) {
        this.http
            .put(backend_url + url, params)
            .subscribe(response => {
                if (typeof callback === 'function') {
                    callback(response);
                } else {
                    console.log(response);
                }
            }, error => {
                console.log(error);
            });
    }

    reqDELETE(url, params, callback) {
        this.http
            .delete(backend_url + url, params)
            .subscribe(response => {
                if (typeof callback === 'function') {
                    callback(response);
                } else {
                    console.log(response);
                }
            }, error => {
                console.log(error);
            });
    }

    locationBack() {
        this.location.back();
    }

}
