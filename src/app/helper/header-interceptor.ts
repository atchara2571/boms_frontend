import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/do';


import {AuthService} from '../helper/auth.service';


@Injectable()
export class HeaderInterceptor implements HttpInterceptor {
  // private AuthService: AuthService;

  constructor(private  authService: AuthService) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken = this.authService.getToken();
    let dummyHeader = {};
    if (authToken) {
      dummyHeader = {
        'Authorization': 'Bearer ' + (authToken),
        'x_access_token': (authToken),
      };
    }
    const dummyrequest = req.clone({
      setHeaders: dummyHeader
    });
    return next.handle(dummyrequest);
  }
}
