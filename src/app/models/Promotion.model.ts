export interface PromotionModel {
    title: string;
    description: string;
    company: string;
    promote: {
        start: string;
        end: string;
    };
    usage: {
        start: string;
        end: string;
    };
    discount: {
        minimum_piece: number;
        minimum_total: number;
        price: number;
        rate: number;
    };
    items: number;
    enable: string;
}
