export interface StoreProduct {
  _id: string;
  company: {
    _id: string;
    image: File;
    description: string;
    title: string;
  };
  warehouse: {
    _id: string;
    enable: boolean;
    description: string;
    title: string;
  };
  partner: {
    import: {
      _id: string;
      contact: {
        long: number;
        lat: number;
        address: string;
        email: string;
        msisdn: number;
        name: string;
      }
      title: string;
    }
  };
  serial_product: {
    title: string;
  };
  tag_code: string;
  enable: boolean;
  charged_product: {
    transportation: number;
    export_value: number;
    import_value: number;
    daily_freight: number;
  };
    charged_warehouse: {
        export_value: number;
        import_value: number;
        daily_freight: number;
    };
  total: string;
}
