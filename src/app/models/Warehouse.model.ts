export interface Warehouse {
      _id: string;
      title: string;
      description: string;
      support: {
          company: string;
          charged_warehouse: {
              import_value: number;
              daily_freight: number;
              export_value: number;
          }
      };
      partners: {
          title: string;
      };
      enable: string;
}

