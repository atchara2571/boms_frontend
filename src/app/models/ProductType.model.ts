export interface ProductType {
  _id: string;
  title: string;
  company: string;
  description: string;
  enable: string;
}
