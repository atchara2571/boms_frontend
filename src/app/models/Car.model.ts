export interface CarModel {
    _id: string;
    company: string;
    title: string;
    description: string;
    enable: string;
    support: {
        partners: string,
        distances: {
            number: number;
            price: number;
        },
        dimension: {
            weight: number;
            long: number;
            height: number;
        }
    }
}
