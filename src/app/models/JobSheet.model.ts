export interface JobSheetModel {
    company: string;
    ref: string;
    cars: string;
    type: {
        name: string;
        key: string;
        description: string;
    };
    start: string;
    benefit: {
        total: string;
        company: string;
        expense: string;
        system: string;
    };
    workers: {
        type: string;
        allowance: number;
        by: string;
        date: string;
    };
    job_position: {
        _id: string;
        control: {
            locations: {
                lat: number;
                long: number;
            };
            note: string;
            signature: string;
            image: string;
            end: string;
            start: string;
        };
        services: string;
        locations: {
            lat: string;
            long: string;
            address: string;
        };
        products: string;
        contact: {
            note: string;
            msisdn: string;
            name: string;
        };
        description: string;
        title: string;
    }
}