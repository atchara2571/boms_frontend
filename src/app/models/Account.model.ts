export interface AccountData {
  api_number: number;
  code: number;
  message: {
    en: string;
    th: string;
  };
  __v: number;
  data: {
    __v: string;
    username: string;
    password: string;
    _id: string;
    enable: boolean;
    userinfo: {
      birthday: string;
      msisdn: number;
      email: string;
      address: {
        zipcode: null;
        address: null;
      }
      google_id: string;
      facebook_id: string;
      lineid: string;
      card_id: number;
      image_profile: string;
      lastname: string;
      firstname: string;
    }
    last_login: null;
    state: number;
    status: number;
    token_expired: number;
    x_access_token: string;
    usertype_id: any;
  };
}
