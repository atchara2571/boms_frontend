export interface Partner {
  _id: string;
  title: string;
  company: string;
  contact: {
    name: string;
    msisdn: string;
    email: string;
    address: string;
    lat: string;
    long: string;
  };
  invoice: {};
  user: {
    by: string;
    type: string;
  };
}
