export interface DialogData {
    pdName: string;
    cName: string;
    pnName: string;
    pnId?: any;
    cId: string;
    wId: string;
    pId: string;
    status: any;
    supportId: string;
}