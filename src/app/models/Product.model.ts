export interface Product {
    _id: string;
    company: string;
    support: {
        partners: {
            title: string;
            _id: string;
        }
        charged_product: {
            import_value: number;
            daily_freight: number;
            export_value: number;
        }
    };
    serial_product: any;
    title: string;
    description: string;
    type: {
        _id: string;
        title: string;
        description: string;
        enable: string;
    };
    enable: boolean;
    image: string;
}
