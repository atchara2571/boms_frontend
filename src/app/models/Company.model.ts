export interface Company {
  _id: string;
  title: string;
  description: string;
  contact: {
    address: string;
    lat: string;
    long: string;
    telephone?: string;
    email: string;
    line: string;
    facebook: string;
    website: string;
  };
  enable: boolean;
  users: {
    by: string;
    type: number;
  };
  comment?: string;
}
