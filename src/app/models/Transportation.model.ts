export interface TransportationModel {
    company: string;
    partner: string;
    cars: string;
    type: {
        type_name: string;
        type_key: string;
        type_description: string;
    },
    transportation_position: {
        title: string;
        description: string;
        contact: {
            contact_name: string;
            contact_msisdn: string;
            contact_note: string;
        },
        locations: {
            lat: string;
            long: string
        }
    }
}