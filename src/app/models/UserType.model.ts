export interface UserTypeModel {
    _id: string;
    title: string;
    description: string;
    company: string;
    key: string;
    enable: string;
}