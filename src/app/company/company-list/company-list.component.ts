import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {UtilsService} from '../../helper/utils.service';
import {ActivatedRoute, Router, ParamMap} from '@angular/router';
import {Company} from '../../models/Company.model';
import {AuthService} from '../../helper/auth.service';

@Component({
    selector: 'app-company-list',
    templateUrl: './company-list.component.html',
    styleUrls: ['../company.scss']
})
export class CompanyListComponent implements OnInit {

    dataSource;
    displayedColumns = [];
    companies: Company[] = [];
    @ViewChild(MatSort) sort: MatSort;
    public companyId: string;
    public userId: string;
    private status: string;

    constructor(public utilsService: UtilsService,
                public authService: AuthService,
                private router: Router,
                public route: ActivatedRoute) {
        this.userId = this.authService.getUserId();
        this.getCompany();

    }

    ngOnInit() {
        this.getCompany();
        this.displayedColumns = ['number', 'companyName', 'status', 'actions'];
    }

    // companies list
    getCompany() {
        const _this = this;
        this.utilsService.reqGET('/company',  _this.userId, function (resp) {
            if (resp && resp.code === 20000) {
                const comList = resp.data.filter(c => c);
                _this.companies = comList;
                _this.dataSource = new MatTableDataSource(_this.companies);
                _this.dataSource.sort = _this.sort;
            } else {
                alert('fail');
            }
        });
    }

    // icon event.
    onSelect(i, event) {
        const companyId = this.dataSource.data[i]._id;
        const companyName = this.dataSource.data[i].title;
        if (event.target.id === 'editPartnerIcon') {
            this.router.navigate(['/company/company-edit', companyId]);
        } else if (event.target.id === 'seePartnerIcon') {
            this.router.navigate(['/partner/partner-by-company', companyId, companyName]);
        } else if (event.target.id === 'addPartnerIcon') {
            this.router.navigate(['/partner/partner-create']);
        } else if (event.target.id === 'companyMemberIcon') {
            this.router.navigate(['/company/company-user-list', companyId]);
        } else if (event.target.id === 'addCompanyMemberIcon') {
            this.router.navigate(['/company/company-invite-user', companyId]);
        } else if (event.target.id === 'seeWarehouse') {
            this.router.navigate(['/warehouse/warehouse-by-company', companyId]);
        }
    }

    // back to previous page.
    back() {
        this.utilsService.locationBack();
    }

    changeStatus(i, event) {
        this.companyId = this.dataSource.data[i]._id;
        this.status = event.checked;
        this.updateCompany();
    }

    // submit edit company info.
    updateCompany() {
        const params = {
            enable: this.status
        };
        this.utilsService.reqPUT('/companys/' + this.companyId, params, resp => {
          if (resp && resp.code === 20000) {
              this.ngOnInit();
          }
        });
    }

}
