import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {UtilsService} from '../../helper/utils.service';
import {Observable} from 'rxjs/Observable';
import {Company} from '../../models/Company.model';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

const backend_url = environment.apiUrl;

@Component({
    selector: 'app-company-edit',
    templateUrl: './company-edit.component.html',
    styleUrls: ['../company.scss']
})
export class CompanyEditComponent implements OnInit {

    form: FormGroup;
    companies: Company[] = [];
    public companyId: string;
    public status = true;
    lat = localStorage.getItem('lat');
    lng = localStorage.getItem('lng');

    constructor(private fb: FormBuilder,
                public utilsService: UtilsService,
                private http: HttpClient,
                private router: Router,
                private route: ActivatedRoute) {

        this.companyId = this.route.snapshot.paramMap.get('companyId');
        // this.getCompanyInfo();
        this.getCompanyInfo();
    }

    ngOnInit() {
        this.getCompanyInfo();
        // this.fetchCompanyForm();
    }

    // submit edit company info.
    updateCompany(companyForm: NgForm) {
        const _this = this;
        const params = {
            title: companyForm.value.title,
            description: companyForm.value.description,
            contact: {
                address: companyForm.value.address,
                lat: companyForm.value.lat,
                long: companyForm.value.long,
                telephone: companyForm.value.telephone,
                email: companyForm.value.email,
                line: companyForm.value.line,
                facebook: companyForm.value.facebook,
                website: companyForm.value.website
            },
            enable: companyForm.value.enable
        };
        this.utilsService.reqPUT('/companys/' + _this.companyId, params, resp => {
            console.log(resp);
        });
        this.router.navigate(['/company/company-list']);
    }

    getCompanyInfo() {
        this.utilsService.reqGET('/companys', this.companyId, resp => {
            if (resp && resp['code'] === 20000) {
                const data = resp['data'].filter(r => r);
                this.companies = data;
                if (this.lat && this.lng) {
                    resp['data'][0].contact.lat = this.lat;
                    resp['data'][0].contact.long = this.lng;
                } else {
                    resp['data'].filter(r => r.contact.lat);
                    resp['data'].filter(r => r.contact.long);
                }
            }
        });
    }

    // back to previous page.
    back() {
        this.utilsService.locationBack();
    }

    ngOnDestroy(): any {
        localStorage.removeItem('lat');
        localStorage.removeItem('lng');
    }

}
