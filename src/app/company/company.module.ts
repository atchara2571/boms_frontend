import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {AngularMaterialModule} from '../angular-material.module';
import {CompanyCreateComponent} from './company-create/company-create.component';
import {CompanyRoutes} from './company.routing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import { CompanyEditComponent } from './company-edit/company-edit.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyUserListComponent } from './company-user-list/company-user-list.component';
import {CompanyInviteUserComponent} from './company-invite-user/company-invite-user.component';
import { SatPopoverModule } from '@ncstate/sat-popover';
import {InlineEditComponent} from '../inline-edit/inline-edit.component';

@NgModule({
  imports: [
    RouterModule.forChild(CompanyRoutes),
    CommonModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    SatPopoverModule
  ],
  declarations: [
    CompanyCreateComponent,
    CompanyEditComponent,
    CompanyListComponent,
    CompanyInviteUserComponent,
    CompanyUserListComponent,
    InlineEditComponent
  ],
 entryComponents: [InlineEditComponent]
})

export class CompanyModule {}
