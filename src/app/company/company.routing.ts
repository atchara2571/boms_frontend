import { Routes } from '@angular/router';
import {CompanyCreateComponent} from './company-create/company-create.component';
import {CompanyEditComponent} from './company-edit/company-edit.component';
import {CompanyListComponent} from './company-list/company-list.component';
import {CompanyUserListComponent} from './company-user-list/company-user-list.component';
import {CompanyInviteUserComponent} from './company-invite-user/company-invite-user.component';

export const CompanyRoutes: Routes = [
  {
    path: '',
    component: CompanyListComponent,
    data: {
      heading: 'Company List',
      css: 'view-no-padding'
    }
  },
  {
    path: 'company-list',
    component: CompanyListComponent,
    data: {
      heading: 'Company List',
      css: 'view-no-padding'
    }
  },
  {
    path: 'company-create',
    component: CompanyCreateComponent,
    data: {
      heading: 'Company',
      css: 'view-no-padding'
    }
  },
  {
    path: 'company-edit/:companyId',
    component: CompanyEditComponent,
    data: {
      heading: 'Edit Company',
      css: 'view-no-padding'
    }
  },
  {
    path: 'company-invite-user/:companyId',
    component: CompanyInviteUserComponent,
    data: {
      heading: 'Invite Company User',
      css: 'view-no-padding'
    }
  },
  {
    path: 'company-user-list/:companyId',
    component: CompanyUserListComponent,
    data: {
      heading: 'Company User List',
      css: 'view-no-padding'
    }
  }
]
