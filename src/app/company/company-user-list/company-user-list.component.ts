import {Component, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../helper/utils.service';
import {Company} from '../../models/Company.model';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {AccountData} from '../../models/Account.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-company-user-list',
    templateUrl: './company-user-list.component.html',
    styleUrls: ['./company-user-list.component.scss']
})
export class CompanyUserListComponent implements OnInit {

    dataSource;
    public companyId: string;
    public companyName: string;
    companies: Company[] = [];
    users: AccountData[] = [];
    displayedColumns = [];

    @ViewChild(MatSort) sort: MatSort;

    constructor(public utilsService: UtilsService,
                private route: ActivatedRoute,
                private router: Router,
                public dialog: MatDialog) {

        this.companyId = this.route.snapshot.paramMap.get('companyId');
        this.getCompanyById();

    }

    ngOnInit() {
        this.displayedColumns = ['number', 'userName', 'phoneNumber', 'Email'];
    }

    // companies by current user list.
    getCompanyById() {
        // const _this = this;
        this.utilsService.reqGET('/companys/' + this.companyId, null, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
                const company = resp.data[0];
                const users = company.users;
                const companyName = company.title;
                this.companyName = companyName;
                this.companies = users;
                this.dataSource = new MatTableDataSource(this.companies);
                this.dataSource.sort = this.sort;
            }
        });
    }

    // navigate to invite user page.
    addUser() {
        this.router.navigate(['/company/company-invite-user', this.companyId]);
    }

    // openDialog(i: number, event: any): void {
    //   const dialogRef = this.dialog.open(InlineEditComponent, {
    //     width: '250px',
    //     data: {edit: event.target.innerHTML, cId: this.companyId, index: i}
    //   });
    //   dialogRef.afterClosed().subscribe(result => {
    //     console.log('The dialog was closed');
    //     event.target.innerHTML = result;
    //   });
    // }

}
