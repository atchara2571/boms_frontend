import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UtilsService} from '../../helper/utils.service';
import {AuthService} from '../../helper/auth.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {Company} from '../../models/Company.model';

@Component({
    selector: 'app-company',
    templateUrl: './company-create.component.html',
    styleUrls: ['../company.scss']
})
export class CompanyCreateComponent implements OnInit {

    public userId: string;
    public companyId: string;
    public form: FormGroup;
    companies: Company[] = [];

    constructor(private fb: FormBuilder,
                public utilsService: UtilsService,
                public authService: AuthService,
                private router: Router,
                public dialog: MatDialog) {

        this.userId = this.authService.getUserId();
        this.getCompanyByUserId();

    }

    ngOnInit() {
        this.fetchCompanyForm();
    }

    // fetch company form
    fetchCompanyForm() {
        this.form = this.fb.group({
            title: [''],
            description: [''],
            address: [''],
            lat: [localStorage.getItem('lat')],
            long: [localStorage.getItem('lng')],
            telephone: [''],
            email: [''],
            line: [''],
            facebook: [''],
            website: [''],
            enable: ['']
        });
    }

    // company selection list
    getCompanyByUserId() {
        const _this = this;
        this.utilsService.reqGET('/company', _this.userId, resp => {
            if (resp && resp.code === 20000) {
                console.log(resp);
                const companyId = resp.data.map(c => c._id);
                _this.companyId = companyId;
            }
        });
    }

    // create new company
    onCreateCompany() {
        if (this.form.invalid) {
            return;
        }
        const params = {
            'title': this.form.value.title,
            'description': this.form.value.description,
            'contact': {
                'address': this.form.value.address,
                'lat': this.form.value.lat,
                'long': this.form.value.long,
                'telephone': this.form.value.telephone,
                'email': this.form.value.email,
                'line': this.form.value.line,
                'facebook': this.form.value.facebook,
                'website': this.form.value.website,
            },
            'enable': this.form.value.enable,
            'users': [{
                'by': this.userId
            }]
        };
        this.utilsService.reqPOST('/companys', params, function (resp) {
            console.log(resp);
            alert('Create successful');
        });
        this.router.navigate(['/company', this.companyId]);
    }

    // clear storage when leave this page.
    ngOnDestroy(): void {
        localStorage.removeItem('lat');
        localStorage.removeItem('lng');
    }

    // back to previous page.
    back() {
        this.utilsService.locationBack();
    }

}
