import {Component, OnInit} from '@angular/core';

import {UtilsService} from '../../helper/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {MatTableDataSource} from '@angular/material';
import {UserTypeModel} from '../../models/UserType.model';
import {AccountData} from '../../models/Account.model';

const backend_url = environment.apiUrl;

@Component({
    selector: 'app-company-invite-user',
    templateUrl: './company-invite-user.component.html',
    styleUrls: ['../company.scss']
})
export class CompanyInviteUserComponent implements OnInit {

    form: FormGroup;
    public userTypes: UserTypeModel[] = [];
    public users: AccountData[] = [];
    public userId: string;
    public companyId: string;
    public userTypeId: string;

    constructor(public utilsService: UtilsService,
                private router: Router,
                private route: ActivatedRoute,
                private fb: FormBuilder,
                private http: HttpClient) {

        this.companyId = this.route.snapshot.paramMap.get('companyId');

    }

    ngOnInit() {
        this.fetchEmailForm();
        this.getUserType();
        // this.getCompanyById();
    }

    fetchEmailForm() {
        this.form = this.fb.group({
            email: [''],
            user_type: ['']
        });
    }

    getUserType() {
        const _this = this;
        this.utilsService.reqGET('/config/type/users', null, resp => {
            if (resp && resp.code === 20000) {
                const data = resp.data.filter(r => r);
                _this.userTypes = data;
            }
        });
    }

    // check existing email.
    checkEmail() {
        const _this = this;
        const email = this.form.value.email;
        this.http.get(backend_url + '/find/userinfo/email/' + email).subscribe(resp => {
            if (resp['code'] === 20000) {
                const userId = resp['data']._id;
                _this.userId = userId;
               _this.checkDuplicatedEmail(email);
            }
        }, error => {
            if (_this.form.value.email) {
                alert('This email have not registered.');
            } else {
                console.log('Please enter an email address.');
            }

        });
    }

    checkDuplicatedEmail(email) {
        const _this = this;
        this.utilsService.reqGET('/companys/' + _this.companyId, null, resp => {
            if (resp && resp.code === 20000) {
                const company = resp.data[0];
                const user_email = company.users.filter(e => e.by.userinfo.email === email);
                console.log(user_email);
                if (user_email.length > 0) {
                    alert('This email is already member');
                } else if (user_email.length === 0) {
                    _this.sendInvite();
                }
            }
        });
    }

    // send email to user.
    sendInvite() {
        const params = {
            'users': [{
                'by': this.userId,
                'status': true
            }]
        };
        this.utilsService.reqPUT('/companys/' + this.companyId + '/' + 'users', params, resp => {
            console.log(resp);
        });
        alert('Success!');
        this.router.navigate(['/company/company-user-list', this.companyId]);
    }

    // back to previous page.
    back() {
        this.utilsService.locationBack();
    }

}
