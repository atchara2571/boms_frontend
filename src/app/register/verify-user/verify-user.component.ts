import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators, NgForm} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {AuthService} from '../../helper/auth.service';

@Component({
    selector: 'app-verify-user',
    templateUrl: './verify-user.component.html',
    styleUrls: ['./verify-user.component.scss']
})
export class VerifyUserComponent implements OnInit {

    form: FormGroup;
    username: string;
    email: string;
    isLoading = false;
    userId: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private authService: AuthService) {

        this.userId = this.authService.getUserId();
        this.email = this.authService.getEmail();

    }

    ngOnInit() {
        this.isLoading = true;
        this.fetchOtpForm();
    }

    // fetch otp form.
    fetchOtpForm() {
        this.form = this.fb.group({
            otp: [null, Validators.compose([Validators.required, CustomValidators.otp])]
        });
    }

    // submit sent verification code to user's email.
    onSendVerify() {
        this.authService.sendVerify(this.email);
        alert('Verification code has been sent! Please check your email address');
    }

    // submit confirm user's verification code.
    confirmCode() {
        if (this.form.invalid) {
            return;
        }
        this.authService.sendCodeConfirm(this.form.value.otp);
        alert('Verify successful!');
    }

}
