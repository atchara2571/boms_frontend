import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormGroup, Validators, FormControl, FormBuilder} from '@angular/forms';

import {AuthService} from '../../helper/auth.service';
import {Subscription} from 'rxjs/Subscription';
import {UtilsService} from '../../helper/utils.service';
import {MatDatepicker} from '@angular/material';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnDestroy {
    form: FormGroup;
    isLoading = false;
    image: File;
    imagePreview: string;

    private authStatusSub: Subscription;

    constructor(public authService: AuthService, public utilsService: UtilsService, private fb: FormBuilder) {
    }

    @ViewChild(MatDatepicker) datepicker: any;

    ngOnInit() {
        this.fetchSignupForm();
        this.checkAuthStatus();
    }

    // check sigin status.
    checkAuthStatus() {
        this.authStatusSub = this.authService.getAuthStatusListener().subscribe(
            authStatus => {
                this.isLoading = false;
            });
    }

    // fetch signup form.
    fetchSignupForm() {
        this.form = this.fb.group({
            username: [''],
            password: [''],
            firstname: [''],
            lastname: [''],
            card_id: [''],
            birthday: [''],
            msisdn: [''],
            email: [''],
            lineid: [''],
            facebook_id: [''],
            google_id: [''],
            image: ['']
        });
    }

    // get image file.
    onImagePicked(event: Event) {
        const file = (event.target as HTMLInputElement).files[0];
        this.form.patchValue({image: file});
        this.form.get('image').updateValueAndValidity();
        const reader = new FileReader();
        reader.onload = () => {
            this.imagePreview = reader.result;
        };
        reader.readAsDataURL(file);
    }

    // submit signing up.
    signUp() {
        if (this.form.invalid) {
            return;
        }
        //   this.isLoading = true;
        this.authService.checkUsername(
            this.form.value.username
        );
        this.authService.checkMsisdn(
            this.form.value.msisdn
        );
        this.authService.checkEmail(
            this.form.value.email
        );
        this.authService.checkFacebook(
            this.form.value.facebook_id
        );
        this.authService.checkGoogle(
            this.form.value.google_id
        );
        this.authService.createUser(
            this.form.value.username,
            this.form.value.password,
            this.form.value.firstname,
            this.form.value.lastname,
            this.form.value.card_id,
            this.form.value.birthday,
            this.form.value.msisdn,
            this.form.value.email,
            this.form.value.lineid,
            this.form.value.facebook_id,
            this.form.value.google_id,
            this.form.value.image);
    }

    // unsubscribe user's session.
    ngOnDestroy() {
        this.authStatusSub.unsubscribe();
    }

}
