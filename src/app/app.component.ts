import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {AuthService} from './helper/auth.service';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent implements OnInit {
  constructor(translate: TranslateService, private authService: AuthService) {
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang('en');

    const browserLang: string = translate.getBrowserLang();
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
  }

  // check user authen status when stating web app
  ngOnInit() {
    this.authService.autoAuthUser();
  }
}
