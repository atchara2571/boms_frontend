import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state?: string;
  name?: string;
  type: string;
  icon?: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    type: 'title',
    name: 'MAIN'
  },
  {
    state: '/',
    name: 'HOME',
    type: 'link',
    icon: 'explore'
  },
  {
    state: 'widgets',
    name: 'WIDGETS',
    type: 'link',
    icon: 'photo'
  },
  {
    type: 'divider'
  },
  {
    type: 'title',
    name: 'APPS'
  },
  {
    state: 'calendar',
    name: 'Calendar',
    type: 'link',
    icon: 'date_range'
  },
  {
    state: 'messages',
    name: 'MESSAGES',
    type: 'link',
    icon: 'email'
  },
  {
    state: 'social',
    name: 'SOCIAL',
    type: 'link',
    icon: 'person'
  },
  {
    state: 'taskboard',
    name: 'TASKBOARD',
    type: 'link',
    icon: 'view_column',
  },
  {
    type: 'divider'
  },
  {
    type: 'title',
    name: 'COMPONENTS'
  },
  {
    state: 'material',
    name: 'MATERIAL',
    type: 'sub',
    icon: 'equalizer',
    badge: [
      {type: 'purple', value: '30'}
    ],
    children: [
      {state: 'autocomplete', name: 'AUTOCOMPLETE'},
      {state: 'button', name: 'BUTTON'},
      {state: 'button-toggle', name: 'BUTTONTOGGLE'},
      {state: 'card', name: 'CARDS'},
      {state: 'checkbox', name: 'CHECKBOX'},
      {state: 'chips', name: 'CHIPS'},
      {state: 'datepicker', name: 'DATEPICKER'},
      {state: 'dialog', name: 'DIALOG'},
      {state: 'drawer', name: 'DRAWER'},
      {state: 'expansion', name: 'EXPANSION'},
      {state: 'grid-list', name: 'GRID'},
      {state: 'input', name: 'INPUT'},
      {state: 'list', name: 'LISTS'},
      {state: 'menu', name: 'MENU'},
      {state: 'progress-bar', name: 'PROGRESSBAR'},
      {state: 'progress-spinner', name: 'PROGRESSSPINNER'},
      {state: 'radio', name: 'RADIO'},
      {state: 'ripple', name: 'RIPPLE'},
      {state: 'select', name: 'SELECT'},
      {state: 'slide-toggle', name: 'SLIDE'},
      {state: 'slider', name: 'SLIDE'},
      {state: 'snack-bar', name: 'SNACKBAR'},
      {state: 'stepper', name: 'STEPPER'},
      {state: 'table', name: 'TABLE'},
      {state: 'tabs', name: 'TABS'},
      {state: 'toolbar', name: 'TOOLBAR'},
      {state: 'tooltip', name: 'TOOLTIP'},
      {state: 'typography', name: 'TYPOGRAPHY'}
    ]
  },
  {
    state: 'forms',
    name: 'FORMS',
    type: 'sub',
    icon: 'looks_3',
    children: [
      {state: 'dynamic', name: 'DYNAMIC'},
      {state: 'validation', name: 'VALIDATION'},
      {state: 'upload', name: 'UPLOAD'},
      {state: 'tree', name: 'TREE'},
    ]
  },
  {
    state: 'dragndrop',
    name: 'DND',
    type: 'link',
    icon: 'show_chart',
  },
  {
    type: 'divider'
  },
  {
    type: 'title',
    name: 'DATAVIZ'
  },
  {
    state: 'tables',
    name: 'DATATABLE',
    type: 'sub',
    icon: 'format_line_spacing',
    badge: [
      {type: 'blue-grey', value: '8'
      }
    ],
    children: [
      {state: 'fullscreen', name: 'FULLSCREEN'},
      {state: 'editing', name: 'EDITING'},
      {state: 'filter', name: 'FILTER'},
      {state: 'paging', name: 'PAGING'},
      {state: 'sorting', name: 'SORTING'},
      {state: 'pinning', name: 'PINNING'},
      {state: 'selection', name: 'SELECTION'},
    ]
  },
  {
    state: 'maps',
    name: 'MAPS',
    type: 'sub',
    icon: 'navigation',
    children: [
      {state: 'google', name: 'GOOGLE'},
      {state: 'leaflet', name: 'LEAFLET'}
    ]
  },
  {
    state: 'charts',
    name: 'CHARTS',
    type: 'link',
    icon: 'show_chart',
  },
  {
    type: 'divider'
  },
  {
    type: 'title',
    name: 'PAGES'
  },
  {
    state: 'account',
    name: 'ACCOUNT',
    type: 'sub',
    icon: 'verified_user',
    children: [
      {state: 'signin', name: 'SIGNIN'},
      {state: 'signup', name: 'SIGNUP'},
      {state: 'forgot', name: 'FORGOT'},
      {state: 'lockscreen', name: 'LOCKSCREEN'},
    ]
  },
  {
    state: 'error',
    name: 'ERROR',
    type: 'sub',
    icon: 'error',
    children: [
      {state: '404', name: '404'},
      {state: 'error', name: 'ERROR'},
    ]
  },
  {
    state: 'ecommerce',
    name: 'ECOMMERCE',
    type: 'sub',
    icon: 'looks_3',
    badge: [
      {type: 'red', value: 'new'
      }
    ],
    children: [
      {state: 'products', name: 'PRODUCTS'},
      {state: 'compact', name: 'COMPACT'},
      {state: 'detail', name: 'DETAIL'},
    ]
  },
  {
    state: 'pages',
    name: 'PAGES',
    type: 'sub',
    icon: 'pages',
    children: [
      {state: 'invoice', name: 'INVOICE'},
      {state: 'timeline', name: 'TIMELINE'},
      {state: 'blank', name: 'BLANK'},
      {state: 'pricing', name: 'PRICING'},
    ]
  },
  {
    type: 'divider'
  },
  {
    state: 'documentation',
    name: 'DOCS',
    type: 'link',
    icon: 'help'
  }
];

@Injectable()
export class MenuService {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  add(menu: Menu) {
    MENUITEMS.push(menu);
  }
}
